# Tag usage:
# {% banner_init %}
#   choose a random banner image
#
# {% banner_img %}
#   print the image name for the chosen banner
#
# {% banner_author %}
#   print the author name for the chosen banner
#
# {% banner_license %}
#   print the license information for the chosen banner
#
# {% banner_source %}
#   print the source URL for the chosen banner
#
# If no random banner was chosen with {% banner_init %}, the default banner is
# taken. See the definition of @@key below.
# A static image on a post can be used by setting the "banner" variable in the
# YAML front matter to one of the banner image files in the BANNERS array below.

module Banner
  ASSET_PATH = "images/header"
  LICENSES = {
    ccbysa40:  "CC BY-SA 4.0",
    ccbysa30:  "CC BY-SA 3.0",
    ccby40:    "CC BY 4.0",
    ccby30:    "CC BY 3.0",
    ccby20:    "CC BY 2.0",
  }
  # syntax: "filename" => { author: "…", license: "…", source: "…" }, where
  # filename is relative to ASSET_PATH
  BANNERS = {
    "ethernet.jpg" => {
      author: "chrissi^"
    },
    "Amiga-Diskettenbox.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Amiga-Diskettenbox.jpg"
    },
    "Bohrersortiment.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Bohrersortiment.jpg"
    },
    "Laterne_Holodeck.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Laterne_Holodeck.jpg"
    },
    "Rainbow_Cable-01.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Rainbow_Cable-01.jpg"
    },
    "Rainbow_Cable-02.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Rainbow_Cable-02.jpg"
    },
    "STM32L151_Makro.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:STM32L151_Makro.jpg"
    },
    "Satzung_Dont_Panic.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Satzung_Don't_Panic!.jpg"
    },
    "aufkleber.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Aufkleber.jpg"
    },
    "tetris.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Tetrissteine.jpg"
    },
    "20120326-pony.jpg" => {
      author: "Pecca, larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:20120326-pony.jpg"
    },
    "20120506-matetags-01.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:20120506-matetags-01.jpg"
    },
    "BarCamp_BS_2012_Drucker_bestaunen.jpg" => {
      author: "Kai Nehm",
      license: LICENSES[:ccby20],
      source: "https://secure.flickr.com/photos/trau_kainehm/8216678944"
    },
    "switch-blinkenlights.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Switch-blinkenlights.jpg"
    },
    "reprap-extruder-drives.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Reprap-extruder-drives.jpg"
    },
    "iptable.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Iptable.jpg"
    },
    "bruestungskanal-reprap-yoda.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Bruestungskanal-reprap-yoda.jpg"
    },
    "GPN14-thinkpad-pickset.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:GPN14-larsan-pickset-thinkpad.jpg"
    },
    "20120201-LEDPanel25.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:20120201-LEDPanel25.jpg"
    },
    "20120201-LEDPanel34.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:20120201-LEDPanel34.jpg"
    },
    "20120201-LEDPanel16.jpg" => {
      author: "larsan",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:20120201-LEDPanel16.jpg"
    },
    "Trontelefon.jpg" => {
      author: "Daniel Bohrer",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Trontelefon.jpg"
    },
    "werkstatt-wand.jpg" => {
      author:  "comawill",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Spacebilder07-wand-werkstatt.JPG"
    },
    "Brildor_Garnkoffer.jpg" => {
      author:  "larsan",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Brildor_Garnkoffer.jpg"
    },
    "Coldbrew_start.jpg" => {
      author:  "drc",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Coldbrew_start.jpg"
    },
    "Frickelraum_space20.jpg" => {
      author:  "Sintox",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Frickelraum_space20.jpg"
    },
    "Stricken_Wollpaket-1.jpg" => {
      author:  "Sonnenschein",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Stricken_Wollpaket-1.jpg"
    },
    "Stricken_Puschen_Space-Invaders.jpg" => {
      author: "Sonnenschein",
      license: LICENSES[:ccbysa30],
      source: "https://stratum0.org/wiki/Datei:Stricken_Puschen_Space-Invaders.JPG"
    },
    "20120430-whiteboard-01.jpg" => {
      author:  "larsan",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:20120430-whiteboard-01.jpg"
    },
    "20120627-kassendisplay-02.jpg" => {
      author:  "larsan",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:20120627-kassendisplay-02.jpg"
    },
    "BarCamp_BS_2012_Druck.jpg" => {
      author:  "Kai Nehm",
      license: LICENSES[:ccby20],
      source:  "https://stratum0.org/wiki/Datei:BarCamp_BS_2012_Druck.jpg"
    },
    "Chillraum_vortragsraum_2_space20.jpg" => {
      author:  "Sintox",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:Chillraum_vortragsraum_2_space20.jpg"
    },
    "Mensadisplay_an_der_Wand.jpg" => {
      author:  "Emantor",
      license: LICENSES[:ccby30],
      source:  "https://stratum0.org/wiki/Datei:Mensadisplay_an_der_Wand.jpg"
    },
    "20121020-spacepics-03.jpg" => {
      author:  "larsan",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/wiki/Datei:20121020-spacepics-03.jpg"
    },
    "LED_grid_rainbows_1.jpg" => {
      author:  "Daniel Bohrer",
      license: LICENSES[:ccby40],
      source:  "https://stratum0.org/wiki/Datei:LED_grid_rainbows_1.jpg"
    },
    "unifi-mesh.jpg" => {
      author:  "Chrissi^",
      license: LICENSES[:ccbysa30],
      source:  "https://stratum0.org/blog/posts/2018/06/01/freifunk-event-wlan/"
    },
  }

  @@key = "Mensadisplay_an_der_Wand.jpg"

  class Tag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super

      if tag_name == "banner_init"
        @render_mode = :init
        if text != ""
          @@key = text
        else
          rand = Random.new.rand(Banner::BANNERS.keys.length)
          @@key = Banner::BANNERS.keys[rand]
        end

      elsif tag_name == "banner_img"
        @render_mode = :file

      elsif tag_name == "banner_author"
        @render_mode = :author

      elsif tag_name == "banner_license"
        @render_mode = :license

      elsif tag_name == "banner_source"
        @render_mode = :source

      end
    end

    def render(context)

      asset_path = context.registers[:site].config["root"].dup
      if not asset_path.end_with? "/"
        asset_path << "/"
      end

      # allow overwrite in YAML front matter
      yaml = context.environments.first["page"]["banner"]

      # if banner.img, banner.author etc was set in YAML...
      if yaml.class == Hash

        if yaml["img"].class == String and yaml["img"].strip != ""

          if @render_mode == :file
            return "#{asset_path}#{yaml["img"]}"

          elsif @render_mode == :author
            return yaml["author"]

          elsif @render_mode == :license
            return yaml["license"]

          elsif @render_mode == :source
            return yaml["source"]
          end

        else
          puts "NOTICE: banner.img was not set in YAML front matter, but others"
          return
        end

      # otherwise, interpret as file name from our predefined banners
      elsif yaml.class == String and yaml.strip != ""
        @@key = yaml
      end

      asset_path << Banner::ASSET_PATH
      if @render_mode == :file
        "#{asset_path}/#{@@key}"
      else
        Banner::BANNERS[@@key][@render_mode]
      end

    end
  end
end

Liquid::Template.register_tag('banner_init', Banner::Tag)
Liquid::Template.register_tag('banner_img', Banner::Tag)
Liquid::Template.register_tag('banner_author', Banner::Tag)
Liquid::Template.register_tag('banner_license', Banner::Tag)
Liquid::Template.register_tag('banner_source', Banner::Tag)
