---
layout: page
title: "impressum"
footer: true
---

Der Verein Stratum 0 e. V. ist beim Amtsgericht Braunschweig unter der
Registernummer 200889 ins Vereinsregister eingetragen. Verantwortlich für den
Inhalt der Homepage ist der Vorstand. Für weitere Informationen siehe [Kontakt].

<strong>Stratum 0 e.V.</strong>

Hamburger Str. 273A<br />
38114 Braunschweig<br />
Tel: +49-531-2876924-5<br />
Fax: +49-531-2876924-6 

[kontakt]: https://stratum0.org/wiki/Kontakt
