---
layout: post
title: "Stratumnews Vol. 19"
author: Stratumnews Team
date: 2017-05-16 18:50
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:
<!-- more -->

IPv6 im Space
-------------
Nach einer endlosen v6-Dürre auf TUBS-Niveau hat der Space nun endlich wieder funktionierendes v6.
Dank gebührt Doom, Bugreports bitte auch an ihn.
Der Ubiquiti Edge-Router wurde dafür außer Betrieb genommen und durch den wdr4300 ersetzt, der zuvor in der Kammer des Schreckens Switch und AP gespielt hat. Da dieser doch etwas schwachbrüstig ist für Spaceanforderungen, soll er beizeiten durch einen D-Link dir-860l ersetzt werden.

CoderDojo
---------
Nach 6 Monaten Pause fand im April unser 19. CoderDojo statt. Um die Veranstaltung sowohl für uns, als auch für die Kinder zukünftig interessanter zu gestalten gab und gibt es eine Reihe an Bastelterminen. Hierbei beschäftigen wir uns mit diversen Dingen die über Scratch hinaus gehen. Beispielsweise wurde ein 10er-Pack bbc-microbits bestellt, sowie ein Satz Elektromotorbausätze.

Bratworscht
-----------
75 kg Fleisch wurden erfolgreich verwurstet. Dank neuer Hardware, insbesondere einem gecrowdfundeten Wurstfüller, konnte der Workshop dieses mal an nur einem Tag durchgeführt werden. Und das trotz der doppelten Menge. Im Vorfeld des Bratwurstworkshops wurde zudem ein neuer Vakuumierer gecrowdfunded. Ein Blogpost folgt in näherer Zukunft.

Elektroschrott und Aufräumsituation
-----------------------------------
Schon in den letzten Monaten wurde einiges an Schrott entsorgt, das soll nun weiter gehen. Es gibt eine allgemeine [Aufräumliste][0]. Wenn ihr etwas von den Dingen haben wollt, die auf den Schrott/zu ELPRO sollen, kümmert euch bitte rechtzeitig darum.

[0]: https://pad.stratum0.org/p/s0aufr%C3%A4umaktion02

3D-Druck
--------
Da sich der Drucker-Pi inkonsistent verhielt, wurde die ganze Angelegenheit kurzerhand runderneuert: Der Raspberry Pi B+ wurde gegen einen Pi 2 getauscht und eine neue SD-Karte eingebaut. Diesmal wurde ein jessie-lite verwendet und bei der Gelegenheit gleichzeitig ein aktueller Repetier-Server installiert. API-Key, IP-Adresse und Hostname haben sich geändert und sind im [Wiki][1] zu finden. Damit keine Daten verloren gehen, wurden sämtliche g-code-Dateien temporär aufs NAS geschoben, sie sind unter /print/3D/ zu finden.

[1]: https://stratum0.org/wiki/3D-Drucken#Schritt_3:_GCode_senden

Finanzielles
------------
Wir hatten kürzlich einen [Blogpost vom /dev/tal][2] sinngemäß kopiert und zum ARD/ZDF/Deutschlandradio-Beitragsservice geschickt. Darauf kam jetzt eine Antwort, dass wir bisher zu Unrecht Rundfunkgebühren bezahlt haben, weil wir keine Angestellten beschäftigen. Die zuviel gezahlten Beiträge von etwa 229,77€ werden uns zurück erstattet.

Unsere Steuererklärung wurde vom Finanzamt abgesegnet und wir haben einen Freistellungsbescheid für Körperschafts- und Gewerbesteuer für 2014 und 2015 erhalten. Damit dürfen wir jetzt bis zum 18.04.2022 Zuwendungsbescheinigungen für Mitgliedsbeiträge und Spenden ausstellen 
(Was mit 2013 ist, ist vorerst noch unklar.)

[2]: https://www.devtal.de/blog/2015/11/18/wir-zahlen-keinen-rundfunkbeitrag/

Misc
----

* Das Schloss in der Haustür wurde durch ein Panikschloss ersetzt, es sollte jetzt nicht mehr möglich sein, dass schlüssellose Entitäten im Gebäude eingesperrt werden.
* Die Windows-Installation auf Herbert kennt nun einen "StratumTUBS"-User. Mit VPN-Client, PaperCut-Client und einigen TU-Druckern. Passwort: tubs
* Es wurden 5 neue Fahrradkellerschlüssel beantragt.
* Der Verstärker im Frickelraum wurde durch den Pioneerverstärker, der noch im Subraum rumstand, ersetzt. Nach Reparatur (Tausch eines Kondensators) scheint dieser nur zuverlässig zu funktionieren.

Termine
-------

*  Mi, 17.05. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Mi, 17.05. 19:00: Freifunk-Treffen
*  Mi, 17.05. 19:00: Vegan Academy
*  Do, 18.05. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 18.05. 19:00 - 23:00: Anime Referat
*  Sa, 20.05. 14:00: CoderDojo
*  Mo, 22.05. 18:00: Malkränzchen
*  Di, 23.05. 17:00: Schreibrunde
*  Mi, 24.05. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Mi, 24.05. 19:00: Freifunk-Treffen
*  Do, 25.05. bis So, 28.05.: GPN17, Karlsruhe
*  Do, 25.05. 19:00 - 23:00: Captain's Log
*  Mo, 29.05. 18:00: Malkränzchen
*  Di, 30.05. 17:00: Schreibrunde
*  Di, 30.05. 18:00: Coptertreffen, Allgemeiner Infoaustausch
*  Mi, 31.05. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Mi, 31.05. 19:00: Freifunk-Treffen
*  Mi, 31.05. 19:00: Vegan Academy
*  Do, 01.06. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 01.06. 19:00 - 23:00: Anime Referat
*  Sa, 03.06. 20:00: KeyMatic Batteriewechselparty
*  Mo, 05.06. 18:00: Malkränzchen
*  Do, 08.06. 19:00 - 23:00: Captain's Log

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!
