---
layout: post
title: "About Writeups, and CTFs"
date: 2013-09-27 23:50
comments: true
author: valodim
categories: [english,meta, ctf]
---

At the point of this writing, 13 out of 16 of our blog entries are writeups for
challenges from various CTFs. In fact, they were taking up so much space we
reduced their visibility on the front page slightly compared to regular blog
entries, and also gave them their own archive page. I felt we owe an
explanation to our readers on what is up with those, and CTFs in general.

Read on to find out about what a CTF is, and how we at Stratum 0 are involved.

<!-- more -->

## What is a CTF?

This acronym stands for "Capture The Flag", which is the common term for a type
of hacking slash security contest. CTFs are regularly organized for
conferences, other types of events or sometimes just stand-alone. During a CTF
event, which usually lasts for two to three days, hackers organized in teams
all over the world try to competitively solve as many challenges as possible to
maximize their score. There are commonly 20 to 25 challenges in a CTF, ranked
by difficulty and grouped together in categories, which differ between contests
but almost always include Pwning, Crypto, Reversing, Forensics and Web.

* **Crypto** challenges are all about understanding and consequently breaking
  insecure crypto ciphers, protocols, and signatures.
* **Forensics** usually covers recovery of files or information from different data
  sources like broken file systems, memory dumps or captured network traffic.
* **Pwning** challenges provide the binary or code of an application, which
  must be dissected to exploit and ultimately discover the flag from a remotely
  running instance of the same application.
* **Reversing** challenges require analysis of a compiled binary file to either
  find information or modify its behavior in order to discover the flag from
  it.
* **Web** challenges involve hacking of web services, especially HTTP. This
  includes SQL injection, analysis of service vulnerabilities and bypassing
  authentication schemes in general.

There are a plethora of other categories coming up irregularly which range from
trivia and recon types to shellcode, guertilla programming or just plain
"misc", serving a challenge spectrum from pure execution to out of the box
thinking. Difficulty also varies wildly between tasks, from mindbendingly
difficult requiring 10+ hours of thorough contemplation and hacking by seasoned
players or "just plain impossible", to solvable by newbies in an hour or two.

## Writeups

Solutions to the challenges are usually not given by the organizers of an event
even after it is over, but instead provided in the form of writeups from the
playing teams. Since oftentimes there is no single obvious way to solve a
problem, there is a certain diversity to the approaches taken by different
teams and reading writeups often provides interesting in- or at least
hindsight.

So that's what we are doing here. Each writeup post is the solution to one
challenge in a CTF event we participated in. We don't write one for every
challenge we solve, obviously, but pick the ones we found particularly
entertaining, consider interesting technically, or were the only ones to solve.

## Our CTF Team

We started playing CTFs some time around May 2012. At first there were three
and a half people attending regularly, stumbling their way through the levels
without much of a clue of what we were doing. This changed over time, we found
more players, everyone learned a lot and we are now about six regulars plus a
couple of on and off players, and some of us even know what they are doing.

The website [CTF Time] tries to keep track of the scoreboards of all CTF
events, assigning points to teams based on rating weights for all events. It
established its status as a global CTF ranking. In 2012, the [Stratum 0 team]
ended up in place 45 of this ranking despite participating only in a relativly
small number of events. In 2013, we are the highest ranking German team on
position 21st, so far.

In a recent development, the Stratum 0 team joined efforts with the team from
[CCC Aachen], forming the new team Stratum Auhuur.  The experiment worked well
with online collaboration at ASIS CTF and CSAW CTF, and we are currently
planning to visit Aachen for the [hack.lu] CTF event. If all goes well, we
might play as a joint team for the entire season of 2014, and are shooting for
a place in the Top 10 at CTF Time.

## Come visit!

I would also like to point out that while a certain level of computer literacy
is required to participate in CTFs, we are always happy about potential new
players and generally open to guests. All CTF events are announced on the
[Normalverteiler] and [Stratum News] mailing lists and are open to anyone who
feels like breaking weak rsa keys, injecting sql, going wild with buffer
overflows, learn what those things even mean, cheer us on, or just look at a
bunch of nerds thinking real hard for extended periods of time.


[CTF Time]: http://ctftime.org/
[Stratum 0 team]: http://ctftime.org/team/1684
[CCC Aachen]: http://ctftime.org/team/555
[hack.lu]: http://ctftime.org/event/97
[normalverteiler]: http://lists.stratum0.org/mailman/listinfo/normalverteiler
[stratum news]: http://lists.stratum0.org/mailman/listinfo/newsletter
