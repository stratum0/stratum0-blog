---
layout: post
title: "Asisctf 2013: memdump"
date: 2013-09-27 19:40
comments: true
author: spq
categories: [english,ctf,writeup,asisCTF13,guestpost]
---

Forensic 220 challenge of [ASIS CTF 2013][asis ctf 2013]

## Task

Points: 220 \\
Level: 1

What if the flag? \\
file: <http://asis.io/media/mem.dump.xz> \\
mirror: <https://docs.google.com/file/d/0B5y5AGVPzpIOM3dINnlPUkk4aGc/edit> \\
MD5: 7d06b077961dbd564e600c5363eaffb8

In this challenge, we get a big file which is obviously a memory dump of a running machine.

<!-- more -->

``` sh
$ file mem.dump
mem.dump: ELF 64-bit LSB core file x86-64, version 1 (SYSV)
```

Okay, what filetype is it exactly?

``` sh
$ readelf -n mem.dump
Notes at offset 0x000002e0 with length 0x00000440:
  Owner                Data size      Description
  VBCORE              0x00000018      Unknown note type: (0x00000b00)
  VBCPU                0x00000400      Unknown note type: (0x00000b01)
```

It's a VirtualBox memory dump...
Lets see, which OS its running, possibly linux?

``` sh
$ strings mem.dump|egrep '^Linux.*[0-9.]{3,}.*SMP'
Linux version 3.5.0-23-generic (buildd@komainu) (gcc version 4.6.3 (Ubuntu/Linaro 4.6.3-1ubuntu5) ) #35~precise1-Ubuntu SMP Fri Jan 25 17:13:26 UTC 2013 (Ubuntu 3.5.0-23.35~precise1-generic 3.5.7.2)
```

Okay, we have a VirtualBox memory dump of a running Ubuntu Linux with kernel
3.5.0. We can use Volatility for analysing the contents but first we need to
have a Volatility profile for this exact kernel, luckily someone else
[did this for us already](http://blog.lse.epita.fr/articles/59-ebctf-2013-for100.html).

Now we have everything we need to find whats inside this memory dump with Volatility.
Lets see which processes are running:

``` sh
$ python vol.py --profile=LinuxUbuntu1204x64 -f mem.dump linux_psaux
Volatile Systems Volatility Framework 2.3_beta
Pid    Uid    Gid    Arguments                                                     
1      0      0      /sbin/init ro                                                 
2      0      0      [kthreadd]                                                     
3      0      0      [ksoftirqd/0]                                                 
5      0      0      [kworker/u:0]                                                 
6      0      0      [migration/0]                                                 
7      0      0      [watchdog/0]                                                   
8      0      0      [cpuset]                                                       
9      0      0      [khelper]                                                     
10    0      0      [kdevtmpfs]                                                   
11    0      0      [netns]                                                       
12    0      0      [sync_supers]                                                 
13    0      0      [bdi-default]                                                 
14    0      0      [kintegrityd]                                                 
15    0      0      [kblockd]                                                     
16    0      0      [ata_sff]                                                     
17    0      0      [khubd]                                                       
18    0      0      [md]                                                           
21    0      0      [khungtaskd]                                                   
22    0      0      [kswapd0]                                                     
23    0      0      [ksmd]                                                         
24    0      0      [fsnotify_mark]                                               
25    0      0      [ecryptfs-kthrea]                                             
26    0      0      [crypto]                                                       
35    0      0      [kthrotld]                                                     
36    0      0      [scsi_eh_0]                                                   
38    0      0      [scsi_eh_1]                                                   
39    0      0      [scsi_eh_2]                                                   
40    0      0      [kworker/u:3]                                                 
42    0      0      [binder]                                                       
61    0      0      [deferwq]                                                     
62    0      0      [charger_manager]                                             
63    0      0      [devfreq_wq]                                                   
195    0      0      [kdmflush]                                                     
205    0      0      [kdmflush]                                                     
219    0      0      [jbd2/dm-0-8]                                                 
220    0      0      [ext4-dio-unwrit]                                             
314    0      0      upstart-udev-bridge --daemon                                   
316    0      0      /sbin/udevd --daemon                                           
417    102    105    dbus-daemon --system --fork --activation=upstart               
426    101    103    rsyslogd -c5                                                   
445    0      0      [kpsmoused]                                                   
526    0      0      dhclient3 -e IF_METRIC=100 -pf /var/run/dhclient.eth0.pid -lf /var/lib/dhcp/dhclient.eth0.leases -1 eth0
617    0      0      upstart-socket-bridge --daemon                                 
766    0      0      /sbin/getty -8 38400 tty4                                     
774    0      0      /sbin/getty -8 38400 tty5                                     
785    0      0      /sbin/getty -8 38400 tty2                                     
801    0      0      /sbin/getty -8 38400 tty3                                     
803    0      0      /sbin/getty -8 38400 tty6                                     
815    103    106    whoopsie                                                       
816    0      0      acpid -c /etc/acpi/events -s /var/run/acpid.socket             
819    0      0      cron                                                           
820    0      0      atd                                                           
837    0      1000  /bin/login --                                                 
967    1000  1000  -bash                                                         
8111  0      0      /sbin/udevd --daemon                                           
8112  0      0      /sbin/udevd --daemon                                           
9425  1000  1000  ./asis-ctf                                                     
15584  1000  1000  nano print.cpp                                                 
15629  0      0      [kworker/0:0]                                                 
16346  0      0      /usr/sbin/apache2 -k start                                     
16349  33    33    /usr/sbin/apache2 -k start                                     
16350  33    33    /usr/sbin/apache2 -k start                                     
16351  33    33    /usr/sbin/apache2 -k start                                     
16783  0      0      [kworker/0:1]                                                 
16784  0      0      [kworker/0:2]                                                 
```

Okay, we have a running editor and a running app called asis-ctf, that process sounds interesting, lets dump the process ./asis-ctf:

``` sh
$ python vol.py --profile=LinuxUbuntu1204x64 -f mem.dump linux_proc_maps -p 9425
Volatile Systems Volatility Framework 2.3_beta
Pid      Start              End                Flags              Pgoff Major  Minor  Inode      File Path                                                                     
-------- ------------------ ------------------ ------ ------------------ ------ ------ ---------- --------------------------------------------------------------------------------
    9425 0x0000000000400000 0x0000000000401000 r-x                  0x0    252      0    393333 /home/netadmin/asis-ctf                                                       
    9425 0x0000000000600000 0x0000000000601000 r--                  0x0    252      0    393333 /home/netadmin/asis-ctf                                                       
    9425 0x0000000000601000 0x0000000000602000 rw-                0x1000    252      0    393333 /home/netadmin/asis-ctf                                                       
    9425 0x00007fd496e34000 0x00007fd496fe9000 r-x                  0x0    252      0    791302 /lib/x86_64-linux-gnu/libc-2.15.so                                             
    9425 0x00007fd496fe9000 0x00007fd4971e8000 ---              0x1b5000    252      0    791302 /lib/x86_64-linux-gnu/libc-2.15.so                                             
    9425 0x00007fd4971e8000 0x00007fd4971ec000 r--              0x1b4000    252      0    791302 /lib/x86_64-linux-gnu/libc-2.15.so                                             
    9425 0x00007fd4971ec000 0x00007fd4971ee000 rw-              0x1b8000    252      0    791302 /lib/x86_64-linux-gnu/libc-2.15.so                                             
    9425 0x00007fd4971ee000 0x00007fd4971f3000 rw-                  0x0      0      0          0                                                                               
    9425 0x00007fd4971f3000 0x00007fd497215000 r-x                  0x0    252      0    791316 /lib/x86_64-linux-gnu/ld-2.15.so                                               
    9425 0x00007fd497408000 0x00007fd49740b000 rw-                  0x0      0      0          0                                                                               
    9425 0x00007fd497411000 0x00007fd497415000 rw-                  0x0      0      0          0                                                                               
    9425 0x00007fd497415000 0x00007fd497416000 r--              0x22000    252      0    791316 /lib/x86_64-linux-gnu/ld-2.15.so                                               
    9425 0x00007fd497416000 0x00007fd497418000 rw-              0x23000    252      0    791316 /lib/x86_64-linux-gnu/ld-2.15.so                                               
    9425 0x00007fff62ff0000 0x00007fff63012000 rw-                  0x0      0      0          0 [stack]                                                                       
    9425 0x00007fff63048000 0x00007fff63049000 r-x                  0x0      0      0          0                                                                               
```

``` sh
$ python vol.py --profile=LinuxUbuntu1204x64 -f mem.dump linux_dump_map -p 9425 -D dump
Volatile Systems Volatility Framework 2.3_beta
Task      VM Start          VM End                        Length Path
---------- ------------------ ------------------ ------------------ ----
      9425 0x0000000000400000 0x0000000000401000            0x1000 dump/task.9425.0x400000.vma
      9425 0x0000000000600000 0x0000000000601000            0x1000 dump/task.9425.0x600000.vma
      9425 0x0000000000601000 0x0000000000602000            0x1000 dump/task.9425.0x601000.vma
      9425 0x00007fd496e34000 0x00007fd496fe9000          0x1b5000 dump/task.9425.0x7fd496e34000.vma
      9425 0x00007fd496fe9000 0x00007fd4971e8000          0x1ff000 dump/task.9425.0x7fd496fe9000.vma
      9425 0x00007fd4971e8000 0x00007fd4971ec000            0x4000 dump/task.9425.0x7fd4971e8000.vma
      9425 0x00007fd4971ec000 0x00007fd4971ee000            0x2000 dump/task.9425.0x7fd4971ec000.vma
      9425 0x00007fd4971ee000 0x00007fd4971f3000            0x5000 dump/task.9425.0x7fd4971ee000.vma
      9425 0x00007fd4971f3000 0x00007fd497215000            0x22000 dump/task.9425.0x7fd4971f3000.vma
      9425 0x00007fd497408000 0x00007fd49740b000            0x3000 dump/task.9425.0x7fd497408000.vma
      9425 0x00007fd497411000 0x00007fd497415000            0x4000 dump/task.9425.0x7fd497411000.vma
      9425 0x00007fd497415000 0x00007fd497416000            0x1000 dump/task.9425.0x7fd497415000.vma
      9425 0x00007fd497416000 0x00007fd497418000            0x2000 dump/task.9425.0x7fd497416000.vma
      9425 0x00007fff62ff0000 0x00007fff63012000            0x22000 dump/task.9425.0x7fff62ff0000.vma
      9425 0x00007fff63048000 0x00007fff63049000            0x1000 dump/task.9425.0x7fff63048000.vma
```

The file task.9425.0x600000.vma contains the binary itself, lets open it in IDA.
The function sub_400644 is the main function, we can see, that the app asks "What do you want?" and if we input "flag", it prints out every second char from a variable in the stack initialized at the beginning of the function:

```
mov    [rbp+var_A0], 42h
mov    [rbp+var_A0+1], 49h
mov    [rbp+var_A0+2], 55h
mov    [rbp+var_A0+3], 52h
mov    [rbp+var_A0+4], 4Ch
mov    [rbp+var_A0+5], 41h
mov    [rbp+var_A0+6], 57h
mov    [rbp+var_A0+7], 4Eh
mov    [rbp+var_A0+8], 64h
mov    [rbp+var_A0+9], 5Fh
mov    [rbp+var_A0+0Ah], 69h
mov    [rbp+var_A0+0Bh], 37h
mov    [rbp+var_A0+0Ch], 69h
mov    [rbp+var_A0+0Dh], 31h
mov    [rbp+var_A0+0Eh], 3Eh
mov    [rbp+var_A0+0Fh], 63h
mov    [rbp+var_A0+10h], 6Bh
mov    [rbp+var_A0+11h], 65h
mov    [rbp+var_A0+12h], 6Ch
mov    [rbp+var_A0+13h], 33h
mov    [rbp+var_A0+14h], 3Bh
mov    [rbp+var_A0+15h], 34h
mov    [rbp+var_A0+16h], 3Dh
mov    [rbp+var_A0+17h], 65h
mov    [rbp+var_A0+18h], 3Fh
mov    [rbp+var_A0+19h], 65h
mov    [rbp+var_A0+1Ah], 6Fh
mov    [rbp+var_A0+1Bh], 63h
mov    [rbp+var_A0+1Ch], 47h
mov    [rbp+var_A0+1Dh], 31h
mov    [rbp+var_A0+1Eh], 75h
mov    [rbp+var_A0+1Fh], 36h
mov    [rbp+var_A0+20h], 72h
mov    [rbp+var_A0+21h], 66h
mov    [rbp+var_A0+22h], 42h
mov    [rbp+var_A0+23h], 62h
mov    [rbp+var_A0+24h], 4Ah
mov    [rbp+var_A0+25h], 65h
mov    [rbp+var_A0+26h], 75h
mov    [rbp+var_A0+27h], 39h
mov    [rbp+var_A0+28h], 49h
mov    [rbp+var_A0+29h], 66h
mov    [rbp+var_A0+2Ah], 48h
mov    [rbp+var_A0+2Bh], 34h
mov    [rbp+var_A0+2Ch], 4Dh
mov    [rbp+var_A0+2Dh], 32h
mov    [rbp+var_A0+2Eh], 4Ah
mov    [rbp+var_A0+2Fh], 34h
mov    [rbp+var_A0+30h], 4Eh
mov    [rbp+var_A0+31h], 37h
mov    [rbp+var_A0+32h], 4Eh
mov    [rbp+var_A0+33h], 32h
mov    [rbp+var_A0+34h], 4Dh
mov    [rbp+var_A0+35h], 35h
mov    [rbp+var_A0+36h], 55h
mov    [rbp+var_A0+37h], 65h
mov    [rbp+var_A0+38h], 50h
mov    [rbp+var_A0+39h], 37h
mov    [rbp+var_A0+3Ah], 82h
mov    [rbp+var_A0+3Bh], 32h
mov    [rbp+var_A0+3Ch], 84h
mov    [rbp+var_A0+3Dh], 61h
mov    [rbp+var_A0+3Eh], 52h
mov    [rbp+var_A0+3Fh], 35h
mov    [rbp+var_A0+40h], 83h
mov    [rbp+var_A0+41h], 39h
mov    [rbp+var_A0+42h], 85h
mov    [rbp+var_A0+43h], 61h
mov    [rbp+var_A0+44h], 53h
mov    [rbp+var_A0+45h], 34h
mov    [rbp+var_A0+46h], 89h
mov    [rbp+var_A0+47h], 39h
mov    [rbp+var_A0+48h], 8Bh
mov    [rbp+var_A0+49h], 64h
mov    [rbp+var_A0+4Ah], 26h
mov    [rbp+var_A0+4Bh], 0
```

Lets see, what happens if we do the same in python:

``` python
s = [0x42,0x49,0x55,0x52,0x4C,0x41,0x57,0x4E,0x64,0x5F,0x69,0x37,0x69,0x31,0x3E,0x63,0x6B,0x65,0x6C,0x33,0x3B,0x34,0x3D,0x65,0x3F,0x65,0x6F,0x63,0x47,0x31,0x75,0x36,0x72,0x66,0x42,0x62,0x4A,0x65,0x75,0x39,0x49,0x66,0x48,0x34,0x4D,0x32,0x4A,0x34,0x4E,0x37,0x4E,0x32,0x4D,0x35,0x55,0x65,0x50,0x37,0x82,0x32,0x84,0x61,0x52,0x35,0x83,0x39,0x85,0x61,0x53,0x34,0x89,0x39,0x8B,0x64,0x26]
print "".join(chr(i) for i in s[1::2])
#IRAN_71ce34eec16fbe9f424725e72a59a49d
```

Voila!

+= 220

[asis ctf 2013]: http://asis.io/
