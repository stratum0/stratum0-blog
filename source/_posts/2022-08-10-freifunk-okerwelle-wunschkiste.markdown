---
layout: post
title: "Freifunk Braunschweig in der Wunschkiste auf Radio Okerwelle"
date: 2022-08-10 20:00
comments: true
author: Chrissi
categories: [german, freifunk]
---


Chrissi^ und Rouven vom Freifunk Braunschweig waren zu Gast in der 
[Wunschkiste](https://okerwelle.de/sendung/wunschkiste/)
auf Radio Okerwelle.
In der Wunschkiste bekommen die Gäste zehn Fragen gestellt und haben danach die
Möglichkeit diese ausführlich zu beantworten.
Die Zeit zwischen den Fragen wird mit Musik aufgelockert - die sich die Gäste
selbst aussuchen dürfen.

Eine Aufzeichnung der Sendung gibt es im [Pressespiegel des Stratum 0](https://stratum0.org/wiki/Pressespiegel#2022).
Die Musik der Sendung kann in dieser [Youtube Playlist](https://www.youtube.com/playlist?list=PLF-Vmd5sVQZ9PkKFGaiIJ1cx7zVYzVBZC)
nachgehört werden.
