---
layout: post
title: "Online-Mitgliederversammlung und Briefwahl"
date: 2021-6-13 13:37
comments: true
author: larsan
categories: [german, stratum0]
---

Am 08. Mai 2021 fand unsere 12. Mitgliederversammlung statt, und zwar diesmal etwas anders als sonst. Hier ein kurzer Überblick wie wir sie über die Bühne gebracht haben und wie es weitergeht. Vielleicht als Vorlage für andere Vereine interessant.

Eigentlich hätte die Mitgliederversammlung turnusmäßig im Januar stattgefunden, es gab auch schon einen Termin, allerdings haben wir sie dann – zunächst ohne konkretes Datum – in den Sommer verschoben, in der Hoffnung eine Option auf eine Vor-Ort-Mitgliederversammlung (gegebenenfalls draußen) abhalten zu können. Als sich abzeichnete, dass das immer unwahrscheinlicher wurde, haben wir begonnen, unsere erste Online-MV zu planen.

{% fancyalbum 300x300 %}
images/posts/2021-05-24-online-mitgliederversammlung-und-briefwahl/mv-gruppenbild-censored.jpg
{% endfancyalbum %}


<!--more-->

Technisch umgesetzt wurde die online-Mitgliederversammlung durch:

* vorab ein selbst geschriebenes [Akkreditierungstool](https://akkreditierung.stratum0.org/) (Warteschlange + [Jitsi-Raum](https://jitsi.stratum0.org/) mit dem Akkreditierungsteam)
* [OpenSlides](https://mv.stratum0.org/) als Veranstaltungs-Management-Tool, frisch selbst gehostet auf dem [Vereinsserver](https://stratum0.org/wiki/Endur)
* ein [BigBlueButton-Raum](https://bbb.stratum0.org/) für die Veranstaltung an sich, ebenfalls neu gehostet auf dem Vereinsserver
* [Mumble](https://stratum0.org/wiki/Telmir) für die Koordination von Ablauffragen (und potentiellen -problemen) für die Versammlungsleitung
* eine nachgelagerte Briefwahl über die auf der Mitgliederversammlung aufgestellten Kandidaten

Vor der Mitgliederversammlung wurden alle Mitgliedsentitäten (inkl. Fördermitglieder) wie üblich per Mail zur Mitgliederversammlung eingeladen. Ordentliche Mitglieder bekamen darüber hinaus die Info, wie sie sich zur Versammlung akkreditieren können, um ihr Stimmrecht wahrzunehmen. Bei der Akkreditierung wurde auch die postalische Adresse zum Versand der Briefwahlunterlagen aufgenommen, da wir diese Daten normalerweise nicht benötigen und damit nicht erheben.
Insgesamt waren für die Mitgliederversammlung 44 von 102 ordentlichen Mitgliedern als stimmberechtigte Entitäten akkreditiert, das sind einige mehr als in den Jahren zuvor (32, 34, 36).
In einer der ersten Abstimmungen wurden, wie üblich, Gäste auf der Versammlung zugelassen. Danach wurde der "Zugriff für anonyme Gast-Nutzer" im OpenSlides aktiviert, die dort dann auch die Zugangsdaten zum BBB-Raum finden konnten. Dies wurde auf der Mailingliste verkündet.
Einfache Abstimmungen wurden nicht-namentlich im OpenSlides durchgeführt, während für die Vorstandswahl eine Briefwahl über die nächsten Wochen erfolgte. Die Erstellung der Kandidatenliste, sowie die Vorstellungs- und Fragerunden dafür fanden während der MV statt.

Für das Live-Protokoll(-pad) gab es wie gewohnt vorab einen Read-Only-Link.

Es wurden der Finanzbericht, Tätigkeitsbericht  des Vorstands, sowie der Jahresbericht des Vereins vorgetragen, diese finden sich mittlerweile auch auf der [Wikiseite der MV](https://stratum0.org/wiki/Mitgliederversammlung_2021-05-08). Themen waren u.A. der Umgang mit der Pandemie, Maker Vs Virus und der LASER!, genaueres im Protokoll. Im Anschluss gab es die Berichte der Vertrauensentität, sowie der Rechnungsprüfer.

Für die Wahl der Vorstandsmitglieder wurden in den Tagen nach der MV die Wahlunterlagen an die bei der Akkreditierung gesammelten Adressen versendet werden. Zunächst war der Plan, die Wahl in zwei Wochen in einem OpenSlides+BBB-Livestream auszuzählen, während der MV wurde aber auf derzeit sehr langsame Postlaufzeiten und einen Feiertag hingewiesen, weswegen die Auszählung auf 4 Wochen nach der MV verschoben wurde. Bis dahin ist der bisherige Vorstand weiter verwaltend im Amt.

{% fancyalbum 300x300 %}
images/posts/2021-05-24-online-mitgliederversammlung-und-briefwahl/wahlbriefe.jpg
{% endfancyalbum %}

Das ganze war auf dem Papier etwas zeitintensiver als eine konventionelle MV, Ende der Versammlung war um 18:28 und damit 1-1,5h später als sonst. Ich wage aber zu behaupten, dass sich das durch die zeitliche und räumliche Entzerrung einiger Komponenten ausglich, z.B.:

* durch die Änderung der Akkreditierung von "1h vor der MV, alle durch die Küche während der Space voll ist" zu "zwei Terminen über zwei Tage gestreckt"
* niemand stört beim Verlassen des Raumes
* die Luft im BBB-Raum wird nicht schlecht
* die Reise nach Braunschweig (oder auch nur der Weg in den Space) entfällt
* etc.

Dass die Versammlung so reibungslos über die Bühne gelaufen ist, ist vielen verschiedenen Entitäten zu verdanken, darunter insbesondere, aber nicht ausschließlich:

* shoragan für Aufsetzen der VMs, sowie Aufsetzen, Inbetriebnahme, Test von OpenSlides, BBB und Jitsi, und Beheben von Problemen, sowie Betreuen der MV auf organisatorischer Seite (Anlegen von Anträgen, Berechtigungen etc.)
* Chrissi^ und Kasa für das Akkreditierungstool und die Akkreditierung
* mist für die Wahlvorbereitung und Wahlleitung, sowie den Wahlhilfsentitäten
* allen, die dabei waren und über mehrere Stunden hinweg mit bis zu 46 Entitäten (und potentiell offenen Mikros) gleichzeitig in einem Raum eine ruhige und koordinierte Versammlung ermöglicht haben.

Danke dafür!

---


Am 5. Juni fand dann wieder im BBB-Raum die Auszählung statt. Von 44 Wahlbriefen sind 42 zurückgekommen, was wohl eine recht gute Beteiligung ist. Es gab keine Stimmengleichheiten, die eine Stichwahl erforderlich gemacht hätten. Alle gewählten Entitäten waren entweder direkt anwesend oder konnten telefonisch erreicht werden und haben ihre Wahl angenommen. Unser übliches Vorgehen für eine Mitgliederversammlung haben wir über die letzten Jahre in einem [Wiki-Artikel](https://stratum0.org/wiki/How_To_Mitgliederversammlung) dokumentiert und immer weiter verfeinert.

{% fancyalbum 300x300 %}
images/posts/2021-05-24-online-mitgliederversammlung-und-briefwahl/auszaehlung.jpg
{% endfancyalbum %}

