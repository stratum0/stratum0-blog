---
layout: post
title: "2. CoderDojo im Stratum 0"
date: 2014-08-30 21:11
comments: true
author: comawill
categories: [CoderDojo,Programming,german]
---
{% fancyimage left images/posts/2014-08-30-coderdojo/kuchen.jpg 180x180 CoderDojoBS - Kuchen (Foto: @CoderDojoBS)%}

Heute war es wieder soweit und 11 Kinder und Jugendliche trafen sich im Stratum 0 zum zweiten Braunschweiger CoderDojo. In nur drei Stunden wurden mit [Scratch][scratch] wieder viele Animationen, Spiele und Geräusche (**\*miau\***) programmiert.


<!--more-->
<p style="clear:both" />

In den drei Stunden standen den Kindern Mentoren zur Seite, die dabei halfen die eigenen Ideen umzusetzen, oder auch an die Vorkenntnisse des jeweiligen Kindes angepasst Vorschläge zu machen, oder neue Richtungen aufzuzeigen. Da die Möglichkeiten mit [Scratch][scratch] trotz der Einsteigerfreundlichkeit nahezu unendlich sind wurde erneut in der abschließenden Präsentationsrunde gezeigt. Von kleinen Spielen, animierten Kurzgeschichten bis zum Zustandsautomaten war alles dabei.


{% fancyimage center images/posts/2014-08-30-coderdojo/chillraum.jpg 500x500 Programmieren im Chillraum (Foto: CC-BY-SA Emantor)%}

{% fancyimage center images/posts/2014-08-30-coderdojo/frickelraum.jpg 500x500 Programmieren im Frickelraum (Foto: CC-BY-SA Emantor)%}

Wie auch beim letzten Mal wäre das alles ohne die tatkräftige Hilfe der fleißigen Mentoren nicht möglich gewesen.
Herzlichen Dank hierfür! Besonders hervorzuheben ist auch die Organisation durch Anke und Ersoy, die den Mentoren mit den selbst produzierten T-Shirts eine riesige Freude bereitet haben.



Das nächste CoderDojo in Braunschweig findet am Sonntag, den 28.9 um 14.00 erneut im Stratum 0 statt.
Weitere Infos hierzu finden sich im [Wiki][wiki], auf der [Veranstaltungsseite][coderdojobs] sowie auf [Twitter][twitter].

Jedes Kind, das gerne mal selber ausprobieren möchte, was man alles selber am Computer anstellen kann, ist herzlich eingeladen beim CoderDojo vorbei zu schauen.
Wer selbst kein Kind mehr ist, aber gerne sein Grundwissen an Programmierung (keine Sorge, ein abgeschlossenes Informatikstudium ist nicht notwendig) weitergeben möchte, ist herzlich eingeladen, die Kinder als Mentor zu unterstützen. Zur einfacheren Organisation kommunizieren wir über die Mailingliste [DojoOrga][dojoorga].



[coderdojobs]: https://zen.coderdojo.com/dojo/820
[scratch]: http://scratch.mit.edu/
[wiki]: https://stratum0.org/wiki/CoderDojo
[twitter]: https://twitter.com/CoderDojoBS
[dojoorga]: https://lists.stratum0.org/mailman/listinfo/dojoorga
