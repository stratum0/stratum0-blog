---
layout: post
title: "Octopress Workflow"
date: 2013-09-08 00:01
comments: true
author: valodim
categories: [german,meta, howto]
---

Weil viel magic involviert ist, halte ich hier mal kurz fest, was man fürs
Bloggen mit Octopress wissen muss. Schritt 1 ist, falls nicht schon geschehen,
einen Account im [Stratum 0 GitLab][gitlab] anzulegen und einem Owner der
Gruppe [stratum0][gl-stratum0] Bescheid zu sagen, um in die Gruppe aufgenommen
zu werden und Schreibrechte zu erhalten.

Octopress ist im Prinzip ein Framework für den Webseitengenerator Jekyll,
welcher wiederum in Ruby implementiert ist. Octopress setzt außerdem auf Git
als versionsverwaltendenden Datenspeicher. Also benötigte Software installieren
und das Git-Repo klonen:

``` sh Setup für Debian-basierte Systeme
sudo apt-get install openssh-client git ruby ruby-dev bundler imagemagick
git clone ssh://git@gitli.stratum0.org/stratum0/stratum0-blog.git
cd stratum0-blog; bundle install --deployment
```

(Falls das jemand auf Windows oder Mac OS schon erfolgreich ausprobiert hat,
immer her mit einer Anleitung :))

``` sh Posts erstellen und editieren
bundle exec rake preview
firefox http://localhost:4000/blog
bundle exec rake new_post"[New Post Title]"
vim source/_posts/2013-09-08-new-post-title.markdown
```

{% img left /images/twi/acrocast.png %}
Solange der rake-preview-Prozess läuft, werden Änderungen an allen Source-Files
automatisch und inkrementell geupdatet.

Die verwendete Markdown-Implementierung ist [Kramdown] (Quick Reference dort).
Prinzipiell funktioniert aber auch Nachgucken in anderen Posts ziemlich gut.
Wie zum Beispiel in diesem hier für Bilder, Codeschnipsel und Links :)

Weitere nützliche Referenzen:

* [Pro Git], Einführung in das Arbeiten mit Git
* [Jekyll Documentation], ausführliche Infos darüber, wie man z.B. Posts
	schreibt oder Plugins baut
* [Octopress Documentation], insbesondere die Abschnitte „Using Octopress“ und
	„Octopress Plugins - Usage & Examples“

Last but not least, den eben angelegten Post committen und aufs GitLab pushen:

``` sh Publishing
git add source/_posts/2013-09-08-post-title.markdown
git commit -m 'post: title'
git push
```

[gitlab]: https://gitli.stratum0.org/users/sign_in "Stratum 0 GitLab"
[gl-stratum0]: https://gitli.stratum0.org/groups/stratum0/group_members "GitLab: stratum0 group members"
[kramdown]: http://kramdown.gettalong.org/quickref.html "Kramdown Quick Reference"
[Pro Git]: https://www.git-scm.com/book/en/v2 "Pro Git book"
[Jekyll Documentation]: https://jekyllrb.com/docs/ "Jekyll: Documentation"
[Octopress Documentation]: http://octopress.org/docs/ "Octopress Documentation"
