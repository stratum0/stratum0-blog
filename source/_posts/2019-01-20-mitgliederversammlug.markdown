---
layout: post
title: "Mitgliederversammlung 2019"
date: 2019-01-20 20:00
comments: true
author: chrissi^
categories: [german, Stratum0]
---

Am 20.01.2019 hat die diesjährige Mitgliederversammlung des Stratum 0 
stattgefunden.
Aktuell gibt es nur das live mitgeschriebene [Protokoll].
Die ins Reine geschriebene Version wird noch folgen.

Beschlussfähigkeit
-----------------------

Die Mitgliederversammlung hat das notwendige Quorum erreicht und war daher
nach Satzung beschlussfähig.
Nach den Berichten des Vorstandes und der [Vertrauensperson] wurde der
alte Vorstand entlastet.

Space 3.0
----------

Anschließend wurde diskutiert wie sich die Mitgliederversammlung die Communitiy
im Space vorstellt und welche Erwartungen an einen Space in fünf Jahren
gestellt werden.
Die Diskussion hat gezeigt, dass es hier verschiedene Erwartungen und
Wahrnehmungen an das Leben im Space gibt.
Konsens war, dass für die weitere Entwicklung eine Vergrößerung des Spaces
förderlich wäre.

[Protokoll]: https://pad.stratum0.org/p/r.a4792fe4aa7bca2476fb92afe4b72171
[Vertrauensperson]: https://stratum0.org/wiki/Arbeitsgruppe:Gesch%C3%A4ftsf%C3%BChrung

Wahlen
-------

Anschließend wurde ein neuer Vorstand gewählt.
Der neue Vorstand besteht aus:

* larsan (Lars Andresen) als 1. Vorsitzender
* reneger (René Stegmaier) als 2. Vorsitzender
* Emantor (Rouven Czerwinski) als Schatzmeister
* chamaeleon (Linda Fliß), Chaosrambo (Daniel Thümen) und Chrissi^ (Chris Fiege) als Beisitzer

blinry (Sebastian Morr) und rohieb (Roland Hieber) scheiden damit aus dem 
Vorstand aus.
Der neue Vorstand dankt blinry und rohieb für ihre Arbeit. 
Dies gilt besonders für rohieb, der seit der Gründung des Stratum 0 im Vorstand
aktiv war.

Als Rechnungsprüfer wurden shoragan (Jan Lübbe) und sonnenschein 
(Angela Schmitt) bestätigt.

dStulle (Daniel Sturm) wurde von der Mitgliederversammlung als Vertrauensperson 
bestätigt.
