---
layout: post
title: "Space 2.0 - Kurz vorher"
date: 2013-09-28 17:00
comments: true
author: valodim
tldr: 
categories: [german,space2.0]
---

Fast zwei Jahre ist es her, dass wir den Mietvertrag für unseren
[ersten Space][space] unterschroben. Ganze $63 \,\mathrm{m}^2$ groß, mit Platz
für drei Sofata, einige Arbeitsplätze, einen Konferenztisch, eine vernünftige
Küche, und ein Bad mit Dusche. Genau unsere Kragenweite - zu der Zeit
jedenfalls.

Gefühlte drei Wochen später war der Platz zu knapp. Zuwachs an Mitgliedern,
Hardwarespenden sowie Veranstaltungen wachsenden Ausmaßes führten schnell dazu,
dass Platzmangel ein großes Bottleneck für viele Aspekte des Vereinslebens
wurde. Ich kann mich erinnern, auf der
[Large Hackerspace Convention III][lhciii] im Sublab Mitte letzten Jahres
einige Häme kassiert zu haben, als wir erzählt hatten dass wir unser ganzes
Geld garnicht sinnvoll ausgeben können weil wir keinen Platz für noch mehr
Stuff haben.

Februar 2013 erhielten wir ein interessantes Angebot für eine neue
Räumlichkeit, im gleichen Gebäude (also in bewährter Lage), aber mit $140
\,\mathrm{m}^2$ mehr als doppelt so groß wie der vorige. Am 16. März darauf
beschloss die Mitgliederversammlung nach einiger Planungsarbeit, den neuen
Space zu mieten - nach damaliger Kalkulation mit recht niedrigem Mietpreis,
dafür aber einer Investition von einigen tausend Euro und einigen hundert
Personenstunden an Sanierungsarbeiten inklusive Einbau mehrerer neuer Wände.

Das nächste halbe Jahr verbrachte die Arbeitsgruppe Space 2.0 mit zähfließenden
Verhandlungen über die genauen Vertragsbedingungen. Am Ende unterzeichneten wir
am 18.07. einen Mietvertrag zum 01. Oktober, mit einem höheren Mietzins als
ursprünglich angesetzt, dafür aber einem deutlichen Entgegenkommen des
Vermieters in Sachen Renovierungsarbeiten.

{% fancyimage center images/posts/2013-09-28-space-2-dot-0-vorher/space2_grundriss.png 600x600 Space 2.0 Grundriss %}

<p style="clear:both" />


{% fancyalbum 150x150 %}
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1182.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1183.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1184.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1185.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1186.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1187.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1188.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1189.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1190.jpg:
images/posts/2013-09-28-space-2-dot-0-vorher/DSCN1192.jpg:
{% endfancyalbum %}



Anfang letzter Woche erhielten wir dann endlich einen Schlüssel. Leider sind
die vertraglich zugesicherten Arbeiten zum Zeitpunkt dieses Schreibens noch
nicht durchgeführt, unser Einzug wird sich noch um einige Zeit verzögern. Wir
sind aber zuversichtlich, dass Anfang und Ende der Umzugsarbeiten noch im
Oktober liegen werden.

[space]: https://stratum0.org/wiki/Space "Space"
[lhciii]: http://sublab.org/lhciii "Large Hackerspace Convention III"
