---
layout: post
title: "Stratumnews Vol. 16"
author: Stratumnews Team
date: 2017-03-03 23:10
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space.
<!--more-->

Spendendose vermisst!
---------------------
Zwischen letztem Samstag, 25. Februar, 22:00 und Sonntag, 4:00 ist die [Spendendose][spd] aus dem Flur [gestohlen worden][spw]. Zum Glück wurde sie kurz vorher geleert, sodass sich der geschätzte Verlust auf einen ein- bis niedrigen zweistelligen Betrag beschränken sollte, sowie den Zeitaufwand für den Bau der Dose :-( Wir überlegen derzeit, wie wir dem Problem zukünftig begegnen können, ohne auf unsere Offene-Tür-Philosophie verzichten zu müssen.

[spd]: https://twitter.com/daniel_bohrer/status/830212739746381829
[spw]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/QgXtSMPwqHqVLwXadOrunPti


Xbox360 im Space
-----------------
Im Space wurde eine Xbox360 gedroppt, dazu ein kabelgebundener, sowie ein kabelloser Controller mit USB-Empfänger. Vielen Dank!


Werkstatt
---------
Festool hat uns jede Menge Werkzeug gespendet \o/ In der Werkstatt steht nun ein Aufbewahrungsturm für das [CMS-Modulsystem][cms] ([Video][cmv],9 min) mit Bandschleifer und Tisch-/Tauchsäge sowie ein mobiler Sauger. Die Grundeinheit zum Draufbauen ist allerdings noch unterwegs… Handbücher liegen wie üblich im RTFM-Ordner im Frickelraumregal.

Es wurden neue Sägeblätter für die Bandsäge gekauft, sie liegen auf dem Regal über der Fräse. Neben Ersatz für alte, rostige, unscharfe Holz-Sägeblätter gibt es nun auch ein Sägeblatt für weiche Metalle. Leider sind die Vibrationen bei Benutzung der Bandsäge (siehe Stratumnews #14) mit der Zeit wieder aufgetaucht, sodass womöglich demnächst eine Achse getauscht werden muss… Bitte vorerst mit Vorsicht sägen.

[cms]: https://www.festool.de/produkte/halbstationaeres-arbeiten
[cmv]: https://www.youtube.com/watch?v=KqXYYxXN7Tw


Frickelraum
-----------
Eine noch offene Idee vom letzten Spacebauathon (siehe Stratumnews #14) war ein Aufbewahrungsregal für den Dymo-Labeldrucker. Diese Idee ist nun neben der Stickmaschine [umgesetzt worden][dym].
Der Schneidplotter hat einen neuen Unterbau mit mehr [Stauraum bekommen][sch].

[dym]: https://twitter.com/daniel_bohrer/status/831911687771004929
[sch]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/PmWbijlEXEKjkomYDBGZKKmi


3D-Drucker
----------
Für den LulzBot wurden sechs Rollen PLA in mehreren Farben angeschafft. Ziel dabei ist es, für unser Hauptarbeitstier immer eine feste, vorhersehbare Auswahl von Farben in leicht verarbeitbarem Material vorrätig zu halten, um das Drucken wieder attraktiver zu machen. Falls ihr euch über die Vielzahl an vorhandenen Rollen wundert: beim übrigen Filament handelt es sich um PLA für die beiden anderen vorhandenen Drucker, die einen kleineren Durchmesser benötigen, und welches vom LulzBot nicht verarbeitet werden kann, und um temperaturempfindlicheres ABS, das aus diesem Grund eher für Fortgeschrittene 3D-Drucker-Bedienende geeignet ist. Da PLA leicht altert und brüchig wird, sollte es immer feuchtigkeitsgeschützt in der großen Samla-Box auf dem Tisch aufbewahrt werden.

Die Verbrauchspreise für 3D-Druck-Material sind nun in der Weboberfläche vom Repetier-Server hinterlegt und können bei jedem Modell beim Klick auf das kleine Info-Icon angezeigt werden. Darin enthalten ist auch eine Pauschale von 20 Cent pro Druckstunde für Stromverbrauch und Abnutzung. Die Preise sind als Spendenempfehlungen zu verstehen, damit sich das System auf lange Sicht weiterhin selbst finanzieren kann.

Küche
-----
Wer die Box mit Matetags sucht, sie hängt nun passend in der Küche links am Getränkeregal.

Die Matestrichlisten wurden etwas defragmentiert. Dabei wurden Zettel #100, #200 und #400 wegoptimiert und einzelne verbliebene Zeilen und Striche auf andere Zettel übertragen. Eine zusätzliche, platzsparendere, Android-Tablet-basierte Lösung hängt am Kühlschrank und ist gerade im Alpha-Betrieb :)


Veranstaltungen
---------------
Am letzten Vierzehnten gab es Vorträge zu JSON auf der Kommandozeile, Dokumentverarbeitung mit pandoc, D-Bus und xss-lock, sowie Wärmeleitung für Elektrotechniker. Material dazu gibt es wie immer [im Wiki][vor]. Aber: Nach den Vorträgen ist vor den Vorträgen, und möchtest du nicht vielleicht einen [Vortrag halten][von]? :-)

[vor]: https://stratum0.org/wiki/talks-2017-02-14
[von]: https://stratum0.org/wiki/Vortr%C3%A4ge

Die #marudor2017 wurde erfolgreich eskaliert. Am Tag zuvor waren die Tickets bereits ausverkauft. Durch eine kleine Unaufmerksamkeit wurde in den Kalender eingetragen, dass sie um 12 Uhr beginne. So waren dann auch die ersten Gäste von nah und fern ab 11:30 vor Ort. Es gab Tschunk, Clunk, Schaschlik, Vorträge (in Saal 1) und Musik.

Die #kasalehlia2018 wird hingegen eine Deeskalation. Gerüchten zufolge wird es aber eine Aftershowparty geben.

Es wurde wieder fleißig Bier gebraut. Aktuell wird nach einem Anlass in einigen Wochen gesucht, zu dem mal ein Fässchen angestochen werden könnte. Vorschläge?


Termine
-------
* Mo, 06.03. 19:00: HOA: Orgatreffen
* Di, 07.03. 18:00: Malkränzchen
* Di, 07.03. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Mi, 08.03. 19:00: Freifunk-Treffen
* Mo, 13.03. 19:00: Vorstandssitzung 2017-03-13
* Di, 14.03. 18:00: Malkränzchen
* Di, 14.03. 19:00: Vorträge
* Di, 14.03. 19:00 - 23:00: Anime Referat
* Mi, 15.03. 19:00: Freifunk-Treffen
* Mi, 15.03. 19:00: Vegan Academy
* Do, 16.03. 19:00 - 23:00: Captain's Log
* Di, 21.03. 18:00: Malkränzchen
* Di, 21.03. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Mi, 22.03. 19:00: Freifunk-Treffen
* Di, 28.03. 19:00 - 23:00: Anime Referat
* Mi, 29.03. 19:00: Vegan Academy
* Do, 30.03. 19:00 - 23:00: Captain's Log
* Do, 25.05. bis So, 28.05.: GPN17, Karlsruhe


Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!

