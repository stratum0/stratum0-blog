---
layout: post
title: "rC3: Remote Sticker Exchange"
date: 2020-11-19 20:30
comments: true
author: Chrissi
categories: [german, stratum0, rc3]
banner:
  img: images/header/aufkleber.jpg
  author: Daniel Bohrer
  license: CC BY-SA 3.0
  source: "https://stratum0.org/wiki/Datei:Aufkleber.jpg"
---

Wer kennt es nicht: Das Jahr neigt sich dem Ende und langsam ist die oberste Schicht Aufkleber auf dem Notebook echt zerkratzt.
Eigentlich kommt dann bald der Congress: Überall Stickerboxen! Eine Wohltat für den Notebookdeckel.

Damit dein Notebook dieses Jahr nicht leer ausgeht, schließen wir uns den Anderen da draußen
([Stickeroperation.center](https://stickeroperation.center/2020/10/26/c3-sticker-exchange/),
[RZL](https://raumzeitlabor.de/blog/Wir-kleben-weiter/)) an und bieten einen
[Snail Mail](https://de.wikipedia.org/wiki/Schneckenpost)-Sticker-Exchange an \o/

Wir planen Rücksendungen zwischen dem 07.12.2020 und dem 04.01.2021.
Du kannst uns deinen Rückumschlag natürlich auch schon vorher schicken.

{% fancyalbum 300x300 %}
images/posts/2020-11-19-rC3-remote-sticker-exchange/stickerbox.jpg: Die Stratum 0 Stickerbox. (CC-BY vidister)
{% endfancyalbum %}

Wie Du an Sticker kommst
------------------------

* Sende uns bis spätestens 04.01.2021 einen frankierten Rückumschlag.
* Dein Rückumschlag sollte nicht zu klein sein.
  Als Empfänger gibst Du hierauf Deine Adresse an.
* Als Porto sollte ein Kompaktbrief (0,95 EUR, 50 g, bis C5/C6) ausreichend sein.
  Falls Du möchtest, kannst Du ihn auch als Großbrief (1,55 EUR, 500g, bis C4) frankieren.
* Den Umschlag schickst du dann in einem weiteren Umschlag an den Stratum 0:

| Stratum 0 e.V.
| "Stickerexchange"
| Hamburger Straße 273a
| 38114 Braunschweig


Wie Du Sticker verschenken kannst
---------------------------------

Du hast Sticker und möchtest sie gern bei unserer Aktion verteilen?
Gib [Chrissi^](https://stratum0.org/wiki/Benutzer:Chrissi%5E) Bescheid und bring
deine Sticker bis zum 07.12.2020 in den Space.
Falls welche übrig bleiben, kannst Du sie entweder nach dem 04.01.2021 abholen, oder
wir lassen sie irgendwann in den Space-Fundus übergehen.
