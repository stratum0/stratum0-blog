---
layout: post
title: "Freifunk Braunschweig Event WLAN"
date: 2018-06-01 18:00
comments: true
author: [chrissi]
categories: [german, freifunk]
banner:
  img: images/posts/2014-09-04-freifunk/header.jpg
  source: http://de.wikipedia.org/wiki/Datei:Freifunk-Initiative_in_Berlin-Kreuzberg.jpg
  license: CC BY-SA 3.0
  author: Boris Niehaus (JUST)
---

Freifunk Braunschweig hat, gefördert durch die Stadt Braunschweig, WLAN-Equipment für Veranstaltungen angeschafft. Mittlerweile ist die Inbetriebnahme
abgeschlossen und die Ausstattung ist bereit für die erste größere 
Veranstaltung.

{% fancyalbum 500x800 %}
images/posts/2018-06-01-freifunk-event-wlan/20180527143648.jpg: Accesspoints des Event-WLAN
{% endfancyalbum %}

Hintergrund
--------------

Der Rat der Stadt Braunschweig verfolgt seit einiger Zeit das Ziel, mehr
WLAN in der Stadt anzubieten. Ein Baustein dabei ist die Kooperation der Stadt
mit HTP und BS-Energy. Zusammen bieten diese an einigen zentralen Punkten der
Innenstadt zeitlich begrenzten und zensierten Internetzugang an.

Als ein weiterer Baustein wird auch Freifunk
Braunschweig gefördert. Der Rat der Stadt hat mit dem Beschluss
[16-03066] Freifunk Braunschweig insgesamt 4.500 € zugesagt.

[16-03066]: https://ratsinfo.braunschweig.de/bi/vo020.asp?VOLFDNR=1004051

Freifunk Braunschweig hat daraufhin ein Konzept
für den Einsatz des Geldes erarbeitet:

* Mit einem Teil des Geldes soll Hardware für das Testen von Freifunk-
  Firmware auf realen Geräten beschafft und aufgebaut werden. Die so geschaffene
  Infrastruktur kann z.B. mit einer Embedded Testing Suite wie [Labgrid]
  benutzt werden, um die weitere Entwicklung zu vereinfachen.
* Der größere Teil des Geldes soll für die Anschaffung von WLAN-Equipment für Events ausgegeben werden.
  
[Labgrid]: https://labgrid.org

Die Anschaffung von Event-WLAN bietet folgende Vorteile:

* Durch den Einsatz von Freifunk auf Veranstaltungen wird die Idee
  des offenen WLANs weiter beworben.
* Wenn die Geräte an einer zentralen Stelle verfügbar sind stehen diese auch
  Nutzern zur Verfügung, für die sich die Anschaffung selber nicht lohnen
  würde.
  

Hardware
--------

{% fancyalbum 500x800 %}
images/posts/2018-06-01-freifunk-event-wlan/20180531192910.jpg: Richtfunkstrecke für größere Distanzen
{% endfancyalbum %}

Bei der Auswahl der WLAN-Hardware wurden folgende Anforderungen
berücksichtigt:

* Es soll die Versorgung von mehreren 100 Geräten mit WLAN möglich sein.
* Die Geräte sollten permanent oder zeitweise für den Einsatz 
  im Außenbereich geeignet sein.
* Im Paket sollte das notwendige Zubehör wie Ethernet-Switche, Netzwerkkabel
  und ein Mini-Server als Controller enthalten sein.
* Es sollten ausreichend Geräte zum Versorgen größerer Flächen, wie z.B.
  bei Veranstaltungen wie dem [Hacken Open Air] oder im 
  [nördlichen Bürgerpark] vorhanden sein.
  

[Hacken Open Air]: https://hackenopenair.de
[nördlichen Bürgerpark]: https://www.openstreetmap.org/?mlat=52.25762&mlon=10.52155#map=18/52.25762/10.52155
[Ubiquiti]: http://ubnt.com/

Die Auswahl fiel auf Geräte von [Ubiquiti]. Beschafft wurde folgende Hardware:

* 2x Ubiquiti [NanoBeam ac Gen2]: Für Richtfunkstrecken
* 4x Ubiquiti [UAP-AC-Pro]: Indoor-Accesspoints
* 7x Ubiquiti [UAP-AC-M]: Outdoor-Accesspoints
* 5x Ubiquiti [UAP-AC-M-Pro]: Outdoor-Accesspoints
* 3x Ubiquiti [UAP-AC-Lite]: Indoor-Accesspoint
* PCEngines [APU2C4]: Mini-Server
* 3x [ZyXEL GS1900] Managed PoE-Switch mit 8 Kupfer-Ports und 2 Faser-Ports
* Huawei e5885 LTE Router
* Netzwerkkabel: 20x 0,5 m; 5x 20 m; 5x 10 m; 10x 5 m
* Mehrfachsteckdosen
* Boxen zum Transport

[NanoBeam ac Gen2]: https://www.ubnt.com/airmax/nanobeam-ac-gen2/
[UAP-AC-Pro]: https://www.ubnt.com/unifi/unifi-ap-ac-pro/
[UAP-AC-M]: http://dl-origin.ubnt.com/datasheets/unifi/UniFi_AC_Mesh_DS.pdf
[UAP-AC-M-PRO]: http://dl-origin.ubnt.com/datasheets/unifi/UniFi_AC_Mesh_DS.pdf
[UAP-AC-Lite]: https://www.ubnt.com/unifi/unifi-ap-ac-lite/
[APU2C4]: https://www.pcengines.ch/apu2c4.htm
[ZyXEL GS1900]: https://www.zyxel.com/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/

Inbetriebnahme
--------------

Nachdem die gesamte Hardware eingetroffen war, wurde das System 
an insgesamt drei Treffen in Betrieb genommen.

Beim ersten Treffen
wurden Geräte inventarisiert und Kabel mit Schildern versehen. Damit soll
sichergestellt werden, dass alle Geräte ihren Weg zurück zum Braunschweger
Event-WLAN finden. Kabel länger als 1 m wurden darüber hinaus an beiden Enden
mit Namen versehen. So ist es möglich auch bei vielen Kabeln noch beide 
Enden zuordnen zu können.

Beim nächsten Treffen wurde der Server und der
WLAN-Controllers eingerichtet. Bei diesem Treffen wurden darüber hinaus alle 
Geräte in
Betrieb genommen. Außerdem wurden Konzepte für den Aufbau und den Betrieb
eines Event-WLAN-Netzes erarbeitet.

Beim letzten Treffen wurde die (Fern-) Wartbarkeit des Systems verbessert.
Das Event-Netz war nun bereit für einen ersten größeren Test.

Testbetrieb
-----------

{% fancyalbum 500x800 %}
images/posts/2018-06-01-freifunk-event-wlan/graph.png: Beim Testbetrieb waren zeitweise über 40 Geräte mit dem WLAN verbunden.
{% endfancyalbum %}

Ein Test konnte auf einer Hacker-Veranstaltung durchgeführt werden. Hier waren
fast 30 Personen mit diversen Geräten anwesend. Es wurden mehrere Gebäude,
sowie Außenbereiche mit WLAN versorgt. In der größten Ausbaustufe waren
elf Accesspoints in Betrieb und versorgen über 40 Clients mit Netzwerkzugang.
Auf dem Event wurden in vier Tagen weit über 100 GB Daten über das WLAN
ausgetauscht.

Das Feedback zur Qualität des WLAN auf dieser Veranstaltung war durchaus
positiv. Beim Auf- und Abbau hat sich die Vorbereitung der vorherigen
Treffen als gut erwiesen.


WLAN auf Deinem Event?
---------------------------

{% fancyalbum 500x800 %}
images/posts/2018-06-01-freifunk-event-wlan/20180601120418.jpg: Outdoor-Accesspoint während des Testaufbaus
{% endfancyalbum %}

Mit dieser Hardware ist Freifunk in der Lage WLAN auf
mittleren Events zur Verfügung zu stellen. Wir sind nun auf der Suche
nach Veranstaltungen im Stadtgebiet, auf denen die Technik getestet
werden kann.

Kontakt kann über kontakt@freifunk-bs.de hergestellt werden.
