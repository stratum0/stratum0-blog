---
layout: post
title: "Stratum 0 vs Virus"
date: 2020-03-30 23:30
comments: true
author: larsan
categories: [german, stratum0, covid19]
---

## Update 01.04.2020

Nach interner Klärung können wir nun Spenden für Material für die 
Gesichtsschutzschirme, sowie für die Weiterentwicklung entgegen nehmen.

Da der Stratum 0 e.V. gemeinnützig ist können diese Spenden steuerlich
geltend gemacht werden.
Für Spenden bis 200 EUR reicht i.d.R.. der Kontoauszug als Beleg gegenüber dem
Finanzamt.
Bei größeren Spenden setzt euch bitte unter vorstand@stratum0.org mit uns in
Verbindung.

Hier geht's zu unserer [Bankverbindung](https://stratum0.org/wiki/Spenden).
Bitte nutzt als Verwendungszweck *MakerVsVirus* oder 
*Material 3D-Druck und Lasercutter*, damit wir das richtig zuordnen können.

Im Laufe des Abends werden wir den aktuellen Spendenstand auch auf
[data.stratum0.org](https://data.stratum0.org/finanz/) unter
*aktuelle Zweckbindung* veröffentlichen.

Spendengelder, die wir nicht für den *Hub Braunschweig* ausgeben, gehen zunächst
an andere gemeinnützige Hubs.
Sollte trotzdem Geld übrig bleiben geben wir das an andere gemeinnützige
Hilfsorganisationen in der Region weiter.

## Gesichtsschutzschirme

> tl;dr: Wir haben uns dem MakerVsVirus.org Hub für Braunschweig und Umgebung 
> angeschlossen und produzieren jetzt Gesichtsschutzschirme für Ärzte und Klinken.
> Die Situation entwickelt sich gerade sehr schnell. 
> Wir versuchen hier die ersten Informationen (mehr oder weniger strukturiert) 
> zusammenzutragen.

Vor einigen Wochen ging in Maker-Kreisen rum, ob man nicht mit vorhandenen 
Mitteln Schutzmasken herstellen könnte – anfangs ging es da primär um 
Filtermasken.
Das ist sowohl hygienisch als auch rechtlich kritisch zu 
[betrachten](https://www.it-recht-kanzlei.de/corona-virus-atemschutz-mundschutz-selbstgemacht.html) –
auch weil Mundschutze für diesen Einsatzzweck wohl eher als Medizinprodukt 
zu werten sind.

Wesentlich unkritischer, weil zum einen (nur) persönliche Schutzausrüstung, 
zum andere vor Ort (im Krankenhaus, bei Ärzten, etc.) desinfizierbar, 
sind dagegen zum Beispiel 
[Gesichtsschutzschirme](https://www.prusaprinters.org/prints/25857-protective-face-shield-rc2).
Diese sind mit Laser, 3D-Drucker, Cutter, oder auch mit Schneidemaschine 
und Schere etc. recht gut konfektionierbar.

{% fancyalbum 300x300 %}
images/posts/2020-03-30-stratumvsvirus/IMG20200327161253.jpg: Gesichtsschutzschirme (noch ohne unteren Halter und Gummi)
images/posts/2020-03-30-stratumvsvirus/IMG20200327162147.jpg: Mitte: Ein fertiger Gesichtsschutzschirm
images/posts/2020-03-30-stratumvsvirus/IMG20200327170356.jpg: Gesichtsschutzschirme und zugeschnittene Schirme
{% endfancyalbum %} 

Gesichtsschutzschirme werden bei der Behandlung von Patienten in der
persönlichen Schutzausrüstung der Behandelnden verwendet um den Kontakt mit vom 
Patienten abgegebenen Flüssigkeiten zu verhindern.
Sie ergänzen andere Maßnahmen, wie z.B. den Mundschutz.

## MakerVsVirus

Um diese Gesichtsschutzschirme geht es auch (hauptsächlich) bei 
[MakerVsVirus](https://www.makervsvirus.org/de/).
Auf diese Initiative wurden wir vor einigen Tagen auf unserer 
[Mailingliste](https://lists.stratum0.org/mailman/private/normalverteiler/Week-of-Mon-20200323/009664.html) (Login notwendig) 
aufmerksam gemacht. 
Das eskaliert gerade etwas. 
Gefühlt sind alle in der Region, die irgendwie Zugriff auf 3D-Drucker haben, 
am Produzieren.

{% fancyalbum 300x300 %}
images/posts/2020-03-30-stratumvsvirus/IMG20200328232758.jpg: Prusa Kopfteil im Druck
images/posts/2020-03-30-stratumvsvirus/IMG20200327194426.jpg: Die 3D-Druck-Teile sind zur Zeit der Flaschenhals, da dieser Produktionsschritt der langsamste ist.
images/posts/2020-03-30-stratumvsvirus/IMG20200327194532.jpg: Fertiger Halter mit Aufnahmen für den Schutzschirm
{% endfancyalbum %}

Die Koordination des Hubs für Braunschweig und Umgebung findet (natürlich) 
online statt. 
Wer sich uns anschließen möchte, klickt [hier](https://www.makervsvirus.org/de/)
ganz unten auf der Seite 
auf "Slack beitreten" und tritt dann dort dem Channel *#hub-braunschweig* bei.

## Produktion und Finanzen

Am Donnerstag haben wir angefangen Folienmaterial zu suchen, am Freitag haben 
wir die ersten Schirme gelasert, am Samstag gingen die ersten Schirme an das 
[Herzogin Elisabeth Hospital](https://www.heh-bs.de/) (HEH)
und am Sonntag wurden erste Teile zu Desinfektionstests ins Klinikum 
Wolfsburg geliefert.
Die Zahl der Involvierten ist in wenigen Tagen von 0 auf über <s>50</s> 80 Leuten und 
einigen Instituten (TU/DLR) in der Region gestiegen.
Es scheint, als sei das gerade erst der Anfang. 
Am Samstag hatte der Hub eine überschlagene Produktionskapazität von 100 
Schirmen am Tag, am Sonntag waren es 230, Tendenz steigend.
Das ganze Unterfangen hat gerade noch recht viel Entropie, ist dafür extrem 
flexibel.

Material wird gemeinsam beschafft, dafür wurde ein 
[PayPal-Pool](https://www.paypal.com/pools/c/8nIcxOA4h8) eingerichtet,
der genauso in wenigen Tagen eskaliert ist wie alles andere gerade.
Im Space haben wir, um kurzfristig handlungsfähig zu sein, in einem 
[Umlaufbeschluss](https://stratum0.org/wiki/Vorstandssitzung_2020-04-07#Umlaufbeschl.C3.BCsse_2) 
erst mal 300€ dafür freigegeben.
Zur Zeit prüfen wir, ob wir Spenden auch über den Stratum 0 abwickeln können.

{% fancyalbum 300x300 %}
images/posts/2020-03-30-stratumvsvirus/IMG20200330171652.jpg: Halbzeug für die Gesichtsschutzschirme
images/posts/2020-03-30-stratumvsvirus/IMG20200329002235.jpg: Folien werden in unseren Lasercutter eingelegt
{% endfancyalbum %}

Die Stadt hat uns bestätigt, dass wir, unter Einhaltung der Abstands- und 
Hygieneregeln, den Space mit einzelnen Personen betreten und benutzen dürfen.
Dies nutzen wir um den Space als Produktionsstätte für 3D-Druckteile, Schirme und
Prototypen zu nutzen.
Insbesondere bietet sich der Space (als recht gut zugänglicher Raum) als 
Sammelpunkt für von Makern im Hub gedruckten Schirmhaltern und weiteren 
Komponenten an.
Darüber hinaus werden die (gedruckten) Halter hier mit den gelaserten 
und geschnittenen Schirmen zusammengebracht und weiter verteilt.

{% fancyalbum 300x300 %}
images/posts/2020-03-30-stratumvsvirus/IMG20200328150540.jpg
images/posts/2020-03-30-stratumvsvirus/IMG20200328150608.jpg
{% endfancyalbum %}

Es sind gerade noch viele Fragen offen, klar ist nur: 
Bedarf ist [da](https://www.zdf.de/nachrichten/heute-19-uhr/videos/zahnarztpraxen-coronavirus-100.html).
Alleine vom HEH kam eine Anfrage über mehrere hundert 
Stück und sie produzieren 
[sogar selbst](https://www.facebook.com/HEHBraunschweig/posts/2930921293618522)! 
Dazu kommen ständig neue Anfragen von Praxen und Krankenhäusern aus der Region.

{% fancyalbum 300x300 %}
images/posts/2020-03-30-stratumvsvirus/IMG20200330141247.jpg: Zugeschnittene und gereinigte Folien
images/posts/2020-03-30-stratumvsvirus/IMG20200330141228.jpg: 3D-Druckteile von Makern aus der Region
images/posts/2020-03-30-stratumvsvirus/IMG20200330142622.jpg: Zugeschnittene Gummibänder
{% endfancyalbum %}

## Mitmachen

Wenn ihr einen 3D-Drucker habt, fangt an zu drucken und sucht euch einen Hub 
in eurer Umgebung, sprecht eure Hacker- und Makerspaces an, nehmt Kontakt auf 
mit 3D-Druck-Labs an euren Unis und Instituten.

Auf [makervsvirus.org](https://www.makervsvirus.org/de/) finden sich
Kontaktdaten in die jeweiligen Hubs.

Soviel zur aktuellen Situation im Space.
Ein großer Dank geht an das spontane Eskalationsteam und alle, 
die gerade irgendwie involviert sind!

PS:
Seit einigen Tagen läuft der Gaming-Rechner im Space durchgehend und rechnet für 
[Folding@Home](https://foldingathome.org/) im 
[CCC-Team](https://stats.foldingathome.org/team/245540).
Wenn ihr eine gute Grafikkarte am rumidlen habt, überlegt doch mal, 
ob ihr euch da anschließen möchtet.

