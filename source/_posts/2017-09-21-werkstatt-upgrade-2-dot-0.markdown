---
layout: post
title: "Werkstatt Upgrade 2.0"
author: [larsan]
date: 2017-09-21 22:14
comments: true
categories: [german]
---

Anfang 2014 sind wir in unsere aktuellen Räumlichkeiten umgezogen. 
Die Grundzüge des alten Spaces sind geblieben: Chillraum, Frickelraum und Küche. Dazugewonnen haben wir insbesondere die Werkstatt, unser "Frickelraum fürs Grobe".

Anfangs war die Werkstatt mehr nur eine Idee in der Ferne:

{% fancyalbum 500x800 %}
images/posts/2017-09-21-werkstatt-upgrade-2-dot-0/werkstatt01.jpg: Werkstatt nur zu erahnen.
{% endfancyalbum %}

Stück für Stück - beginnend mit dem Bau einer Werkbank - entstand unsere Werkstatt.

{% fancyalbum 500x800 %}
images/posts/2017-09-21-werkstatt-upgrade-2-dot-0/werkstatt02.jpg: Alle arbeiten aktiv an der Werkbank.
{% endfancyalbum %}

Zu der Festool-Kappsäge, die wir uns während des Umzugs zur Verlegung des Fussbodens angeschafft hatten, kamen immer mehr Werkzeuge hinzu. Hier mal eine Spende, dort mal eine Dauerleihgabe, oder auch mal eine eigene Anschaffung.
So ist  Werkstatt in den vergangenen Jahren stetig gewachsen und wurde immer weiter ausgebaut. In den letzten Monaten gab es nun einige Sprünge, die wir in den folgenden Tagen und Wochen gesondert erwähnen wollen.

{% fancyalbum 500x800 %}
images/posts/2017-09-21-werkstatt-upgrade-2-dot-0/werkstatt03.jpg: In etwa der Zustand während dieser Blogpost geschrieben wird.
{% endfancyalbum %}

Be prepared!

