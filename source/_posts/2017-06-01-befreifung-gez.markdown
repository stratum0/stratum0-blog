---
layout: post
title: "Stratum 0 zahlt keine GEZ mehr"
date: 2017-06-01 22:55
comments: true
author: chrissi^
categories: [german]
---

Als Verein mit eigenem Raum haben wir uns im August 2013 beim Beitragsservice angemeldet. Natürlich haben wir dabei angegeben, dass wir gemeinnützig sind, um immerhin nur den reduzierten Beitrag von 5,99€ pro Monat zahlen zu müssen.
Da der Beitragsservice in seiner öffentlichen Kommunikation stark das Gefühl verbreitet, dass sowieso jeder zahlen müsste und ganz schlimme Dinge passieren, wenn man es nicht tut, war die Motivation sich mit der genauen Gesetzeslage zu beschäftigen eher gering.

Später wurden wir auf einen [Blogpost][0] von /dev/tal hingewiesen. Dort hat sich jemand den Gesetzestext im Wortlaut angesehen und ist dabei auf folgenden Abschnitt gestoßen:

> §5 RBStV (5) Ein Rundfunkbeitrag nach Absatz 1 ist nicht zu entrichten für Betriebsstätten
> 1. die gottesdienstlichen Zwecken gewidmet sind,
> 2. in denen kein Arbeitsplatz eingerichtet ist oder
> […]

Natürlich wäre es auch interessant sich mit (1) zu beschäftigen und einmal herauszufinden, welchen Aufwand eigentlich die Anerkennung als Ort für gottesdienstliche Zwecke mit sich bringt. Der dafür notwendige Aufwand scheint allerdings den für (2) zu überschreiten.

Als kleiner Verein haben wir sowieso keine Angestellten – unsere Arbeit beruht auf Ehrenamt. Entsprechend haben wir nie den Aufwand betrieben bei uns formale Arbeitsplätze einzurichten. Warum sollten wir uns auch mit der Berufsgenossenschaft, Arbeitssicherheit und ähnlichem beschäftigen?

rohieb hat sich daraufhin an den Beitragsservice gewendet:

> Sehr geehrte Damen und Herren,
> wir sind ein gemeinnütziger Verein (Stratum 0 e. V., Amtsregister Braunschweig, VR 200889) und beschäftigen keine Angestellten bzw. haben noch nie Angestellten beschäftigt, und somit in unseren Räumlichkeiten keine Arbeitsplätze eingerichtet.
> 
> Nach §5 Abs. 5 RBStV Punkt 2 („Ein Rundfunkbeitrag nach Absatz 1 ist nicht zu entrichten für Betriebsstätten [. . . ] in denen kein Arbeitsplatz eingerichtet ist [. . . ]“) müssten wir also keinen Rundfunkbeitrag entrichten. Ist dies soweit korrekt? Kann der Rundfunkbeitrag für unseren Verein abgemeldet werden?

Ergebnis:
Der Stratum 0 e. V. ist nun vom Rundfunkbeitrag befreit. Darüber hinaus haben wir den gezahlten (und nicht verjährten) Beitrag rückwirkend erstattet bekommen. \o/


[0]: https://www.devtal.de/blog/2015/11/18/wir-zahlen-keinen-rundfunkbeitrag/