---
layout: post
title: "Stratumnews Vol. 29"
author: Stratumnews-Team
date: 2020-01-03 23:30
comments: true
categories: [german,stratumnews]
---
Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:

<!-- more -->

Beamer
------

Der Chillraum hat einen neuen [Beamer][] bekommen, einen Sony VPL-PHZ10 Laserbeamer. 
Bitte pfleglich behandeln, sodass wir lange etwas davon haben.
Der alte Chillraum-Beamer wurde in den Frickelraum portiert, dank 2*HDMI geht ein Kabel zum Tisch runter.

[Beamer]: https://pro.sony/de_DE/products/laser-projectors/vpl-phz10


Hacken Open Air 2020
--------------------

Es wird wieder ein [HOA][] geben! 
Wir haben ein Gelände und einen Termin. 
Die Vorbereitungen sind angelaufen, das nächste Orgatreffen findet am 16.01.2020 statt.
Wer mit organisieren möchte, sollte sich in die [Mailingliste][] eintragen.

[HOA]: https://hackenopenair.de/
[Mailingliste]: https://lists.stratum0.org/mailman/listinfo/hoa-orga


Acolyte/Buildserver
-------------------

Im Spaceserverschrank steht jetzt ein bisschen [Hardware][], die Dinge bauen kann. 
Freifunkimages z.B. oder jemand schlug auch mal nicht mehr offiziell gebaute Lineage Builds vor.
Ansprechpartner für die Maschine ist [53c70r][].

[Hardware]: https://stratum0.org/wiki/Acolyte
[53c70r]: https://stratum0.org/wiki/Benutzer:53c70r

Misc
----

* Die Werkstatt hat nun eine eigene [Klappleiter][leiter] für die oberen Regalebenen bekommen.
* Eine [Verdunkelungslösung][dunkel] im Chillraum wurde realisiert.
* Der [Plotterschrank][plotte] hat neue, heile Rollen und Haltegriffe bekommen.
* Das [Bedienpanel][bedien] des Plotters hat das zeitliche gesegnet, es wird Ersatz benötigt.
* Der Kodi-RPi im Frickelraum hat jetzt ein Audiokabel zum Verstärker.
* Kühlschrankfächer wurden geklebt und teilweise mit Aluprofilen verstärkt, don't break.
* Es gibt nun digitale/elektronische Thermostate an den Heizkörpern. Larsan hat mehr Informationen.
* Eine Abordnung aus dem Space war auf dem 36C3.
* Die [akaFunk] Braunschweig trifft sich nun ein mal monatlich im Space. Die Termine stehen im [Kalender][].
* Der Stratum 0-[Blog][] ist wieder repariert. Schreibt viele coole Beiträge! \o/


[leiter]: https://chaos.social/@daniel_bohrer/102968097521058946
[dunkel]: https://gitli.stratum0.org/stratum0/TUWAT/issues/12
[plotte]: https://matrix.stratum0.org/_matrix/media/r0/download/stratum0.org/psHlJGuZuDXTIEzEyQErLFIu
[bedien]: https://gitli.stratum0.org/stratum0/TUWAT/issues/26
[akaFunk]: https://akafunk-bs.de/
[Kalender]: https://stratum0.org/wiki/Termine#Sonstiges
[Blog]: https://stratum0.org/blog/


New Things in Space
-------------------

* Rotations-Exzenterschleifer (Werkstattregal über der Kappsäge)
* Granit-Abrichtplatte (unter der Werkbank, bitte pfleglich behandeln!)
* Digital-Winkelmesser (Messbox, Werkstattregal hinter der Tür)
* Braten-/Ofenthermometer (Schrank links neben dem Ofen)
* 7 m-Bandspanner (Werkstatt, Wandregal neben der Tür)
* 2 Rollböcke (Werkstatt, unter der Kappsäge)



Termine
-------

* Mo, 06.01. 18:00: Malkränzchen, offener Kreativabend
* Di, 07.01. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Di, 07.01. 19:00: Vorstands-Arbeitstreffen
* Mi, 08.01. 19:00: Freifunk-Treffen
* Do, 09.01. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Mo, 13.01. 18:00: Malkränzchen, offener Kreativabend
* Mo, 13.01. 19:00: HOA: Netzwerktreffen
* Di, 14.01. 19:00: C64 Demo, Treffen im Stratum0
* Di, 14.01. 19:00: Vorträge (Aufzeichnungen)
* Mi, 15.01. 19:00: Freifunk-Treffen
* Do, 16.01. 19:00: HackenOpenAir 2020 Orgatreffen
* Do, 16.01. 20:00: Diskussionsgruppe Autonymes Reisen (mit DC)
* So, 19.01. 14:00: Mitgliederversammlung 2020-01-19
* Mo, 20.01. 18:00: Malkränzchen, offener Kreativabend
* Di, 21.01. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Di, 21.01. 19:00: HOA: Flächennutzungstreffen
* Mi, 22.01. 19:00: Freifunk-Treffen
* Do, 23.01. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Sa, 25.01. 14:00: CoderDojo
* Di, 28.01. 19:00: C64 Demo, Treffen im Stratum0
* Do, 06.02. 18:00: offenes Treffen der akaFunk



Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!
