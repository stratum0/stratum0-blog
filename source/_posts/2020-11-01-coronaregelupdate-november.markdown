---
layout: post
title: "Corona-Regelupdate November 2020"
date: 2020-11-01 21:30
comments: true
author: rohieb
categories: [german, stratum0, covid19]
---

| __**Update 2021-06-20: diese Regelungen wurden abgelöst.**__
| __Die neue Regelung findest du [hier](https://stratum0.org/blog/posts/2021/06/20/coronaregelupdate/).__
|

| __**Update 2021-03-17: diese Regelungen wurden aktualisiert.**__
| __Die aktuelle Verordnung des Landes gibt es [hier](https://www.niedersachsen.de/download/166315).__
| __Ab heute sind wieder bis zu fünf Personen aus maximal zwei Haushalten erlaubt.__
|

| __**Update 2021-01-10: diese Regelungen wurden aktualisiert.**__
| __Die aktuelle Verordnung des Landes gibt es [hier](https://www.niedersachsen.de/download/162602).__
| __Ab heute sind nur noch die Personen eines Hausstands, sowie maximal eine weitere Person gleichzeitig im Space erlaubt.__
|

| __**Update 2020-12-01: diese Regelungen wurden aktualisiert.**__
| __Die aktuelle Verordnung des Landes gibt es [hier](https://www.niedersachsen.de/download/161342 ).__
| __Ab heute sind nur noch maximal 5 Personen gleichzeitig im gesamten Space erlaubt.__
|

Es geht recht schnell in diesen Zeiten…
Das Niedersächsische Ministerium für Soziales, Gesundheit und Gleichstellung
hat zum 02.11.2020 eine neue [Verordnung zur Eindämmung des Corona-Virus](https://www.niedersachsen.de/download/160239)
erlassen.
Mit dieser neuen Regelung folgen wir der Verordnung.

**Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**

* Im gesamten Space dürfen sich maximal fünf Personen aufhalten.
* Diese Personen sollten aus einem Haushalt stammen.
  Ausnahme hiervon sind Treffen von Personen aus einem Haushalt
  mit Personen eines weiteren Haushalts, soweit sich diese explizit
  zum Zusammenarbeiten treffen.
* Es muss immer mindestens ein Mitglied anwesend sein.
* Es ist ein Abstand von 1,5 Metern zwischen Personen unterschiedlicher
  Haushalte einzuhalten.
* Im Space ist eine Mund-Nasen-Bedeckung zu tragen. Ausnahmen hiervon
  sind die Nahrungs- und Flüssigkeitsaufnahme.

* Belegungskoordination:
  * Der Space muss im Spacenutzungsplan-Pad reserviert und die Einträge
    ggf. aktualisiert werden. Bitte seid beim Reservieren fair
    zueinander und tragt sowohl ein Beginn als auch ein abgeschätztes
    Ende ein! (URL aus Datenschutzgründen bitte im Chat oder auf der
    Mailingliste erfragen.)
  * Auch kurze Besuche, z.B. um Dinge oder Post aus dem Space abzuholen,
    müssen ins Spacenutzungsplan-Pad eingetragen werden.
  * Der Space-Status muss bei Anwesenheit von Personen auf "offen"
    gesetzt werden, und bei Nichtanwesenheit von Personen auf "zu".
  * Der Space soll primär für Projekte verwendet werden, welche zu Hause
    nicht umgesetzt werden können!
  * Wer den Space betritt, muss weiterhin seine Kontaktdaten hinterlassen:
    - Hierzu liegen Zettel aus, die ausgefüllt in einen verschlossenen
      Kasten geworfen werden.
    - Wir werden auf diesen Zetteln Name, einen Kontaktweg und den
      Zeitraum Deines Besuches erfassen.
    - Diese Daten werden nach drei Wochen vernichtet. Die gesammelten
      Daten werden nur zur Nachverfolgung eventueller Infektionsketten
      genutzt.
  * Bitte bestellt vorerst keine weitere Post in den Space.

<!-- -->

* Hygieneregeln:
  * Vor und nach der Nutzung eines Raumes muss dieser, so weit möglich,
    gründlich (mindestens 3 Minuten lang) gelüftet werden. Während der
    Nutzung bietet es sich an, mindestens ein Fenster gekippt zu lassen.
  * Bitte wasche Dir regelmäßig die Hände und verhalte Dich hygienisch.
  * Bringe ein eigenes Händehandtuch mit. Wenn Du kein eigenes Handtuch
    dabei hast, benutze Papierhandtücher.
  * Nach der Nutzung müssen genutzte Oberflächen, Geräte und Maschinen
    entweder desinfiziert oder mit Seifenwasser abgewaschen werden.
  * Und natürlich: Wenn Du dich krank fühlst, komm nicht in den Space!

Happy Hacking!

PS: Der Vorstand versucht diese Regelungen laufend zu optimieren.
Wenn Du Änderungsvorschläge hast, lass uns diese gern wissen.
