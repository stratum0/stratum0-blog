---
layout: post
title: "Öffnung des Spaces – ein klein wenig (Juni-Edition, Update)"
date: 2020-06-23 23:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

__**Update 2020-10-27 diese Regelungen wurden abgelöst. 
[Hier](https://stratum0.org/blog/posts/2020/10/27/coronaregelupdate/) geht es zu den neuen Regelungen.**__

__**Update 2020-09-04 um weiteren Nachfragen vorzubeugen: Die Regelungen unten
werden auch für die absehbare Zukunft beibehalten werden. Bitte benutzt auch
weiterhin die Zettel vor Ort zur eventuellen Corona-Nachverfolgung, und die
wöchentlichen Koordinations-Pads (URL aus Datenschutzgründen bitte im Chat oder
auf der Mailingliste erfragen).**__

Am 22.06.2020 hat die Landesregierung in Niedersachsen die nächsten [Lockerungen der Kontaktbeschänkungen](https://www.niedersachsen.de/Coronavirus/vorschriften/vorschriften-der-landesregierung-185856.html) beschlossen.

Dieses nimmt der Vorstand als Anlass die Regelungen ein weiteres Mal zu überarbeiten.
Auch diese Änderungen haben das Ziel, den Space so weit wie möglich zu öffnen und dabei die Vorgaben
der Landesregierung einzuhalten.

**Diese Regelung ersetzt alle vorherigen Regelungen.**
Der Vorstand hat heute folgende neue Regelung beschlossen:

* Es dürfen sich auch weiterhin maximal 10 Personen gleichzeitig im Space aufhalten.
* Es ist ein Abstand von 1.5 Metern zwischen Personen unterschiedlicher Haushalte einzuhalten.
* Mitglieder dürfen Gäste mit in den Space bringen. Die Gäste dürfen aber nicht alleine/unbetreut im Space sein.
* Es dürfen nun auch Leute aus mehreren Haushalten dauerhaft in einem Raum sein.
* Es sind weiterhin keine öffentlichen Veranstaltungen im Space zugelassen.
  Das heißt Zusammenkünfte, die keine vor Ort geschlossene Gesellschaft
  sind, bleiben untersagt.
  Bitte beteiligt trotzdem die Vereinsöffentlichkeit, z.B. über einen Videostream,
  einen Blogbeitrag oder Toots / Tweets an eurer Veranstaltung.
* Die Küche darf wie jeder andere Raum genutzt werden, so lang die
  Abstands- und Hygieneregeln eingehalten werden.
  Die Nutzung der Küche muss aber allen Entitäten ermöglicht werden.
* Wer den Space betritt, muss weiterhin seine Kontaktdaten hinterlassen:
  * Hierzu werden [Zettel](https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/XliisJtXTyQIyMBYZCikIyKL)
    ausliegen, die ausgefüllt in einen verschlossenen Kasten
    geworfen werden.
  * Wir werden auf diesen Zetteln Name, einen Kontaktweg und den 
    Zeitraum Deines Besuches erfassen.
  * Diese Daten werden nach drei Wochen vernichtet. Die gesammelten 
    Daten werden nur zur Nachverfolgung eventueller Infektionsketten 
    genutzt.

* Die Planungspads sollen aber weiterhin genutzt werden, um Kollisionen
  bei der Raumnutzung zu vermeiden.
  Jedoch kann der Space nun auch ohne Reservierung genutzt werden.
  Aktuell werden die Pads nach Ablauf der Woche gelöscht.

|

* Vor und nach der Nutzung eines Raumes muss dieser, so weit möglich, 
  gründlich gelüftet werden. Während der Nutzung bietet es sich an, 
  mindestens ein Fenster gekippt zu lassen.
* Bitte wasche Dir regelmäßig die Hände und verhalte Dich hygienisch.
* Bringe ein eigenes Händehandtuch mit. Wenn Du kein eigenes Handtuch 
  dabei hast, benutze Papierhandtücher.
* Nach der Nutzung müssen genutzte Oberflächen, Geräte und Maschinen 
  entweder desinfiziert oder mit Seifenwasser abgewaschen werden.
* Und natürlich: Wenn Du dich krank fühlst, komm nicht in den Space!

PS:
Der Vorstand versucht diese Regelungen laufend zu optimieren.
Wenn Du Änderungsvorschläge hast lass uns diese gern wissen.

*Aktualisierung 2020-07-04*:
Der Regelung wurde eine Klarstellung zu Veranstaltungen und zur
Nutzung der Küche hinzugefügt. (Siehe dieser [diff](https://gitli.stratum0.org/stratum0/stratum0-blog/-/commit/a25628338a94ee2233ada68fdb2d4895a5a7e54f))
