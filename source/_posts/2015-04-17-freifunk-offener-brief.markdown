---
layout: post
title: "Offener Brief an den Rat der Stadt Braunschweig zur Mitteilung DS 14314/15"
date: 2015-04-17 18:00
comments: true
author: [chrissi]
categories: [german, freifunk]
banner:
  img: images/header/unifi-mesh.jpg 
  source: https://stratum0.org/blog/images/posts/2018-06-01-freifunk-event-wlan/20180527143648.jpg
  license: CC BY-SA 3.0
  author: Chrissi^
---

Sehr geehrter Rat der Stadt Braunschweig,
----------------------------------------------------
als Freifunk-Initiative in Braunschweig verfolgen wir mit Interesse die dem
[Beschluss 3584/14] folgenden Entwicklungen und somit auch die kürzliche
Mitteilung der Verwaltung ([DS 14314/15]) an den Wirtschaftsausschuss für den
17.04.2015. Hierbei fiel uns auf, dass die Verwaltung bei ihrer Recherche viele
Punkte missverständlich dargestellt hat, und leider auch die großen und
erfolgreichen Freifunkprojekte in Städtekooperation wie Arnsberg und
insbesondere Berlin unberücksichtigt lässt.

[Beschluss 3584/14]: https://ratsinfo.braunschweig.de/frame.php?site=fulltext&amp;action=details&amp;id=2805&amp;idx=0&amp;source=Antrag&amp;db_database=0
[DS 14314/15]: https://ratsinfo.braunschweig.de/index.php?site=fulltext&amp;action=openblob_treffer_to&amp;type=pdf&amp;id=8840&amp;idx=0&amp;source=Mitteilung&amp;&amp;showto=1&amp;db_database=0


Im Folgenden greifen wir daher Abschnitte der Mitteilung auf und korrigieren
diese:

> Zudem entzieht sich diese dezentrale 
> Lösung der verlässlichen Steuerung 
> der Versorgungsgebiete und –qualitäten.

Die oben genannten Beispiele zeigen klar, dass die Kooperation öffentlicher
Einrichtungen, Gewerbetreibender und Privatleute zu einer erfolgreichen
Flächendeckung mit kostenlosem WLAN in innerstädtischen Bereichen führen kann.
Mit Paderborn und Hamburg lassen sich darüber hinaus noch zwei Beispiele
anführen, in denen private Initiativen, sogar ohne Beteiligung der Städte,
umfangreiche Netze an Zugangspunkten aufgebaut haben.

> Ein verlässliches Versprechen gegenüber
> den Nutzern könnte damit nicht abgegeben
> werden.

Durch die Beteiligung Vieler am Aufbau der Zugangspunkte ist für die
Freifunknutzer quasi jederzeit ein technischer Ansprechpartner verfügbar. Die
Sicherstellung des Betriebs der Infrastruktur wird durch die dezentrale Struktur
auf viele Schultern verteilt und die Betriebskosten sind dementsprechend niedrig.
Die geringen Kosten für den Betrieb der Zugangspunkte, nämlich die Stromkosten,
verteilen sich auf die gleichen Schultern. Soll ein neues Gebiet gezielt
erschlossen werden, so kann, unter Mithilfe der Stadt oder ähnlicher
Einrichtungen, sowie der Anwohner, auf deren verteilte und bestehende
Infrastruktur zurückgegriffen werden. Durch die so entstehende Redundanz kann
von einer höheren Verfügbarkeit als bei einer zentralisierten Infrastruktur
ausgegangen werden.

> Auch wenn gesetzliche Änderungen 
> hinsichtlich der sogenannten Störerhaftung 
> in Aussicht gestellt sind, bleibt
> doch fraglich, ob derartige Initiativen 
> juristisch nachhaltig abgesichert 
> sind und auch von offizieller 
> Seite genutzt werden sollten."

Erste Urteile weisen in der aktuellen Rechtsprechung darauf hin, dass auch
Betreiber von Freifunk-Zugängen als Provider anzusehen sind und diese somit von 
der Störerhaftung ausgenommen sind ([AG Charlottenburg], [Stellungnahme Kanzlei 
Hubrig]). Freifunk Rheinland hat sich darüber hinaus dafür entschieden, selber 
Internetzugangsanbieter (ISP – Internet Service Provider) zu werden und betreibt
die Infrastruktur nun im Rahmen des Freifunk Rheinland e.V. ([FF Rheinland1], 
[FF Rheinland2]). Da aus unserer Sicht somit derzeit eine juristische Absicherung 
gegeben ist, spricht nichts gegen eine Kooperation einer öffentlichen
Einrichtung mit Freifunk. Wie die aktuell laufende Gesetzgebung Freifunk oder
kommerzielle Anbieter beeinflusst ist zum jeztigen Zeitpunkt schwer absehbar. 
Der Vorstand für Recht & Politik des eco e.V. sagt hierzu zum Beispiel: 
"Wird der Referentenentwurf 1:1 umgesetzt, bleibt Deutschland weiterhin eine 
WLAN-Wüste." ([eco]) Eine Zusammenfassung weiterer Stimmen findet sich z.B. 
bei »Freifunk statt Angst« ([FSA]).

[AG Charlottenburg]: http://freifunkstattangst.de/files/2015/01/AG-CharlottenburgBeschluss.pdf
[Stellungnahme Kanzlei Hubrig]: https://freifunk-bs.de/assets/documents/RA_Stellungnahme_Freifunk.pdf
[FF Rheinland1]: https://forum.freifunk.net/t/projekt-provider-werden/35
[FF Rheinland2]: https://twitter.com/ffrhein/status/516184999352352769
[eco]: https://www.eco.de/2015/pressemeldungen/wlan-wueste-deutschland-nachbesserungsbedarf-fuer-die-bundesregierung.html
[FSA]: http://freifunkstattangst.de/2015/04/13/nur-kritische-stimmen-bisher-veroeffentlichte-stellungnahmen-zur-geplanten-aenderung-des-telemediengesetzes/

> Der Verein Freifunk ist eine 
> private Initiative, bei der 
> Bürger ihre private Internetverbindung 
> kostenfrei für andere Nutzer freigeben.

Die Bereitsteller von Freifunk-Zugangspunkten reservieren einen von ihnen 
selbst festgelegten Teil ihrer Bandbreite für das Freifunknetz. Wird diese nicht
ausgelastet, steht sie auch weiterhin ihnen selbst zur Verfügung. Freifunknutzer 
und -bereitsteller kann jede/r sein, nicht nur Bürger, sondern auch Ladenlokale 
oder gar Ketten (s. Paderborn) oder öffentliche Einrichtungen (s. Berlin). 
Die Möglichkeit, sich einem stadtweiten, offenen WLAN anzuschließen ist ein 
Standortvorteil für kleine und mittelständische Unternehmen. Dies ist mit 
Freifunk für Unternehmen mit geringem Aufwand und vernachlässigbaren Kosten 
realisierbar; in Verbindung mit einer kommerziellen Lösung jedoch eher aufwendig.

> Die jeweiligen Netze werden 
> nicht als getrennte Einwahlknoten 
> genutzt, sondern untereinander 
> verbunden (Mesh-Netze). 
> Dabei wird die sogenannte Störerhaftung 
> „umgangen“.

Jeder Knoten ist ein "Einwahlknoten" (Access-Point), mit dem sich Nutzer 
verbinden können. Knoten, die selbst keinen direkten Internetzugang haben, aber 
direkt oder indirekt einen Knoten mit Internetzugang per Funk erreichen können 
("Meshing"), können mit dieser Technik an infrastrukturarmen Orten 
Internetzugang anbieten. Diese Art der Verbindung über Meshing ermöglicht nicht
die Störerhaftung zu umgehen. Stattdessen dient Meshing als technisches Mittel
zur Vergrößerung der Reichweite des Netzes. Freifunk würde auch ohne diesen 
Reichweitenbonus funktionieren. Für die rechtliche Bewertung ist die Betrachtung
dieser Funktionsweise irrelevant.

> "Derzeit schützt sich die Freifunk 
> Initiative vor einer Inanspruchnahme 
> durch die Verlagerung der Server 
> für ihre Mesh-Netze ins Ausland."

Die genannten Server ("Gateways" genannt) sind die Ausgänge, über welche 
Nutzer des Freifunknetzes tatsächlich "ins Internet" kommen. Prinzipiell ist 
aus deutscher Sicht irrelevant, ob die Gateway-Server im Inland oder im Ausland
stehen, denn bisher wurde nicht nachgewiesen, dass die Störerhaftung für
Freifunk greift, da die angebotenen Dienste denen eines Providers entsprechen 
([AG Charlottenburg]). Unter Aufrechterhaltung der Unschuldsvermutung darf daher 
nicht von einer Umgehung der Störerhaftung gesprochen werden. Der Rückgriff auf
Gateways verschiedener Anbieter in verschiedenen Ländern dient vielmehr der
Ausfallsicherheit und Lastenverteilung des Netzes und erhöht somit Stabilität 
und Qualität des Angebotes. Das Providerprivileg dient dazu, Provider vor der
falschen Vorgabe von Rechtsansprüchen Dritter zu schützen; so wie ein Postbote
nicht für den Inhalt der Briefe verantwortlich ist, die er zum Briefkasten 
bringt. Das Providerprivileg ist die Grundvoraussetzung für den Betrieb einer
öffentlichen Zugangsinfrastruktur. ([Compuserve-Urteil], [Stellungnahme Kanzlei 
Hubrig])

[Compuserve-Urteil]: http://de.wikipedia.org/wiki/Compuserve#Das_Compuserve-Urteil

> "Die befragten Kommunen treten 
> mehrheitlich nicht als Provider 
> auf, auch um nicht in die 
> Störerhaftung zu gelangen."

Diese Aussage ist inhaltlich falsch, da die Störerhaftung für eine Kommune, 
die als Provider auftritt, nicht gilt. Darüber hinaus besteht für die Stadt 
natürlich die Möglichkeit, Freifunk als Provider zu fördern, beispielsweise 
durch finanzielle Unterstützung und/oder durch Nutzung öffentlicher Gebäude.

Die Verwaltung möchte offensichtlich rechtliche Risiken von der Stadt abwenden; 
der aktuellen Rechtsauffassung nach existieren diese aber nicht, oder lägen im 
Zweifel beim Kooperationspartner der Stadt (z. B. Freifunk).

Die Verwaltung hat in ihrer Mitteilung auch einen Anforderungskatalog 
formuliert. Diesen wollen wir kurz kommentieren:

Folgende Anforderungen sollen an das Netz bzw. den Betrieb selbst gestellt 
werden:

1. *Betrieb nicht bei der Stadt Braunschweig, sondern über einen Provider*<br>
   Als Netzbetreiber sind Freifunker in der Lage, als Provider zu agieren.
2. *Vollständige Abdeckung der oben genannten Plätze*<br>
   In Zusammenarbeit mit der Stadt, lokal ansässigen Gewerbetreibenden und 
   Privatpersonen ist eine Erschließung und Abdeckung der genannten Plätze 
   sofort möglich.
3. *Stabile Bandbreiten auch bei höheren Besucherzahlen zwischen 6 und 16 Mbit/s*
   Die verteilte Infrastruktur des Freifunknetzes und die damit entstehende 
   Lastenverteilung ergibt ein redundantes und skalierbares Netz, das auch 
   große Nutzerzahlen abfangen kann. Konkrete Zahlen hängen von den örtlichen 
   Bereitstellern ab und ist bei Bedarf schnell ausbaubar.
4. *Einfaches Anmeldeprozedere*
   Freifunk Braunschweig verzichtet auf eine Anmeldung der Nutzer. Das ist die 
   einfachste Form der Nutzung. Jegliche Form einer Anmeldung/Registrierung ist 
   ein Nutzungshemmnis, daher verzichten auch Freifunk Berlin (trotz 
   Städtekooperation) und Hamburg darauf.
5. *Kompatibilität mit Eduroam (WLAN-Netz der Universitäten)*<br>
   Die gleichzeitige Verbreitung von Eduroam muss mit den Universitäten 
   abgesprochen werden. Eine gleichzeitige Existenz von Freifunk und Eduroam 
   ist problemlos möglich. Da Eduroam prinzipbedingt höhere Nutzungshürden als 
   Freifunk hat (insbesondere nur für Studenten und universitäre Mitarbeiter zur
   Verfügung steht), kann für den reinen Netzzugang auch stattdessen einfach 
   Freifunk benutzt werden.
6. *maßvolle Werbeeinblendungen während der Nutzung (ggf. feste Integration auf*
   *der Landing Page, parallele Druckwerbung etc.)*<br>
   Freifunk verzichtet auf den unter 4) genannten Gründen auf eine Landing-Page. 
   Freifunk selbst ist werbefrei. Die Stadt Braunschweig kann natürlich (z.B. 
   auf ihrer Homepage) dafür werben, dass sie mit diesem sozialen 
   Infrastrukturprojekt zusammenarbeitet. Druckwerbung ist bei entsprechend 
   verfügbaren Budgets denkbar.
7. *Kostenfreiheit für die Nutzer von mindestens einer Stunde täglich*<br>
   Freifunk bietet 24 Stunden täglich, 7 Tage die Woche kostenfreien 
   Internetzugang. Auch an Feiertagen.
8. *Landing Page bei Netzanmeldung, aus der der Kooperationspartner*
   *„Stadt Braunschweig“ deutlich erkennbar ist und von der aus weiterführende* 
   *Links und ggf. auch kurze Informationen zu städtischen Angeboten möglich*
   *sind*<br>
   Freifunk verzichtet auf den unter 4) genannten Gründen auf eine Landing-Page. 
   Permanente Werbung für die Kooperation der Stadt Braunschweig auf der 
   Homepage der Stadt, der Homepage von Freifunk Braunschweig und/oder durch 
   Druckwerbung und Mundpropaganda werden zu einer positiven Wahrnehmung des 
   Internetangebotes der Stadt führen.
9. *Möglichkeit der späteren Nutzung für eventuelle städtische Anwendungen*
   *bzw. Nutzung für Smart-City Ansätze (Leitsysteme, Informationssysteme etc.)*<br>
   Die Freifunk-Infrastruktur diskriminiert keine Dienste. Der Stadt 
   Braunschweig steht unser Netzwerk somit für eigene Dienste offen.
   
Ausblick & Perspektiven
-----------------------------

- *Freifunk ohne Unterstützung der Stadt*<br>
  Freifunk Braunschweig kann ohne Unterstützung langsam weiter wachsen. 
  Dies geschieht stetig, aber mit eher zufälliger Ausbreitung in der 
  Flächenabdeckung, sofern keine aktiven Werbeaktionen seitens der Initiative 
  selbst forciert werden.

- *Stadt als Teilnehmer und Unterstützer der Initiative*<br>
  Wenn Freifunk Braunschweig von der Stadt Braunschweig unterstützt wird, 
  kann die Initiative die von der Stadt angestrebten Plätze vorrangig 
  ausbauen, indem dort Access-Points installiert werden. Die Sicherheit 
  des städtischen Netzwerkes (stadteigene Gebäude), sofern dieses genutzt 
  wird, wird dadurch nicht beeinträchtigt. Lediglich wird die jeweils am Ort 
  vorhandene Bandbreite in jenem maximalen Ausmaß eingeschränkt, das von der 
  Stadt in Kauf genommen wird. Die Kosten pro Router betragen dabei geschätzt 
  pro Anschaffung 20-100€ und für jährlichen Betriebsstrom im Schnitt 
  ca. 10-15 € pro Einheit. Die Minimalausstattung der von der Stadt 
  angestrebten Plätze kann so mit wenigen Einheiten ermöglicht werden und 
  erfüllt den Anspruch einer "möglichst kostenneutralen Lösung". 
  Für eine komfortable, vollständigere und ausfallsicherere Abdeckung 
  wären je nach Ort mehr Einheiten notwendig.

- *Stadt als Schirmherr*<br>
  Die Stadt bewirbt die Freifunk-Initiative z.B. auf ihrer Webseite oder 
  durch Auslage von Print-Werbung in Bürgerbüros, etc. Darüber hinaus kann 
  die Stadt als Türöffner bei ansässigen Gewerbetreibenden dienen und 
  Kontakte herstellen.

- *Stadt mit mehreren Kooperationspartnern*
  Es ist auch die Kooperation der Stadt mit mehreren Partnern zugleich 
  denkbar. So kann Freifunk Braunschweig die Abdeckung zusammen mit Bürgern 
  und Gewerbetreibenden weiter ausbauen und die Stadt gleichzeitig eine 
  Kooperation mit einem kommerziellen Provider eingehen.

Freifunk Braunschweig steht immer für den gemeinsamen Ausbau der 
Grundversorgung mit freiem WLAN zur Verfügung. Gerne stehen wir ihnen für 
Details, Fragen und Konzeptgespräche zur Verfügung.

Anhang: Liste der größten Freifunk-Communities
-----------------------------------------------

| Name & Stadt/Region   	 | Zugangspunkte |
| -------------------------  | ------------- |
| Freifunk Hamburg	         |           794 |
| Freifunk Paderborn 	     |           742 |
| Freifunk Südwestfalen 	 |           670 |
| Freifunk Düsseldorf        |       	450  |
| Freifunk Berlin 	         |           371 |
| Freifunk München 	         |           371 |
| Freifunk Muenster 	     |           336 |
| Freifunk Bielefeld 	     |           310 |
| (...)	                     |               |
| Freifunk Braunschweig      |       	118  |


Insgesamt über 11.000 Zugangspunkte in Deutschland: [Karte]

[Karte]: http://www.freifunk-karte.de/
