---
layout: post
title: "Corona-Regelupdate November 2022"
date: 2022-11-10 22:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

| __**2022-11-10: Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**__
|

__**tl;dr: 2G, Veranstaltungen möglich.**__


Im Space gilt eine "2G"-Regelung. Folgende Personen haben Zutritt:

* Geimpfte und genesene Personen.
* Ebenfalls Zutritt haben Personen, die sich nicht impfen lassen können
    (mit Nachweis)
    und Kinder unter 5 Jahren.


## Dies setzen wir wie folgt um:

* Alle Mitgliedsentitäten sind Betreiber des Spaces, und damit selbst
    für die Einhaltung der Regelung verantwortlich.
* Wer Gäste mitbringt, ist dafür verantwortlich, dass diese die Regeln
    einhalten.
* Wenn im normalen Spacebetrieb Gäste vor der Tür stehen, so sind die im
    Space anwesenden Mitglieder dafür verantwortlich,
    diese vor Zutritt auf die 2G-Regelung hinzuweisen, die Einhaltung
    sicherzustellen und bei Nichteinhaltung,
    oder bei Nichteinwilligung zur Überprüfung den Zugang zum Space zu
    verwehren.

## Veranstaltungen

* Geplante Veranstaltungen sind im Space-Terminkalender einzutragen.
* Für Veranstaltungen ist mindestens eine anwesende Entität zu definieren,
     die die Kontrolle der 2G-Regelung zu verantworten hat.
     Dies kann zum Beispiel auch über den “Kontakt”-Eintrag auf der Veranstaltungsseite
     im Wiki passieren.

## Hygienemaßnahmen:

* Wascht euch die Hände und haltet den Space sauber.
* Lüftet, insbesondere zu Beginn und zum Ende eures Spacebesuchs.
* Komme nicht in den Space, wenn du dich krank fühlst.

_Dieser Text darf gerne frei kopiert, übernommen und verändert werden.
Falls eine Lizenzangabe nötig ist: CC0_
