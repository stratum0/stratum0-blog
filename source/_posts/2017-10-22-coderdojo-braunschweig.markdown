---
layout: post
title: "CoderDojo Braunschweig"
date: 2017-10-23 17:00
comments: true
author: [larsan]
categories: [german, coderdojo]
---

Die Braunschweiger Zeitung hat uns zum 24. CoderDojo besucht und einen [Artikel] über uns geschrieben.
Die Anmeldung zum 25. CoderDojo am 18.11. erfolgt über [eventbrite].

[eventbrite]: https://www.eventbrite.de/e/coderdojo-025-tickets-39213156607
[Artikel]: https://www.braunschweiger-zeitung.de/braunschweig/article212325211/Hier-tuefteln-Programmierer-von-morgen.html

Nach dem 24. CoderDojo, hier ein kurzer Zwischenbericht.

Seit Mitte 2014 findet das CoderDojo Braunschweig - mit einige Aussetzern - monatlich statt. An einem Samstagnachmittag betreuen dann unsere Mentoren für einige Stunden interessierte Kinder und Jugendliche, die sich spielend programmieren beibringen.

Angefangen hat alles, als Eltern bei uns anfragten, ob wir nicht dabei helfen könnten in Braunschweig ein solches CoderDojo aufzubauen, da sie zu dem Zeitpunkt mit ihrer Tochter regelmäßig nach Berlin fuhren. Das Interesse geweckt, fuhren direkt 7 Entitäten mit zum nächsten CoderDojo-Termin nach Berlin, um sich mal anzuschauen, was das eigentlich genau ist. Kurz darauf stand für uns fest: Das wollen wir auch.

Je nach Erfahrungslevel beginnen die Kinder und Jugendlichen beispielsweise mit [Scratch], einer aufs Erlernen ausgelegte Programmiersprache und -Umgebung.
Den Sprung zur Hardwareebene schaffen kleine Platinencomputer wie das [BBC-micro:bit], von denen wir einige zur Verfügung stellen können.
Wenn auf eine visualisierte Darstellung des Codes verzichtet werden kann, kommt gerne die Programmiersprache Python zum Einsatz.

[Scratch]: https://scratch.mit.edu/ 
[snap]: https://snap.berkeley.edu/
[BBC-micro:bit]: https://microbit.org/guide/

Auch 3D-Druck, bei dem die Modelle ähnlich wie beim Programmieren erstellt werden können, oder der Bau eines [Elektromotors] ist bei uns möglich.

[Elektromotors]: http://www.eschke.com/


