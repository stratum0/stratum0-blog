---
layout: post
title: "Yes, we blag!"
date: 2013-09-06 14:08
author: valodim
comments: true
categories: [german,first, meta]
---

...wurde ja auch mal Zeit.

Die Idee, einen Blog mit relevanten Neuigkeiten zu pflegen, ist älter als unser
Eintrag im Vereinsregister. Meine Einstellung war bisher "kein Blog ist besser
als ein toter Blog," was denke ich auch nach wie vor zutrifft.

Was hat sich geändert? Nichts Großes, aber viele kleine Sachen. Wir werden
demnächst einen supertollen großen neuen [Space] haben, über dessen Ausbau es
sicherlich einiges zu berichten gibt. Wir haben mittlerweile ein aktives
[CTF-Team], das hoffentlich aktiv Writeups beisteuern wird. Es finden dann und
wann Veranstaltungen (Android-Workshop, Cryptoparty, ...) statt, bei denen der
Eintrag in der "Aktuelles"-Box im Wiki kaum als Understatement zu bezeichnen
ist.

Ich jedenfalls bin frohen Mutes, dass das Experiment Blog von Erfolg gekrönt
sein wird. Und wenn in vier Wochen noch von niemandem ausser mir ein Beitrag
geschrieben wurde: rm -rf ~/blog.

Bin gespannt =)

[space]: https://stratum0.org/wiki/Space_v2.0 "Space 2.0"
[ctf-team]: http://ctftime.org/team/1684 "Stratum 0 @ ctftime"
[cryptoparty]: http://stratum0.org/cryptoparty "Cryptoparty"
