---
layout: post
title: "Corona-Regelupdate September 2021"
date: 2021-09-19 22:30
comments: true
author: larsan
categories: [german, stratum0, covid19]
---

Viel hat sich in den letzten Wochen und Monaten bezüglich der
Corona-Vorgaben verändert.
Auch wir wollen unsere Regeln an die aktuelle Situation anpassen 
und den Space wieder weiter öffnen.

Einige der Erfahrungen der letzten Monate haben wir als sehr gut
bewertet und möchten Teile davon auch ersteinmal bei behalten.

| __**2021-09-19: Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**__
|

* Im gesamten Space dürfen sich maximal 10 Personen aufhalten.
* Im Space ist eine Mund-Nasen-Bedeckung zu tragen.
  Ausnahmen hiervon sind die Nahrungs- und Flüssigkeitsaufnahme.

|

* Belegungskoordination:
  * Der Space muss im Spacenutzungsplan-Pad reserviert und die Einträge
    ggf. aktualisiert werden. Bitte tragt auch mitgebrachte Gäste 
    (z.B. mit "+1") ein, damit die Gesamtanzahl an Entitäten im Space
    nicht überschritten wird.
  * Allgemein gilt weiterhin: Sprecht euch bei Unklarheiten direkt ab!
  * Der Space-Status muss bei Anwesenheit von Personen auf “offen”
    gesetzt werden, und bei Nichtanwesenheit von Personen auf “zu”.

|

* Wer den Space betritt, muss weiterhin seine Kontaktdaten hinterlassen:
  * Hierzu liegen Zettel aus, die ausgefüllt in einen verschlossenen
    Kasten geworfen werden.
  * Wir werden auf diesen Zetteln Name, einen Kontaktweg und den
    Zeitraum Deines Besuches erfassen.
  * Diese Daten werden nach drei Wochen vernichtet. 
    Die gesammelten Daten werden nur zur Nachverfolgung eventueller 
    Infektionsketten genutzt.

| 

* Hygieneregeln:
  * Vor und nach der Nutzung eines Raumes muss dieser, so weit möglich,
    gründlich (mindestens 3 Minuten lang) gelüftet werden. Während der
    Nutzung bietet es sich an, mindestens ein Fenster gekippt zu lassen.
  * Bitte wasche Dir regelmäßig die Hände und verhalte Dich hygienisch.
  * Benutze Papierhandtücher.
  * Und natürlich: Wenn Du dich krank fühlst, komm nicht in den Space!

Happy Hacking!

PS: Der Vorstand versucht diese Regelungen laufend zu optimieren. 
Wenn Du Änderungsvorschläge hast, lass uns diese gern wissen.

