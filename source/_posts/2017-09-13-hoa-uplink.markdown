---
layout: post
title: "Hacken Open Air – Internet-Grundversorgung im Neuland"
date: 2017-09-13 12:00
comments: true
author: chrissi^, Kasa
categories: [english, hoa]
---

Stratum 0 hosted the [Hacken Open Air] hacker camp in Peine. Over 120 hackers attended our camp and did a lot of nerd stuff. But as hackers we want to be online - even while camping.
Thus beside having warm outdoor-showers, campfire and music we needed to care about internet connectivity.

[Hacken Open Air]: https://hackenopenair.de/

The classical approach
----------------------

Our first attempt was DSL via good old landline. An insider in one of the bigger German DSL providers scheduled a measurement for us: around 6000 kBit/s were possible there. Well… that won't do the job.

Reaching out
------------

The next step was approaching the nearby industry. Since they all suffer a similar (yet not so harsh) fate, nothing was to gain on that front.

So we looked further, literally. Since we knew the people running the ham radio repeater DB0OI in Braunschweig we kicked up [linktool]: 17.1 km and even with a 10 m tower on the camp ground we got the following profile:

[linktool]: http://ham.remote-area.net/linktool/index

{% fancyalbum 200x200 %}
images/posts/2017-09-13-hoa-uplink/linktool-db0oi.png: Height profile between DB0OI and our campground.
{% endfancyalbum %}
This did not fare well on our planned 24 GHz Ubnt AirFiber link. As a backup, we ordered a pair of 5 GHz Ubnt PowerBeams.

Searching for other hops we found [DL0PTB] - still located in Braunschweig but closer to Peine and located a bit higher than DB0OI.

[DL0PTB]: http://www.dl0ptb.de/

{% fancyalbum 200x200 %}
images/posts/2017-09-13-hoa-uplink/linktool-dl0ptb.png: Height profile between DL0PTB and our campground.
{% endfancyalbum %} 


We gave that a try with our 10 m tower:

{% fancyalbum 200x200 %}
images/posts/2017-09-13-hoa-uplink/tower.jpg: Antenna-tower on the campground.
{% endfancyalbum %} 

As you might have guessed: It did not work. Neither the AirFiber nor the PowerBeam could establish a link.
But on the bright side: At this point we had the go from the PTB guys to use their uplink.
With three days left until the event, we were already prepared to use a LTE uplink.

Back to Peine
-------

We went back to the local industry. This time not asking for connectivity but for a high building and a power outlet. Luckily we were allowed to use a 32 m building with line of sight to our tower on the camp ground.
Again we tried to establish the link. Better prepared this time, we brought a scope to help us align our radio equipment.

{% fancyalbum 200x200 %}
images/posts/2017-09-13-hoa-uplink/telescope.jpg: Looking from DL0PTB to Peine.
images/posts/2017-09-13-hoa-uplink/linktool-dl0ptb-industry.png: Heigh-profile between DL0PTB and the industrial building.
{% endfancyalbum %} 

But no luck: We did not see our target building. With hopes low, we tried anyway.
To our surprise, we were able to establish the 5 GHz link with only basic alignment by landmarks. Around 50 Mbps link capacity on the first try.
In search for more bandwidth and as a backup link on another band, we tried aligning the 24 GHz hardware. But without line of sight, we only achieved an unstable link at -87 dBm Rx Power - being far too close to the sensitivity limit or noise floor (what ever hit first).
After some hours of aligning and beaconing, we gave up on the AirFiber and further tweaked the PowerBeam alignment.
In the end we achieved around 120 Mbps of link capacity, which exceeded our expectations by far.

Our guess: the 5 GHz Fresnel zone is a factor 2 wider than the one at 24 Ghz. What left the PowerBeam with more Rx Power and thus more link budget.

At this point we used two Ubnt NanoStations at 2.4 GHz as 500 m link to our campground. 


Looking pitiful
------

Fiddling with the links on the roof of the industry building our contact there had mercy and finally agreed to share some bandwidth with us.
So we did some rearranging on short notice: The AirFiber was used on the link to the campground (thanks to perfect LOS conditions) and the NanoStations were used to establish a link to a switch port in an auxiliary building within the complex, performing perfectly on a 100 m link without direct line of sight and only very rough alignment.


New Problems
-------

At this point we had two fully functional uplinks. Both at around 50 Mbit/s. But our ChaosVPN endpoint did not allow us to use multiple uplinks. Additionally someone in the camp threw in an unlimited LTE SIM card.

Shoragan and Emantor ended up terminating the ChaosVPN on [Endor] - the Stratum 0 server and using [Equal Cost Multipath Routing] over multiple OpenVPN connections from the camp to Endor.

[Endor]: https://stratum0.org/wiki/Endor
[Equal Cost Multipath Routing]: https://datatracker.ietf.org/doc/rfc6438
