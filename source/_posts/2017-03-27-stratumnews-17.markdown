---
layout: post
title: "Stratumnews Vol. 17"
author: Stratumnews Team
date: 2017-03-27 11:07
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space.
<!--more-->

Causa Spendendose
-----------------

Die hölzerne Spendendose wurde aus dem Flur geklaut (siehe letzte Stratumnews), wir haben Anzeige erstattet.
Comawill hat eine [neue Spendendose][kl] gebaut! \o/ (clap4coma) Jetzt mit Kensington-Lock.

[kl]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/aCIeZIHejYcStVtebMlOJifG


Chillraum
---------

An den Chillraum-Fenstern wurden [Rollos][ch] angebracht, sodass die Lichteinfuhr gesteuert werden kann. Ebenso hat das Innenfenster zwischen Chillraum und Frickelraum ein Rollo bekommen, die Sperrholzplatten sind nun Geschichte.

[ch]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/OghzIDNlLKJaExmVhQugkARm


HOA-Presale
-----------

Das [Presale][ht] für das Hacken Open Air ist gestartet, bitte klickt euch eure Tickets. Mehr Infos unter [https://hackenopenair.de][ho].

[ht]: https://tickets.hackenopenair.de/
[ho]: https://hackenopenair.de/


Holodeck
--------

Das [Holodecklicht][hl] wurde aus dem defekten Zustand direkt auf die Version 2.0 upgegraded, jetzt mit Erdnuss485 statt I²C und mehr fancy. Die alten Links funktionieren weiterhin, die Dokumentation ist im Wiki.

Außerdem gibt es jetzt endlich einen echten Schalter zum Bedienen des Lichtes, nun muss man auch ohne netzwerkfähiges Gerät nicht im Dunkeln sitzen. (Keine Sorge, das Netzwerkgerät ist im Schalter (WLAN natürlich, lasst euch nicht vom Kabel täuschen)).

Außerdem hat die Küche ein neues Sofa bekommen, das alte Sofa wurde aufs Holodeck portiert, ein neuer Bezug ist in Anschaffung.

[hl]: https://stratum0.org/wiki/Holodeck/Licht


Video: Gründen in Braunschweig
------------------------------

Letztes Jahr war eine Filmcrew von [kitchen tv][kt] bei uns, um im Auftrag des Stadtmarketings ein paar Szenen für einen "Gründerfilm" zu drehen. Mittlerweile wurde das Video [veröffentlich und ist auf Vimeo][vi] zu finden.

[vi]: https://vimeo.com/192992813
[kt]: http://kitchen-tv.com/


Formalfoo
---------

Da ab und an Fragen kommen, wie eigentlich Anschaffungsanträge gestellt werden sollen, bzw. gehandhabt werden, gibt es jetzt ein eher ausführliches "[How To Antrag][an]". Nicht abschrecken lassen, es sieht schwieriger aus, als es ist.

Die öffentlichen Protokolle von den [letzten][v6] [beiden][v7] Vorstandssitzungen sind jetzt im Wiki zu finden.

[an]: https://stratum0.org/wiki/How_To_Antrag
[v6]: https://stratum0.org/wiki/Vorstandssitzung_2016-12-20
[v7]: https://stratum0.org/wiki/Vorstandssitzung_2017-03-13


Termine
-------

Nach über einem Jahr wird es nun am 22.04. endlich wieder einen [Sushiabend][su] geben! Anmeldung im Pad, erscheinet zahlreich:

[su]: https://stratum0.org/wiki/Sushiabend

Weitere Termine:

* Di, 28.03. 16:30: Schreibrunde
* Di, 28.03. 18:00: Malkränzchen
* Di, 28.03. 19:00 - 23:00: Anime Referat
* Mi, 29.03. 19:00: Freifunk-Treffen
* Mi, 29.03. 19:00: Vegan Academy
* Do, 30.03. 14:00: Arbeitsgruppe SPAC3
* Do, 30.03. 19:00 - 23:00: Captain's Log
* Sa, 01.04. 16:00: CoderDojo Vorbereitungsbasteln
* Di, 04.04. 16:30: Schreibrunde
* Di, 04.04. 18:00: Malkränzchen
* Di, 04.04. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Mi, 05.04. 19:00: Freifunk-Treffen
* Do, 06.04. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
* Di, 11.04. 16:30: Schreibrunde
* Di, 11.04. 18:00: Malkränzchen
* Di, 11.04. 19:00 - 23:00: Anime Referat
* Mi, 12.04. 19:00: Freifunk-Treffen
* Mi, 12.04. 19:00: Vegan Academy
* Do, 13.04. 19:00 - 23:00: Captain's Log
* Fr, 14.04. bis Mo, 17.04.: EasterHegg 2017, Mühlheim am Main
* Di, 18.04. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Do, 20.04. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
* Sa, 22.04. 18:30: Sushiabend - Sushi bauen
* So, 23.04. 14:00: Freifunk-Reboot Workshop (Ort wird noch bekannt gegeben)
* Do, 25.05. bis So, 28.05.: GPN17, Karlsruhe
* Mi, 21.06. 01:00 bis Mi, 21.06. 07:00: Sommersonnenwendenwanderung 2017


Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!

