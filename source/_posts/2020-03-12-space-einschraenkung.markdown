---
layout: post
title: "Vorübergehende Einschränkungen im Spacebetrieb"
date: 2020-03-12 23:00
comments: true
author: chrissi^
categories: [german, stratum0, covid19]
---

 > Update vom 16.03.2020:
 > Der Space wird für einige Zeit vollständig geschlossen.
 > Mehr Informationen gibt es in [diesem Blogpost](https://stratum0.org/blog/posts/2020/03/16/space-geschlossen/).
 
Die Einschläge kommen näher, gestern erklärte die WHO die Ausbreitung
des Coronavirus zur [Pandemie][1].

Es gibt Maßnahmen, die insbesondere am Anfang dieser Entwicklung
sinnvoll sind ([hier][3], [NDR][4]), darunter: weniger soziale Durchmischung.
Der Space als Schmelztiegel des sozialen Miteinanders ist damit in
einer Position, wo wir etwas verändern können.
Einige Schulen in Niedersachsen bleiben bereits [geschlossen][5], und
auch die Informatik der TU Braunschweig bereitet ihre Studenten auf
Einschränkungen vor.

Im Space finden die verschiedensten Veranstaltungen statt: kleine
Gruppen von Entitäten, die eh jeden Tag da sind, aber auch Events, bei
denen viele Externe mehrere Stunden auf engerem Raum beisammensitzen.
Prominentestes Beispiel ist dafür wohl das CoderDojo.
Der ständige soziale Kontakt von Spacemembern untereinander hat nun
bisher schon stattgefunden, und es macht wenig Sinn, diesen nun zu
unterbinden.
Aber gerade die Veranstaltungen mit hohem Anteil an externen Personen
erleichtern die Ausbreitung des Virus.
Hier eine Grenze zu ziehen ist schwierig, der Rahmen des Möglichen
bewegt sich von "keine großen Gruppen", über "nur Mitglieder" bis hin
zur kompletten – temporären – Schließung des Spaces.


Daher ist es aus unserer Sicht derzeit sinnvoll – zunächst für zwei
Wochen – sämtliche Veranstaltungen im Space abzusagen, also auch die
Vorträge, Freifunk etc.


Darüber hinaus möchten wir den Zutritt zum Space für die selbe Dauer
auf Mitglieder beschränken.
Einerseits wollen wir dadurch die soziale Durchmischung auf das
Nötigste beschränken und andererseits können wir im Falle einer
nachgewiesenen Infektion die Menge potenzieller Kontaktpersonen besser
nachvollziehen und diese ggf. kontaktieren.

Das bedeutet nicht, dass der Space vollständig geschlossen ist. Wir
möchten mit dieser Maßnahme aber unseren Beitrag zur Vermeidung von
nicht notwendigen sozialen Kontakten leisten.

Zu guter letzt noch der obligatorische Hinweis: falls ihr euch krank
fühlt, bleibt bitte vorerst zuhause und werdet gesund.
Falls zudem Verdacht auf eine Infektion mit dem Coronavirus besteht,
helfen euch die unten stehenden Links sowie die offiziellen
[Informationen der Stadt Braunschweig][2] und des [Robert-Koch-Instituts][6]
weiter.
Solltet ihr im Falle einer gesicherten Infektion durch Ärzte oder Ämter
gebeten werden, mögliche Kontaktpersonen zu ermitteln, so helfen wir
euch natürlich dabei.
Darüber hinaus möchten wir euch bitten, uns im Falle einer gesicherten
Infektion zu informieren, falls ihr zwei Wochen vor Symptombeginn (oder
danach) im Space wart.
Bitte meldet euch in diesem Fall bei uns per Mail an 
<vorstand@stratum0.org> (oder falls euch telefonieren lieber ist, könnt
ihr euch bei Chrissi unter 0160 84 97 884 melden).


Wir behalten – gemeinsam mit euch – die Situation im Auge. Sollte es
sinnvoll sein, passen wir diese Regelungen natürlich an.


Der Vorstand

[1]: https://www.who.int/dg/speeches/detail/who-director-general-s-opening-remarks-at-the-media-briefing-on-covid-19---11-march-2020
[2]: http://www.braunschweig.de/politik_verwaltung/nachrichten/corona.php
[3]: https://medium.com/@tomaspueyo/coronavirus-act-today-or-people-will-die-f4d3d9cd99ca
[4]: https://www.ndr.de/nachrichten/info/12-Schulen-schliessen-und-Gemeinden-unterstuetzen,audio652058.html
[5]: https://www.vmz-niedersachsen.de/wissenswertes/schulausfall/
[6]: https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/nCoV.html
