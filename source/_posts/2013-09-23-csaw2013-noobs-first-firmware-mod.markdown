---
layout: post
title: "CSAW 2013: Noobs First Firmware Mod"
date: 2013-09-23 00:42
author: comawill
comments: true
categories: [english,ctf,writeup,CSAW2013, reverse]
---

## The task

N00b firmware modder says: "My first u-boot mod, there might be errors :("
noobs-first-firmware-mod.tgz

<!-- more -->

## The solution
We analyzed the code of the file `u-boot` and found a suspicious function `do_csaw`.
So we converted the assembly code into a bit more readable python code:

``` python
#!/usr/bin/env python
buffer = [0,]*200

def write(pos, val):
	buffer[pos] = val

def read(pos):
	return buffer[pos]

def memcpy(dst, src, leng):
	for i in range(leng):
		write(dst+i, read(src+i))


R5 = 100
R5_delta = 0x80002013-R5
R4_delta = 0x7fb8088

for i,c in enumerate("SUPERSEXYHOTANDSPICY"):
	write(100+i, ord(c))

for i,c in enumerate("key="):
	write(0+i, ord(c))

R4 = 0
R11 = R5+40
R10 = R4 + 4
R7 = R4
R6 = R4
i = 0
while R5 != R11:
	memcpy(R10,R5,1)
	R5 += 2
	R3 = read(R7+4)
	write(R6+5, ord('-'))
	R7 +=1
	write(R6+4, R3)
	R6 +=2
	

write(R4+0x2B, 0)

# Dump Memory
print repr("".join(map(chr,buffer)))
```

Output:

	key=\x00--------------------------------------\x00

Obviously this is not the key...

So we wasted a lot of time looking through the binary, simulated it with qemu (`qemu-system-arm  -kernel u-boot -M versatilepb -m 3G -nographic`) which also din't help.

After multiple additional hours we decided to add `R10 += 1` after line 32 and got the output

	key=SPREYOADPC\x00

we tried "SPREYOADPC" as key and got the points.

