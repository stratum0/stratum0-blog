---
layout: post
title: "Freifunk: Project Parker"
date: 2018-12-22 20:00
comments: true
author: chrissi^
categories: [german, freifunk]
---

Project Parker: Klassisches Routing für Freifunk
==========================================================

Shoragan und ich haben auf dem 35C3 über das Project Parker gesprochen.
Unsere Vortragsankündigung lautete:

> Gluon mit BATMAN setzt klassisch auf ein großes Layer 2 Netz, was in den großen 
> Freifunk Communities zu Problemen führt. 
> Diese Communities begegnen den Problemen i.d.R. mit der Segmentierung des 
> Netzes in Domains. 
> Innerhalb dieser Domains wird allerdings das klassische Netz (BATMAN) eingesetzt.
> 
> Mit "Project Parker" versuchen wir diese Probleme durch klassisches Routing 
> in einem hierarchischem Netz zu lösen: Jeder Freifunk-Knoten mit Uplink ist 
> Gateway für sein Netzsegment, macht DHCP und ist DNS-Cache.
> Das Routing mit BATMAN beschränkt sich nun auf kleine Mesh-Netze innerhalb 
> des gesamten Netzes.
> 
> Wir stellen in unserem Talk unsere Architektur vor und zeigen welchen Stand
> unsere Umsetzung erreicht hat.

Hier unsere Ankündigung im [OIO pretalx].

Unsere [Präsentation] kann man hier herunterladen.

Update 20190109: Unser Talk ist [online] verfügbar.

[OIO pretalx]: https://pretalx.35c3oio.freifunk.space/35c3oio/talk/V3XY87/
[Präsentation]: /images/posts/2018-12-28-project-parker-35c3.pdf
[online]: https://media.ccc.de/v/35c3oio-69-project-parker-klassisches-routing-fr-freifunk
