---
layout: post
title: "Stratumnews Vol. 28"
author: Stratumnews-Team
date: 2019-09-29 22:10
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche/jährliche *hust* Zusammenfassung for all things space.
Die letzten Stratumnews wurden am 01.07.2018 verschickt, es ist also alles schon etwas her. Das heißt aber auch, dass alles was hier steht, garantiert nicht vollständig ist und im letzten Jahr eigentlich noch viel viel mehr im Space passiert ist und angeschafft und gebaut wurde.


<!-- more -->


MV 2019
-------

Im Januar fand die 10. Mitgliederversammlung statt, Kurzzusammenfassung in einem [Blogpost][], Live-Protokoll ist dort verlinkt.

[Blogpost]: https://stratum0.org/blog/posts/2019/01/20/mitgliederversammlug/


0b1000
------

War großartig \o/


CCCamp 2019
-----------

War großartig \o/


Hacken Open Air
---------------

War 2018 wieder sehr großartig.
Allgemeiner Konsens war, 2019 kein HOA stattfinden zu lassen, da schon sehr viele Ressourcen mit dem CCCamp2019 gebunden waren.
2020 wird es wohl wieder ein HOA geben, die Planungstreffen haben begonnen, kommt vorbei!


Vorträge
--------

Seit den letzten news gab es etliche Vortragsabende und natürlich auch wieder einige [Aufzeichnungen][].

[Aufzeichnungen]: https://www.youtube.com/user/stratum0/videos


pretix
------

Für Workshops und sonstige geeignete Veranstaltungen läuft auf einer VM (smaut) auf dem Vereinsserver nun ein [pretix][].
Zur Zeit wird es produktiv fürs CoderDojo genutzt.

[pretix]: https://smaut.stratum0.org/


Mehr Beamer und Audio
---------------------

In der Küche hängt ein Beamer unter der Decke, auf einem daran angeschlossenen Pi läuft ein Kodi, kann mit üblichen Endgeräteapps (z.B. Kore) angesteuert werden, ist auch auf dem Matekassentablet installiert.
Audio am Küchenverstärker hat jetzt drei Inputs, das Kabel am Werkstattwürfel, Kodi, sowie einen Bluetoothadapter.
Im Frickelraum hängt ebenfalls ein Beamer, ebenfalls mit einem Pi mit Kodi daneben. Audio muss hier noch eingerichtet werden.


Treffen mit ags und akafunk
---------------------------

Um die technikaffinen Organisationen in unserem Umfeld zu vereinen, gibt es seit geraumer Zeit ein unregelmäßiges Treffen zwischen der [akaFunk][], der [ags][] und dem Stratum 0.
Die Treffen finden bisher üblicherweise an einem Freitagabend statt und vor und in den Räumen der ags, im Grotrian.

[akaFunk]: https://akafunk-bs.de/
[ags]: https://www.ags.tu-bs.de/


misc
----

* Auf trokn wurde etwas umsortiert; im Root-Verzeichnis können User jetzt keine Dinge mehr an/ablegen.
* Infodisplay b0rked, repaired, b0rked, semi repaired, b0rked.
* Der Stick-/Näh-Arbeitsplatz hat eine weitere [Ablage][stick] bekommen.
* Es gibt jetzt je eine Filz- und Lederrestekiste in der Stickecke.
* Ebenso gibt es eine Box Lederbearbeitungswerkzeug auf dem Regal in der Lötecke.
* Die Werkbank hat einen [Multifunktionstisch][multi] inkl. Spannelementen bekommen (Box unter dem Tisch), und eine [Vorderzange][zange].
* Im Flur hängt jetzt wieder funktionsfähige [Kunst][kunst]
* Der Space hat jetzt ein Punktschweißgerät, damit kann man z.B. Laptopakkus mit frischen Lithiumzellen bestücken.
* Die Klimaanlage im Frickelraum wurde jetzt etwas permanenter installiert.
* Holodecklüfter wurde ausgetauscht und angebracht.
* Die Steckdose für den Staubsauger im Flur wurde angeschlossen.
* Die Lötecke hat ein LED-Panel an der Decke bekommen, Steuerung über den Sonoff-Taster im Brüstungskanal oder <http://192.168.178.73/>
* Der Gefrierschrank wurde mit einem ESP32 und einem Solid State Relay repariert und läuft jetzt nicht mehr 24/7 durch, schickt dafür seine Temperatur an den MQTT-Broker.
* Der Löteckentisch hat eine Untertischbeleuchtung, für falls mal wieder ein Bauteil runtergefallen ist.
* Der zweite Löteckentisch hat ein Sortiment/Ordnungssystem an der Wand bekommen.
* Wir haben von intech einen Satz gebrauchte Bürostühle bekommen.
* In der Küche gibt es einen neuen, größeren Pizzaofen.

[stick]: https://chaos.social/@daniel_bohrer/100811762096209446
[kunst]: https://chaos.social/@daniel_bohrer/100810499983919371
[multi]: https://chaos.social/@daniel_bohrer/102034293067264331
[zange]: https://chaos.social/@daniel_bohrer/102835660505824546


Lost&Found
----------

* Lost: Smash Up, Kartenspiel. Der Space besitzt das Basisspiel und eine Erweiterung, sind allerdings nicht im Space zu finden. Sehen ungefähr so aus: <https://www.amazon.de/Pegasus-Spiele-17260G-Smash-Kartenspiele/dp/B008YEBTO0/>
* Found: Siemens Gigaset E365 mit Verpackung, Lag irgendwann mal in der Küche, jetzt im L&F


Termine
-------

* Fr, 27.09. 20:00 bis So, 29.09. 20:00: Projektbeendewochenende
* Mo, 30.09. 18:00: Malkränzchen, offener Kreativabend
* Di, 01.10. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Di, 01.10. 19:00: Vorstands-Arbeitstreffen
* Mi, 02.10. 19:00: Freifunk-Treffen
* Do, 03.10. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Sa, 05.10. 18:00: Chillraum: Black Mirror Viewing
* Mo, 07.10. 18:00: Malkränzchen, offener Kreativabend
* Di, 08.10. 19:00: C64 Demo, Treffen im Stratum0
* Mi, 09.10. 19:00: Freifunk-Treffen
* Do, 10.10. 19:00 - 23:00: Captain's Log
* Mo, 14.10. 18:00: Malkränzchen, offener Kreativabend
* Mo, 14.10. 19:00: Vorträge (Aufzeichnungen)
* Di, 15.10. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Mi, 16.10. 19:00: Freifunk-Treffen
* Do, 17.10. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Sa, 19.10. 14:00: CoderDojo
* Mo, 21.10. 19:00: Blinken-Zeug-Treffen
* Di, 22.10. 19:00: C64 Demo, Treffen im Stratum0
* Di, 22.10. 19:00: HackenOpenAir 2020 Orgatreffen
* Do, 24.10. 19:00 - 23:00: Captain's Log
* Sa, 16.11. 10:00 - 17:00: Kassenzwischenprüfung (Frickelraum reserviert)


Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!

