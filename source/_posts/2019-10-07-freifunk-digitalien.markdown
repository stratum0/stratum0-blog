---
layout: post
title: "Freifunk beim Logbuch Digitalien"
date: 2019-10-07-21:12
comments: true
author: chrissi^
categories: [german, freifunk]
---

Der Podcast *Logbuch Digitalien* hatte den Freifunk Braunschweig eingeladen.
Wir haben die Einladung gern angekommen.
So haben sich Larsan und Chrissi^ vom Freifunk mit
[Markus Hörster] und [Christian Cordes] bei [Radio Okerwelle] getroffen.

{% fancyalbum 300x300 %}
images/posts/2019-10-07-freifunk-digitalien.jpg
{% endfancyalbum %} 

Dort hatten wir Gelegenheit das Projekt Freifunk Braunschweig vorzustellen und
ein wenig aus dem Nähkästchen zu plaudern.

Die Folge gibt es auf [www.logbuch-digitalien.de] zum anhören und herunterladen.
Natürlich gibt es das Logbuch Digitalien auch bei allen großen Podcast-Suchen.
Viel Spaß beim Hören!

Hast du auch ein Thema, dass du gern im *Logbuch* besprechen möchtest? 
Themenvorschläge sind bei Markus und Christian willkommen.

[Markus Hörster]: https://www.markushoerster.com/
[Christian Cordes]: https://twitter.com/erlebnischris
[Radio Okerwelle]: https://okerwelle.de/
[www.logbuch-digitalien.de]: https://www.logbuch-digitalien.de/podcast/episode-31-funken-und-frequenzen/



