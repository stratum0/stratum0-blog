---
layout: post
title: "Corona-Regelupdate April 2022"
date: 2022-04-11 22:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

| __**2022-04-11: Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**__
|

__**tl;dr: 2G und CWA. Veranstaltungen möglich.**__

Im Space gilt eine "2G"-Regelung. Folgende Personen haben Zutritt:

* Geimpfte und genesene Personen.
* Ebenfalls Zutritt haben Personen, die sich nicht impfen lassen können
    (mit Nachweis)
    und Kinder unter 5 Jahren.

## Dies setzen wir wie folgt um:

* Alle Mitgliedsentitäten sind Betreiber des Spaces, und damit selbst
    für die Einhaltung der Regelung verantwortlich.
* Wer Gäste mitbringt, ist dafür verantwortlich, dass diese die Regeln
    einhalten.
* Wenn im normalen Spacebetrieb Gäste vor der Tür stehen, so sind die im
    Space anwesenden Mitglieder dafür verantwortlich,
    diese vor Zutritt auf die 2G-Regelung hinzuweisen, die Einhaltung
    sicherzustellen und bei Nichteinhaltung,
    oder bei Nichteinwilligung zur Überprüfung den Zugang zum Space zu
    verwehren.

## Veranstaltungen

* Geplante Veranstaltungen sind im Space-Terminkalender einzutragen,
     dabei bitte maximal eine Veranstaltung im Space gleichzeitig.
* Für Veranstaltungen ist mindestens eine anwesende Entität zu definieren,
     die die Kontrolle der 2G-Regelung zu verantworten hat.
     Dies kann zum Beispiel auch über den “Kontakt”-Eintrag auf der Veranstaltungsseite
     im Wiki passieren.
* Veranstaltungen können Maskenpflicht festlegen.
     Dies muss dann beim Kalendereintrag dabei stehen und gilt für alle Anwesenden im Space.
     Restliche Regeln (2G) gelten weiterhin.

## Kontaktverfolgung

* Im Space hängen CWA-QR-Codes, bitte checkt euch ein (und wieder aus).
* Wer den CWA-QR-Code nicht nutzen kann oder möchte, benutzt einfach
    weiterhin die bekannten Zettel.
* Gebt uns Bescheid, wenn es zu positiven Fällen kommt, denn nur so
    können wir die Entitäten im jeweils anderen System warnen.

## Hygienemaßnahmen:

* Wascht euch die Hände und haltet den Space sauber.
* Lüftet, insbesondere zu Beginn und zum Ende eures Spacebesuchs.
     * Wenn es die Witterung zulässt, haltet den Space auf Durchzug.
* Komme nicht in den Space, wenn du dich krank fühlst.
* Trage im öffentlichen Flurteil weiterhin Maske und halte die Tür zum
    Flur geschlossen.

_Dieser Text darf gerne frei kopiert, übernommen und verändert werden.
Falls eine Lizenzangabe nötig ist: CC0_
