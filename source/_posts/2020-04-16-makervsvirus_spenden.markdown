---
layout: post
title: "Spenden für MakerVsVirus - Hub Braunschweig"
date: 2020-04-16 16:33
comments: true
author: larsan
categories: [german, stratum0, covid19]
---

Falls ihr uns finanziell unterstützen, gibt es mehrere Möglichkeiten: 

## 1. Spende über Stratum 0 e.V.

Da der Stratum 0 e.V. gemeinnützig ist, können diese Spenden steuerlich geltend gemacht werden. Für Spenden bis 200 EUR reicht i.d.R. der Kontoauszug als Beleg gegenüber dem Finanzamt aus. Bei größeren Spenden setzt euch bitte unter vorstand@stratum0.org mit uns in Verbindung. 

### Bankverbindung
    Verwendungszweck: MakerVsVirus
    Kontoinhaber: Stratum 0 e.V.
    SWIFT-BIC: NOLADE2HXXX (Nord/LB Hannover)
    IBAN: DE40 2505 0000 0200 0249 17

Diese Informationen findet ihr auch unter [Spenden](https://stratum0.org/wiki/Spenden) im Wiki.

Der aktuelle Spendenstand ist [hier](https://data.stratum0.org/finanz/) unter "Aktuelle Zweckbindungen" zu finden. 


## 2. Spende über den Paypal Moneypool

*Wichtig: Hierfür kann vom Stratum 0 **keine** Spendenquittung ausgestellt werden!*

Der [Moneypool](https://www.paypal.com/pools/c/8nIcxOA4h8) wurde bereits zu Beginn der Aktion eingerichtet und hat maßgeblich zur guten Handlungsfähigkeit des Braunschweiger Hubs beitragen. Für Kleinspenden und Spenden, für die keine Quittung benötigt wird möglicherweise der komfortablere Spendenweg.

Vielen Dank für eure Unterstützung!
