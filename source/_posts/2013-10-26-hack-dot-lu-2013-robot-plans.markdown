---
layout: post
title: "Hack.lu 2013: Robot Plans"
date: 2013-10-26 17:56
comments: true
author: doommaster & spq
categories: [english,ctf,writeup,hack.lu13,forensics]
---

<span style="font-family: Iceland,sans; font-size: 18px;">We have captured a robot from behind, while he dropped some cooling liquid into the bushes. We tried to interrogate the robot, but he still refuses to speak. Luckily we could extract files from the android's communication module. Hopefully we get some information about the robots' motives, before every information is swiped away...
Here is the challenge: http://link.to.image.tar.gz</span>

<!-- more -->

The description isn't that helpful, but it still hints to Android and some kind of forensic task.

The first thing to do is always having a brief look at the file itself but it looked unsuspicios like a simple compressed tar file.

``` sh
tar xaf image.tar.gz
```

Surprise! It seems to be a complete android rootfs.

``` sh
tree -d -L 3 -I "sys|proc"
.
├── cache
│   ├── dalvik-cache
│   └── recovery
├── data
│   ├── anr
│   ├── backup
│   │   └── pending
│   ├── data
│   │   ├── com.android.bluetooth
│   │   ├── com.android.calendar
│   │   ├── com.android.camera
│   │   ├── com.android.contacts
│   │   ├── com.android.deskclock
│   │   ├── com.android.email
│   │   ├── com.android.htmlviewer
│   │   ├── com.android.inputmethod.latin
│   │   ├── com.android.launcher
│   │   ├── com.android.mms
│   │   ├── com.android.phone
│   │   ├── com.android.providers.calendar
│   │   ├── com.android.providers.contacts
│   │   ├── com.android.providers.downloads
│   │   ├── com.android.providers.media
│   │   ├── com.android.providers.settings
│   │   ├── com.android.providers.telephony
│   │   ├── com.android.providers.userdictionary
│   │   ├── com.android.settings
│   │   ├── com.cyanogenmod.cmparts
│   │   ├── com.cyanogenmod.stats
│   │   ├── com.koushikdutta.rommanager
│   │   └── org.openintents.cmfilemanager
│   ├── misc
│   │   ├── bluetooth
│   │   └── wifi
│   ├── property
│   └── system
│       ├── dropbox
│       ├── registered_services
│       ├── sync
│       ├── throttle
│       └── usagestats
├── mnt
│   └── sdcard
│       ├── DCIM
│       ├── documents
│       ├── download
│       └── media
├── sbin
└── system
    ├── app
    ├── bin
    ├── etc
    │   ├── bash
    │   ├── bluetooth
    │   ├── dhcpcd
    │   ├── init.d
    │   ├── nano
    │   ├── permissions
    │   ├── ppp
    │   ├── security
    │   ├── terminfo
    │   └── wifi
    ├── fonts
    ├── framework
    ├── lib
    │   ├── bluez-plugin
    │   ├── egl
    │   ├── hw
    │   ├── modules
    │   └── soundfx
    ├── media
    │   └── audio
    ├── tts
    │   └── lang_pico
    ├── usr
    │   ├── icu
    │   ├── keychars
    │   ├── keylayout
    │   ├── share
    │   └── srec
    ├── wifi
    └── xbin

82 directories
```{: style="max-height: 300px; overflow: auto;"}

On a fist glance it becomes clear that we have a Cyanogenmod image, so let's have a look which CM it is.

``` sh
grep 'model\|build.id' system/build.prop 
  ro.build.date=Fri Mar  1 07:40:07 PST 2013
  ro.build.date.utc=0
  ro.product.model=U8160
```

So it is some Huawei phone. Another thing to check is the phonenumber and some logs (if they exist).
Sadly the image is too old and we cannot get a 1:1 copy of the original.
But nice enough, the image was expanded Sept 12 and changes were done Sept 13, so we can get a list of all changed files.

``` sh
find . -type f -exec ls -l {} \;|awk '{print $6, $7, $8, $9}'|grep -v 'Sep 12'
  Sep 13 09:46 ./system/etc/wifi/wpa_supplicant.conf
  Sep 13 09:16 ./data/system/accounts.db
  Sep 13 09:16 ./data/system/usagestats/usage-20130910
  Sep 13 09:16 ./data/system/usagestats/usage-20130911
  Sep 13 09:16 ./data/system/usagestats/usage-20130912
  Sep 13 09:16 ./data/system/wallpaper_info.xml
  Sep 13 09:16 ./data/system/appwidgets.xml
  Sep 13 09:16 ./data/system/sync/pending.bin
  Sep 13 09:16 ./data/system/sync/status.bin
  Sep 13 09:16 ./data/system/sync/accounts.xml
  Sep 13 09:16 ./data/system/sync/stats.bin
  Sep 13 09:16 ./data/system/batterystats.bin
  Sep 13 09:16 ./data/system/dropbox/system_app_anr@1378836303117.txt.gz
  Sep 13 09:16 ./data/system/dropbox/SYSTEM_BOOT@1378836269774.txt
  Sep 13 09:16 ./data/system/dropbox/SYSTEM_RECOVERY_LOG@1378836269739.txt.gz
  Sep 13 09:16 ./data/system/throttle/temp
  Sep 13 09:16 ./data/system/throttle/-1883440333
  Sep 13 09:16 ./data/system/uiderrors.txt
  Sep 13 09:16 ./data/system/packages.xml
  Sep 13 09:16 ./data/system/packages.list
  Sep 13 10:10 ./data/system/gesture.key
  Sep 13 09:16 ./data/system/called_pre_boots.dat
  Sep 13 09:16 ./data/system/registered_services/android.accounts.AccountAuthenticator.xml
  Sep 13 09:16 ./data/system/registered_services/android.content.SyncAdapter.xml
  Sep 13 09:16 ./data/system/device_policies.xml
  Sep 13 09:16 ./data/system/profiles.xml
  Sep 13 09:16 ./data/system/entropy.dat
  Sep 13 09:16 ./data/anr/traces.txt
  Sep 13 09:16 ./data/misc/wifi/nvram.txt
  Sep 13 09:16 ./data/misc/bluetooth/bdaddr
  Sep 13 12:14 ./data/backup/ccaiq3
  Sep 13 11:39 ./data/backup/jecd1q
  Sep 13 12:55 ./data/backup/bmkk0i
  Sep 13 10:22 ./data/backup/kxnu9f
  Sep 13 12:26 ./data/backup/6t1tdd
  Sep 13 10:28 ./data/backup/rvfoaw
  Sep 13 12:49 ./data/backup/1hqqf0
  Sep 13 11:21 ./data/backup/v11yb7
  Sep 13 10:58 ./data/backup/tq4fs6
  Sep 13 12:31 ./data/backup/0ytutd
  Sep 13 10:40 ./data/backup/xnk0yh
  Sep 13 12:08 ./data/backup/4bm94d
  Sep 13 09:16 ./data/backup/pending/journal152335267.tmp
  Sep 13 12:43 ./data/backup/z5k0mb
  Sep 13 11:03 ./data/backup/ul6003
  Sep 13 11:09 ./data/backup/7cbxho
  Sep 13 13:01 ./data/backup/ks9s8k
  Sep 13 10:11 ./data/backup/150kfu
  Sep 13 10:16 ./data/backup/ua2udm
  Sep 13 10:34 ./data/backup/cun0mb
  Sep 13 11:44 ./data/backup/oq6liq
  Sep 13 10:52 ./data/backup/kaw5ua
  Sep 13 11:50 ./data/backup/xbu809
  Sep 13 11:56 ./data/backup/9yk5r9
  Sep 13 12:02 ./data/backup/uyqqkm
  Sep 13 10:46 ./data/backup/56bdap
  Sep 13 11:33 ./data/backup/312l4y
  Sep 13 12:37 ./data/backup/stwz86
  Sep 13 12:20 ./data/backup/f7mb17
  Sep 13 11:15 ./data/backup/xdh1gd
  Sep 13 11:27 ./data/backup/xe9b48
  Sep 13 09:16 ./data/data/com.android.providers.calendar/databases/calendar.db
  .
  .
  .
```{: style="max-height: 300px; overflow: auto;"}
\\
90% of those are default files, but some are "different" and suspicious, as `/data/backup/\*`, `/data/system/\*` and `/system/etc/wifi/wpa_supplicant.conf`
One could also sqlitebrowse all those DBs but we are still at the beginning so let us have a look at those plain files.

1. `/data/backup/` seems to contain random named files with sha1 hashes inside
2. `/data/system/gesture.key` contains `h.a.h.a.c.a.n.t.g.e.t.m.e.i.m.a.d.e.b.a.c.k.u.p.z.z.`
3. `/system/etc/wifi/wpa_supplicant.conf` contains some random WiFi credentials

There was one odd thing about those hashes in `/data/backup/` because they where duplicates.

``` sh
for f in `find data/backup/ -maxdepth 1 -type f`; do cat $f; echo; done|wc -l
30
for f in `find data/backup/ -maxdepth 1 -type f`; do cat $f; echo; done|sort|uniq|wc -l
7
```

Since I had no idea what those hashes might be good for, Google was my friend and even gave me the needed hint.
`23a6e7c835cd75c3f17ecc4d1cd7d840b74095251` led me to [Lock Pattern et Android..](http://thanat0s.trollprod.org/2013/07/lock-pattern-et-android/) and assuming that the other hashes where also gestures (hence the hint in the gesture.key file) I setup oclHashcat to brute the other hashes.

``` sh
./oclHashcat-plus64.bin -m 100 -a 3 --outfile-format=3 -o result hash.txt --hex-charset -1 000102030405060708 ?1?1?1?1?1?1?1?1?1 --force -i
oclHashcat-plus v0.15 by atom starting...

Hashes: 7 total, 1 unique salts, 7 unique digests
Bitmaps: 8 bits, 256 entries, 0x000000ff mask, 1024 bytes
Workload: 128 loops, 80 accel
Watchdog: Temperature abort trigger set to 90c
Watchdog: Temperature retain trigger set to 80c
Device #1: Redwood, 512MB, 650Mhz, 5MCU
Device #1: Kernel ./kernels/4098/m0100_a3.Redwood_1311.2_1311.2.kernel (434972 bytes)
Device #1: Kernel ./kernels/4098/markov_le_plus_v4.Redwood_1311.2_1311.2.kernel (322112 bytes)
Device #1: Kernel ./kernels/4098/bzero.Redwood_1311.2_1311.2.kernel (33868 bytes)

                                             
Session.Name...: oclHashcat-plus
Status.........: Cracked
Input.Mode.....: Mask (?1?1?1?1?1?1?1?1?1) [9]
Hash.Target....: File (hash.txt)
Hash.Type......: SHA1
Time.Started...: Sat Oct 26 19:53:04 2013 (2 secs)
Speed.GPU.#1...:   267.3 MH/s
Recovered......: 7/7 (100.00%) Digests, 1/1 (100.00%) Salts
Progress.......: 381890688/387420489 (98.57%)
Rejected.......: 0/381890688 (0.00%)
HWMon.GPU.#1...:  0% Util, 46c Temp, N/A Fan

Started: Sat Oct 26 19:53:04 2013
Stopped: Sat Oct 26 19:53:10 2013
```

The resulting patterns could be visually mapped to numbers:

~~~~
3 2 1
4 9 8 => 6
5 6 7

3 4 5
2 1 6 => 9
9 8 7

3 2 1
4 5 6 => 5
9 8 7

2 3 4
1 6 5 => 2
7 8 9
		
1 2 3
x 4 x => 7
5 x x

7 1 2
x 3 x => 8
4 5 6

1 2 3
x 4 5 => 3
8 7 6
~~~~

We first ordered them by filename which resulted in `\d67\d87\d87\d76\d78\d57\d76\d63\d79\d35\d95\d52\d75\d56\d67` which already looked like ASCII but made no sense (`CWWLN9L?O#_4K8C`), but when ordered by creation date the ASCII begins to make sense and results in the flag: `kill_all_humans`
