---
layout: post
title: "Corona-Regelupdate Dezember 2021"
date: 2021-12-02 22:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

| __**2021-12-02: Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**__
|

__**tl;dr: 2G, CWA und maximal 10 Personen.**__

Im Space gilt eine "2G"-Regelung. Folgende Personen haben Zutritt:

* Geimpfte und genesene Personen.
* Ebenfalls Zutritt haben Personen, die sich nicht impfen lassen können
   (mit Nachweis) 
   und Kinder unter 12 Jahren.

|

* Die Anzahl der Personen (Mitglieder und Gäste) ist auf zehn begrenzt.

## Dies setzen wir wie folgt um:

* Alle Mitgliedsentitäten sind Betreiber des Spaces, und damit selbst
   für die Einhaltung der Regelung verantwortlich.
* Wer Gäste mitbringt, ist dafür verantwortlich, dass diese die Regeln
   einhalten.
* Wenn im normalen Spacebetrieb Gäste vor der Tür stehen, so sind die im
   Space anwesenden Mitglieder dafür verantwortlich,
   diese vor Zutritt auf die 2G-Regelung hinzuweisen, die Einhaltung
   sicherzustellen und bei Nichteinhaltung,
   oder bei Nichteinwilligung zur Überprüfung den Zugang zum Space zu
   verwehren.

## Veranstaltungen

* Veranstaltungen im Space sind untersagt.
* Private Treffen, z.B. zum Umsetzen von Projekten, sind weiterhin möglich.

## Kontaktverfolgung

* Im Space hängen CWA-QR-Codes, bitte checkt euch ein (und wieder aus).
* Wer den CWA-QR-Code nicht nutzen kann oder möchte, benutzt einfach
   weiterhin die bekannten Zettel.
* Gebt uns Bescheid, wenn es zu positiven Fällen kommt, denn nur so
   können wir die Entitäten im jeweils anderen System warnen.

## Hygienemaßnahmen:

* Wascht euch die Hände und haltet den Space sauber.
* Lüftet, insbesondere zu Beginn und zum Ende eures Spacebesuchs.
    * Wenn es die Witterung zulässt, haltet den Space auf Durchzug.
* Komme nicht in den Space, wenn du dich krank fühlst.
* Trage im öffentlichen Flurteil weiterhin Maske und halte die Tür zum
   Flur geschlossen.

_Dieser Text darf gerne frei kopiert, übernommen und verändert werden. Falls eine Lizenzangabe nötig ist: CC0_

