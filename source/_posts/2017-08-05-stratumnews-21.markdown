---
layout: post
title: "Stratumnews Vol. 21"
author: Stratumnews-Team
date: 2017-08-05 13:00
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:
<!-- more -->

Festool-Spende
--------------

Aus der zugesprochenen Spende sind nun alle Teile angekommen: die CMS-Grundeinheit zum Einsetzen der Module, das Tischsägemodul (CMS-TS) sowie eine Oberfräse (OF-1400 EBQ) mit entsprechendem CMS-Tischmodul. Außerdem hat der Space von einem Mitglied einige Fräser für die Oberfräse zur Verfügung gestellt bekommen. Für eine Einführung in den sicheren Umgang mit den neuen Geräten fragt bitte rohieb, reneger, oder fototeddy.

Die Geräte wurden auch schon fleißig benutzt, beispielsweise wurde das wackelige Schuhregal im Flur [etwas stabilisiert][sr].

[sr]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/DngjAbwCNcuywSFsuyXgqshL


Spende durch In-Tech
--------------------

Im Mai hatten wir an einem Freitagmorgen Besuch von In-Tech, sie haben sich nun entschlossen, 1000€ für den Verein zu spenden.


Whiteboardmarker
----------------

Wir haben wieder funktionierende Whiteboardmarker! Zumindest eine größere Anzahl schwarzer Stifte. Sie liegen im Flurregal bei den Schreibutensilien. Falls sie wieder ausgehen: Bestellt neue und erstattet sie euch über die Verbrauchsmaterialkasse.


Mehl in der Küche
-----------------

Wir haben seit (nach) dem Easterhegg 2017 zwei große 25kg-Säcke Mehl in der Küche stehen, einmal Tipo 00, einmal Type 405. Beide sind jetzt angebrochen und stehen unter dem kleineren Tisch an der Wand neben der Stapel-Bar. Da sie etwas unhandlich sind, gibt es für beide Mehlsorten praktische Abfüllbehälter auf der Arbeitsplatte. Nutzt sie.


Vermisst und/oder gefunden
--------------------------

Der Space besitzt zwei Philips SHL3005BK Kopfhörer, schwarz, overear, klappbar. In der Audiokiste befinden sie sich nicht, wo sind sie? Auf jeweils einer Ohrmuschel war ein Stratum 0-Aufkleber. (Anmerkung während des Schreibens: Der eine ist in desolatem Zustand in der PCI(e)-Karten-Kiste wieder aufgetaucht.)


Stadtradeln
-----------

Die Stadt Braunschweig [beteiligt][bsradeln] sich dieses Jahr erstmalig am [Stadtradeln][radeln]: Als ganze Stadt über einen gewissen Zeitraum zusammen/gegeneinander so viel Rad fahren wie möglich. Anyhow, wir haben als Verein ein Team eingetragen, wer mitmachen möchte, einfach bei der Registrierung das richtige Team auswählen. Der Auswertungszeitraum beginnt am 27.08. und geht über 3 Wochen.

[bsradeln]: http://braunschweig.de/politik_verwaltung/nachrichten/stadtradeln
[radeln]: https://www.stadtradeln.de/hintergrund/


Nächste Mitgliederversammlung
-----------------------------

Auf der letzten Mitgliederversammlung im Dezember 2016 wurde gewünscht, dass der Termin der jährlichen Mitgliederversammlung langfristig in den Januar rutschen soll. In diesem Fall würden nun zwischen zwei Mitgliederversammlungen mehr als ein Jahr liegen. Unsere Satzung sieht eine Mitgliederversammlung pro Jahr vor, und der Vorstand ist auch nur auf ein Jahr gewählt, deswegen werden wir eine außerordentliche Mitgliederversammlung ansetzen.

Der Termin dazu ist Sonntag, der 24. September. Es gibt außer den Formalien nicht sehr viel zu besprechen, deswegen sehen wir als Tagesordnung eine Kassenprüfung, einen kurzen Rechenschaftsbericht, und die Entlastung des Vorstandes vor, dann eine Bestätigung des Vorstandes bis zum Januar 2018. Danach wird vielleicht der Grill angeworfen, um die letzten paar Sommertage auszunutzen :)

Es wäre schön, wenn genügend Mitglieder für Beschlussfähigkeit anwesend sind. Alles weitere dann [im Wiki][mv].

[mv]: https://stratum0.org/wiki/Mitgliederversammlung_2017-09-24


Rechner im Space
----------------

Nach sterbenden Platten in der Spacekiste und Stratux und mutmaßlich versagendem Netzteil in SpaceCADet wurden die Rechner im Space ein bisschen umgebaut und deren Innereien durchgetauscht und auch ein wenig aufgefrischt.
Spacekiste v3 ist up and running, die Platte "data" aus der alten Spacekiste wurde mitmigriert, die meisten Steamspiele sind also noch da. Die alte Spacekiste befindet sich derzeit noch bootfähig im Frickelraum, vorraussichtlich werden die Inhalte der alten Systemplatte noch irgendwo zugreifbar weggesichert. Es laufen jetzt ein Ubuntu 16.04 und eine Windows 10 im Dual Boot. Sie ist leider noch lauter als die alte Spacekiste, da müsste man mal™ was machen.

Stratux und SpaceCADet wurden vereint, kein Rechner mehr, der die Stickecke blockiert, dafür hoffentlich auch keine Möhre mehr, die sich bei Stickmotiverstellung per BSOD verabschiedet. Der Einrichtungsprozess ist ongoing, die Maschine aber schon benutzungsfähig.
Hier laufen nun ein Fedora 26 und ein Windows 10 im Dual Boot.


Küchenmülleimer
---------------

Wir haben einen neuen Tretmülleimer in der Küche. Mit Hoffnung auf weniger Versifftheit. Feature: Beim Auftreten schließt er sich wieder, sobald man den Fuss runter nimmt. Öffnet man ihn von Hand, so bleibt der Deckel offen, bis man ihn manuell schließt.


Termine
-------

Weitere Termine in nächster Zeit:

*  Mo, 07.08. 18:00: OF-Hochzeitskochen (fototeddy) (Küche)
*  Mo, 07.08. 19:00: HOA: Orgatreffen
*  Fr, 04.08. bis Di, 08.08.: SHA2017, Zeewolde (NL)
*  Di, 08.08. 18:00: Coptertreffen, Abschlussfliegen im Praktiker Gliesmarode
*  Mi, 09.08. 16:00: Schreibrunde
*  Mi, 09.08. 19:00: Freifunk-Treffen
*  Mi, 09.08. 19:00: Vegan Academy
*  Do, 10.08. 18:00 - 21:30: BS LUG
*  Do, 10.08. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 10.08. 19:00 - 23:00: Anime Referat
*  Mo, 14.08. 18:00: Malkränzchen
*  Mo, 14.08. 19:00: HOA: Orgatreffen
*  Mo, 14.08. 19:00: Vorträge
*  Di, 15.08. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 16.08. 16:00: Schreibrunde
*  Mi, 16.08. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Mi, 16.08. 19:00: Freifunk-Treffen
*  Do, 17.08. 18:00 - 21:30: BS LUG
*  Do, 17.08. 19:00 - 23:00: Captain's Log
*  Sa, 19.08. 14:00: CoderDojo
*  Mo, 21.08. 18:00: Malkränzchen
*  Mo, 21.08. 19:00: HOA: Orgatreffen
*  Mi, 23.08. 16:00: Schreibrunde
*  Mi, 23.08. 19:00: Freifunk-Treffen
*  Mi, 23.08. 19:00: Vegan Academy
*  Do, 24.08. 18:00 - 21:30: BS LUG
*  Do, 24.08. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 24.08. 19:00 - 23:00: Anime Referat
*  Fr, 25.08. bis So, 27.08.: Maker Faire Hannover
*  Mo, 28.08. 18:00: Malkränzchen
*  Di, 29.08. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 30.08. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Do, 31.08. 19:00 - 23:00: Captain's Log

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!
