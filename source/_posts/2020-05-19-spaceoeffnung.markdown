---
layout: post
title: "Öffnung des Spaces – ein klein wenig"
date: 2020-05-19 23:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

**Update 26. Juni:** [Juni-Edition](https://stratum0.org/blog/posts/2020/06/23/spaceoeffnung/)

In vielen Bereichen in unserer Gesellschaft ist es bereits zu
Lockerungen gekommen und die [MakerVsVirus-Aktivitäten][] im Space laufen
langsam aus.

[MakerVsVirus-Aktivitäten]: https://stratum0.org/blog/posts/2020/04/07/makervsvirus/

Wir möchten den Space daher jetzt wieder ein wenig für Mitglieder
öffnen.
Das Ziel dieser Öffnung ist es, den Mitgliedern zu ermöglichen,
unsere Werkzeuge, Maschinen und Einrichtungen zu nutzen.
Veranstaltungen und Treffen können weiterhin nicht stattfinden. Ebenso
soll der Space nicht zum Rumhängen oder Chillen genutzt werden.

Wie ein Vereinsraum vom Typ "Hackerspace" geöffnet oder genutzt werden
kann, wird von der [Landesregierung](https://www.niedersachsen.de/download/155111)
\(natürlich\) nicht explizit geregelt.
Wir orientieren uns mit unseren Vorgaben daher an den
Regelungen für ähnliche Einrichtungen und den allgemeinen Leitlinien
der Vorgabe.

Wir möchten daher folgende Regelungen für die Nutzung des Spaces
festlegen:

* Der Space steht Mitgliedern wieder eingeschränkt für die Nutzung 
  zur Verfügung.

* Primär sollen die Räume Chillraum, Frickelraum und Werkstatt genutzt werden. 
  Die gemeinsam genutzten Räume (Küche, Flur und WC) sollen immer nur kurzzeitig 
  genutzt werden. Bitte haltet hierbei den Mindestabstand ein.

* Je Raum darf sich auf längere Zeit ein Mitglied oder ein Mitglied und Personen 
  aus dem selben Hausstand aufhalten (Durchgangsräume betreten, um andere Räume 
  zu erreichen, ist natürlich ausgenommen, solange die Abstandsregeln 
  eingehalten werden). 

* Es dürfen sich maximal 10 Personen gleichzeitig im Space aufhalten.

* Zwischen den Personen unterschiedlicher Hausstände ist ein 
  Mindestabstand von 1,5 Metern einzuhalten.

* Vor und nach der Nutzung eines Raumes muss dieser, so weit möglich, 
  gründlich gelüftet werden. Während der Nutzung bietet es sich an, 
  mindestens ein Fenster gekippt zu lassen.

* Räume müssen im Spacenutzungsplan reserviert werden.
  Bitte seid beim Reservieren fair zu einander!
  (Den Plan findet ihr in dieser Ankündigung auf dem
  [Normalverteiler](https://stratum0.org/wiki/Kontakt#Mail_und_Mailinglisten)
  und im IRC.)
  

* Wer den Space betritt, muss seine Kontaktdaten hinterlassen:

  * Hierzu werden [Zettel](https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/XliisJtXTyQIyMBYZCikIyKL)
    ausliegen, die ausgefüllt in einen verschlossenen Kasten
    geworfen werden.
  * Wir werden auf diesen Zetteln Name, einen Kontaktweg und den 
    Zeitraum deines Besuches erfassen.
  * Diese Daten werden nach drei Wochen vernichtet. Die gesammelten 
    Daten werden nur zur Nachverfolgung eventueller Infektionsketten 
    genutzt.

|

* Bitte wascht euch regelmäßig die Hände und verhaltet euch hygienisch.

* Bringt ein eigenes Händehandtuch mit. Wenn ihr kein eigenes Handtuch 
  dabei habt, benutzt Papierhandtücher.

* Nach der Nutzung müssen genutzte Oberflächen, Geräte und Maschinen 
  entweder desinfiziert oder mit Seifenwasser abgewaschen werden.

* Und natürlich: Wenn du dich krank fühlst, komm nicht in den Space!

Wir hoffen mit diesen Regeln eine Möglichkeit zu schaffen, unseren
Space wieder nutzen zu können.
Wir wissen aber auch, dass dies nur eine erste Version dieser
Regelungen ist.
Wir werden daher die Umsetzung der Regelungen und auch
die veränderten Rahmenbedingungen weiter im Auge behalten und unsere
Regelungen bei Bedarf anpassen.

Happy Hacking!
Euer Vorstand
