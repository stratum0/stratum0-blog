---
layout: post
title: "Corona-Regelupdate Oktober 2021"
date: 2021-10-05 22:00
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

| __**2021-10-05: Diese Regelung gilt ab sofort und ersetzt alle vorherigen Regelungen.**__
|

__**tl;dr: 2G und CWA statt Masken und Zettel.**__

Im Space gilt eine "2G"-Regelung. Folgende Personen haben Zutritt:

* Geimpfte und genesene Personen.
   Diese müssen keine Mund-Nasen-Bedeckung tragen, oder Abstände
   einhalten.
* Ebenfalls Zutritt haben Personen, die sich nicht impfen lassen können
   (mit Nachweis).
   Haben diese einen gültigen Test, müssen diese keine Mund-Nasen-
   Bedeckung tragen oder Abstände einhalten.
* Kinder unter 12 Jahren.
   Diese müssen keine Mund-Nasen-Bedeckung tragen oder Abstände
   einhalten.


## Dies setzen wir wie folgt um:

* Alle Mitgliedsentitäten sind Betreiber des Spaces, und damit selbst
   für die Einhaltung der Regelung verantwortlich.
* Wer Gäste mitbringt, ist dafür verantwortlich, dass diese die Regeln
   einhalten.
* Wenn im normalen Spacebetrieb Gäste vor der Tür stehen, so sind die im
   Space anwesenden Mitglieder dafür verantwortlich,
   diese vor Zutritt auf die 2G-Regelung hinzuweisen, die Einhaltung
   sicherzustellen und bei Nichteinhaltung,
   oder bei Nichteinwilligung zur Überprüfung den Zugang zum Space zu
   verwehren.

## Veranstaltungen

* Geplante Veranstaltungen sind im Space-Terminkalender einzutragen,
   dabei bitte maximal eine Veranstaltung im Space gleichzeitig.
* Für Veranstaltungen ist mindestens eine anwesende Entität zu
   definieren, die die Kontrolle der 2G-Regelung zu verantworten hat.
   Dies kann zum Beispiel auch über den "Kontakt"-Eintrag auf der
   Veranstaltungsseite im Wiki passieren.
* Veranstaltungen können Maskenpflicht festlegen.
   Dies muss dann beim Kalendereintrag dabei stehen und gilt für alle
   Anwesenden im Space.
   Restliche Regeln (2G) gelten weiterhin.
* Veranstaltungen können Testpflicht festlegen.
   Dies muss dann beim Kalendereintrag dabei stehen und gilt für alle
   Anwesenden im Space.
   Restliche Regeln (2G) gelten weiterhin.

## Kontaktverfolgung

* Im Space hängen CWA-QR-Codes, bitte checkt euch ein (und wieder aus).
* Wer den CWA-QR-Code nicht nutzen kann oder möchte, benutzt einfach
   weiterhin die bekannten Zettel.
* Gebt uns Bescheid, wenn es zu positiven Fällen kommt, denn nur so
   können wir die Entitäten im jeweils anderen System warnen.

## Hygienemaßnahmen:

* Wascht euch die Hände und haltet den Space sauber.
* Lüftet, insbesondere zu Beginn und zum Ende eures Spacebesuchs.
    * Wenn es die Witterung zulässt, haltet den Space auf Durchzug.
* Komme nicht in den Space, wenn du dich krank fühlst.
* Trage im öffentlichen Flurteil weiterhin Maske und halte die Tür zum
   Flur geschlossen.

## Begründung

In unserem Umfeld ist (auch nach Erfahrungen vom HOA) eine sehr hohe 
Durchimpfungsquote vorhanden. Wir legen den Fokus der 
Spacercoronaregelung auf die Abwägung zwischen Rückführung zum 
Normalbetrieb und den Schutz der <1% Bevölkerung, die sich aus Gründen 
tatsächlich nicht impfen lassen kann und ca. 11% Bevölkerung Kinder 
unter 12 für die es noch keine Impfempfehlung gibt, vor den der 
restlichen 20% der Bevölkerung, die sich derzeit aktiv gegen eine 
Impfung entschieden haben. 
([Siehe auch](https://www.stk.niedersachsen.de/startseite/presseinformationen/anderung-der-niedersachsischen-corona-verordnung-gut-geschutzt-in-den-herbst-mehr-moglichkeiten-durch-mehr-2g-204361.html))
Nach der Niedersächsischen Verordnungsänderungsverordnung vom 21.09.2021 
ist es möglich, dass Betreiber einer Einrichtung den Zutritt auf 
geimpfte und genesene Personen beschränken können ($8(7)). Diese 
Möglichkeit nutzen wir nun.


_Dieser Text darf gerne frei kopiert, übernommen und verändert werden. Falls eine Lizenzangabe nötig ist: CC0_

