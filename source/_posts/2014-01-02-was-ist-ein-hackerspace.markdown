---
layout: post
title: "Was ist ein Hackerspace?"
date: 2014-01-02 00:14
comments: true
author: valodim
categories: [german,hackerspaces]
---

Ich finde mich des Öfteren mit der Frage konfrontiert, was denn ein Hackerspace
überhaupt ist. Da dies eine unerwartet knifflige Frage ist, schreibe ich diesen
Blogpost, um künftig darauf verweisen zu können.


## "Hacker"spaces

Ein Hackerspace ist in erster Linie ein Treffpunkt für technikinteressierte
Menschen, üblicherweise in Form eines Vereins. Wie sich in einem Sportverein
Menschen treffen, um gemeinsam Sport zu treiben oder einfach unter
Gleichgesinnten gesellig zu sein, trifft man sich in einem Hackerspace um
gemeinsam zu hacken oder einfach unter Gleichgesinnten gesellig zu sein.

<!--more-->

Der fehlende, aber wichtige Punkt dabei ist - was ist "hacken"? Der Artikel zum
Thema [Hacker] in der Wikipedia ist recht lang und unspezifisch, enthält aber
folgendes schöne Zitat des Hackers erster Stunde [Wau Holland]:

> Ein Hacker ist jemand, der versucht einen Weg zu finden, wie man mit einer
> Kaffeemaschine Toast zubereiten kann.
{: style="font-size: 18px;" }

Diese Aussage bringt es gut auf einen Nenner. Das "Hacken" ist die kreative,
spielerische, aber dennoch tiefgängige Auseinandersetzung mit Technik. Das ist
zugegeben eine reichlich unscharfe Definition, die aber notwendig ist um der
Vielfältigkeit der Hackerkultur gerecht zu werden.

Der aus der Populärkultur bekannte Stereotyp des "Computerhackers," der von
seinem Laptop aus Banken ausraubt, fällt dabei durchaus in den Rahmen dieser
Definition. Dieses Bild hat mit der Realität allerdings ähnlich viel zu tun
wie die meisten von Hollywood geprägten Rollenbilder.

Wichtigste Aufgabe des Vereins ist es, eine Räumlichkeit bereitzustellen. Dort
können Mitglieder sich treffen, an ihren Projekten arbeiten, Erfahrungen
auszutauschen, oder auch einfach gemeinsam ihre Freizeit gestalten. Darüber
hinaus finden in vielen Hackerspaces auch Vorträge oder Workshops zu speziellen
Themen statt, die sich im Normalfall auch an Externe richten. Die genaue
Ausprägung dieser Aspekte ist allerdings sehr individuell.

Generell versuchen die meisten Hackerspaces, eine möglichst unbürokratische und
offene Struktur zu pflegen, um Interessierten einen einfachen Einstieg zu
ermöglichen, wenn sie bei einem Projekt mitmachen oder gar etwas eigenes
anfangen möchten, und dafür eine geeignete Plattform suchen. Viele Hackerspaces
erlauben auch Externen die Nutzung vorhandener Infrastruktur wie speziellem
Werkzeug oder anderen Gerätschaften, meist mit Empfehlung einer Spende.


## Projekte

Die folgenden drei Projekte sind Beispiele für Aktivitäten, die im Stratum 0
von verschiedenen Mitgliedern verfolgt werden. Dies ist keineswegs eine
vollständige Liste, es handelt sich um ausgewählte Projekte mit guter
Vorzeigbarkeit.


- {% img right https://stratum0.org/mediawiki/images/thumb/8/88/20121020-reprap-01.jpg/180px-20121020-reprap-01.jpg Unser RepRap %}
  Mittlerweile ein Muss für jeden Hackerspace, besitzt auch unser Hackerspace
  einen **[3D-Drucker]** des Typs [RepRap]. Darauf wurden schon
  unterschiedlichste nützliche oder kunstvolle Gegenstände gedruckt,
  insbesondere der [Mate Tag] erfreut sich unter Besuchern als Souvenir großer
  Beliebtheit.

  Das Thema 3D Printing erhielt in den letzten Jahren einige Aufmerksamkeit in
  der Öffentlichkeit und den Medien. Auch von uns hielten verschiedene
  Mitglieder Talks zu dem Thema auf Veranstaltungen in Braunschweig, wie dem
  [Studium Generale] der TU Braunschweig, oder dem [Barcamp 2012].

- Unser CTF Team nimmt regelmäßig und erfolgreich an sogenannten "**Capture The
  Flag**" Security Contests teil. In diesen Contests geht es darum, in
  Konkurrenz mit anderen Teams komplizierte Aufgaben zu lösen, meist aus
  verschiedenen Bereichen der Computersicherheit.

  {% img left /images/posts/2014-01-02-was-ist-ein-hackerspace/pwn.png %}

  Dies ist wohl die Aktivität des Hackerspaces, welche dem klassischen Bild des
  "Hackens" am nähsten kommt. Hier geht es in der Tat darum, Sicherheitslücken
  von Software zu finden und auszunutzen - wenn auch in kontrolliert
  sportlichem Rahmen. Näheres zu diesem Thema im entsprechenden Blogpost
  "[About Writeups, and CTFs][ctfs]" (leider nur auf Englisch)

- Noch recht neu ist das regelmäßig stattfindende **[Multikopter-Treffen]**.
  Ein Multikopter ist eine flugfähige, mit drei oder mehr Rotoren ausgestattete
  Drohne, die ähnlich wie im traditionellen Modellflug ferngesteuert geflogen
  werden kann, dabei aber größere Autonomie sowie eine weitaus höhere
  Flexibilität hinsichtlich möglicher Erweiterungen und Basteleien aufweist.

  Die Interessierten verfolgen dabei teils recht unterschiedliche
  Zielsetzungen: Ein Mitglied schraubt beispielsweise noch an seinem ersten
  Quadkopter herum, um irgendwann den ersten eigenen Flug starten zu können.
  Ein anderes versucht, eine drahtlose Verbindung herzustellen zwischen einer
  auf seinem Kopter montierten Kamera und einer speziellen Videobrille, um auf
  diese Weise Flüge aus der Ich-Perspektive unternehmen zu können.


[Hacker]: http://de.wikipedia.org/wiki/Hacker
[Wau Holland]: http://de.wikipedia.org/wiki/Wau_Holland
[Multikopter-Treffen]: https://stratum0.org/wiki/Multikopter
[3D-Drucker]: https://stratum0.org/wiki/RepRap
[RepRap]: http://reprap.org/
[Mate Tag]: http://www.thingiverse.com/thing:38861
[ctfs]: /blog/2013/09/27/about-writeups-and-ctfs/
[Studium Generale]: http://www.braunschweig.de/kultur/veranstaltungen/index.html?mode=details&event_id=123363
[Barcamp 2012]: http://barcampbs.mixxt.de/

