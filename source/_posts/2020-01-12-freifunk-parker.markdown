---
layout: post
title: "Freifunk Braunschweig: Project Parker im Beta-Betrieb"
date: 2020-01-12 22:00
comments: true
author: chrissi^
categories: [german, freifunk]
---

Im klassischen Gluon-basierenden Freifunk wird in der Regel mit B.A.T.M.A.N 
ein großes Layer-2-Netz simuliert.
Gerade in großen Netzen (ab einigen hundert Knoten bzw. Benutzern) führt dies
zu Problemen.
*Project Parker* versucht diesen Problemen mit klassischem Routing zu
begegnen.

Im Hintergrund haben wir die Arbeiten an diesem Projekt fortgesetzt und haben
nun einen Stand erreicht, mit dem wir einen geschlossenen Beta-Test durchführen.

{% fancyalbum 300x300 %}
images/posts/2020-01-12-freifunk-parker/mesh.jpg: Testnetz im Meshviewer
images/posts/2020-01-12-freifunk-parker/parker-mesh-open.jpg: Getestet wird auf der schwächsten Hardware im Freifunk-Netz
{% endfancyalbum %} 

Technologie
=============

Allen, die an der Technologie hinter *Project Parker* interessiert sind sei
der [Talk] ans Herz gelegt, den Shoragan und Chrissi^ auf dem 35C3 gehalten 
haben.

[Talk]: https://stratum0.org/blog/posts/2018/12/22/freifunk-parker/

Die wesentlichen Quellen unseres Projektes befinden sich an drei Orten:

* Git: [Gluon-Fork]: im *master*-Branch
* Git: [Freifunk Braunschweig Packages]: im *next*-Branch
* Git: [Ansible Repo]: im *master*-Branch

Unser Gluon-Fork enhält dabei im Wesentlichen Änderungen am Gluon, die sich
mit aktuellen Gluon-Mitteln nicht konfigurieren lassen.
In unseren Packages lebt die meiste Logik von *Project Parker* im Paket
[gluon-ffbsnext-nodeconfig].

Die [Site] Konfiguration, die zu unserem aktuellen Beta-Test passt, befindet 
sich im *next*-Branch.

[Gluon-Fork]: https://gitli.stratum0.org/ffbs/ffbs-gluon/tree/master
[Freifunk Braunschweig Packages]: https://gitli.stratum0.org/ffbs/ffbs-packages/tree/next
[Ansible Repo]: https://gitli.stratum0.org/ffbs/ffbs-ansible
[gluon-ffbsnext-nodeconfig]: https://gitli.stratum0.org/ffbs/ffbs-packages/tree/next/gluon-ffbsnext-nodeconfig
[Site]: https://gitli.stratum0.org/ffbs/ffbs-site/tree/next


Geschlossener Beta-Test
==========================

*Project Parker* befindet sich aktuell noch in einem frühen Zustand:
Viele Details, besonders um das Roaming von Clients in einem Mesh, sind noch
nicht fertig oder nicht abschließend getestet.
Dennoch haben wir uns entschieden einen geschlossenen Beta-Test zu starten.
In diesem Test wollen wir Erfahrungen sammeln und noch unbeachtete Sonderfälle
finden.

Folgendes sollte bereits funktionieren:

* Ein einzelner Knoten mit Uplink (am WAN-Interface) baut Tunnel zu unserer
  zentralen Infrastruktur auf.
* Der Knoten stellt IPv4- und IPv6-Konnektivität für seine Clients zur
  Verfügung.
* Knoten können per WLAN miteinander meshen.
* Clients können auch an meshenden Knoten das Internet erreichen.
* Es gibt Visualisierung via Meshviewer und Grafana.
* Die Test-Infrastruktur besteht aus drei *Concentrator* und einem *Exit*.
  Concentrator halten dabei die Verbindung zu den Clients. Der Exit leitet
  den Traffic über Freifunk Rheinland in das Internet aus.
  
Für unseren geschlossenen Beta-Test gelten ein paar wenige Besondersheiten:

* Auf allen Knoten sind standardmäßig SSH-Keys der Entwickler hinterlegt.
* Alle Knoten loggen per *rsyslog* Informationen in unsere Infrastruktur.

Wer am Beta-Test teilnehmen möchte kann eine E-Mail an kontakt@freifunk-bs.de
schreiben. 
Wir geben dann die notwendigen Informationen für eine Teilnahme weiter.

Ausblick
========

{% fancyalbum 300x300 %}
images/posts/2020-01-12-freifunk-parker/parco.jpg: Zu Project Parker gibt's immer Pizza von Parco.
{% endfancyalbum %} 

Viel Detailarbeit liegt nun hinter uns. Aber natürlich gibt es noch genug zu tun.
Die nächsten großen Schritte sind wahrscheinlich:

* Erfahren mit dem Beta-Test sammeln und Langzeitstabilität verbessern.
* Monitoring der Infrastruktur ausbauen.
* Testen auf anderer Hardware.
* Verhalten bei Roaming von Clients in Meshen mit mehreren Uplinks
  optimieren.
