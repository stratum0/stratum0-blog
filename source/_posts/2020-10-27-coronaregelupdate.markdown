---
layout: post
title: "Corona-Regelupdate Oktober 2020"
date: 2020-10-27 23:30
comments: true
author: Chrissi
categories: [german, stratum0, covid19]
---

__**Update 2020-11-01 diese Regelungen wurden abgelöst. 
[Hier](https://stratum0.org/blog/posts/2020/11/01/coronaregelupdate-november/) geht es zu den neuen Regelungen.**__

Die Corona-Fallzahlen in Braunschweig steigen 
[rasant](https://www.braunschweig.de/aktuell/aktuelle-informationen.php#Aktuelle_Zahlen), 
daher wollen
wir die Spaceregelungen an die aktuelle Situation und an die neuen
[Regelungen durch das Land](https://www.niedersachsen.de/download/160020) anpassen.

Dies bedeutet für uns wieder neue Einschränkungen bei der Nutzung des
Spaces. Mit diesen Regelungen wollen wir es ermöglichen Teile der Räume
zu nutzen und gleichzeitig dem Infektionsschutz nachkommen.

Der Chillraum wurde so umgebaut, dass sich dort nun auch Arbeitsplätze
befinden. Der Plotter, die T-Shirt-Presse und die Nähmaschinen wurden
ebenfalls dorthin umgezogen. Dies soll es mehr Entitäten gleichzeitig
ermöglichen, ihren Projekten nachzugehen.

Neue Regelungen sind:

* Es ist ein Abstand von 1,5 Metern zwischen Personen unterschiedlicher
  Haushalte einzuhalten.
* Im Space ist eine Mund-Nasen-Bedeckung zu tragen.
  Ausnahmen hiervon sind die Nahrungs- und Flüssigkeitsaufnahme, sowie
  körperlich anstrengende Arbeiten in der Werkstatt.
* Wir empfehlen, dass sich pro Raum jeweils nur die Mitglieder des 
  selben Haushalts und enge Angehörige aufhalten.
* Es dürfen sich maximal 10 Personen gleichzeitig im Space aufhalten.

* Mitglieder dürfen Gäste mit in den Space bringen. Die Gäste dürfen
  aber nicht alleine/unbetreut im Space sein. (Die vorherigen Punkte
  gelten natürlich weiterhin.)
* Es sind weiterhin keine öffentlichen Veranstaltungen im Space
  zugelassen.

* Wer den Space betritt, muss weiterhin seine Kontaktdaten
hinterlassen:
  * Hierzu liegen [Zettel](https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/XliisJtXTyQIyMBYZCikIyKL) aus, die ausgefüllt in einen
    verschlossenen Kasten geworfen werden.
  * Wir werden auf diesen Zetteln Name, einen Kontaktweg und den
    Zeitraum Deines Besuches erfassen.
  * Diese Daten werden nach drei Wochen vernichtet. Die gesammelten
    Daten werden nur zur Nachverfolgung eventueller Infektionsketten
    genutzt.

* Räume müssen im Spacenutzungsplan reserviert werden.
  Bitte seid beim Reservieren fair zueinander und tragt sowohl ein
  Beginn als auch ein abgeschätztes Ende ein!
  (URL aus Datenschutzgründen bitte im Chat oder auf der Mailingliste erfragen.)

* Der Space soll primär für Projekte verwendet werden, welche zu hause
  nicht umgesetzt werden können! Bitte kommt nicht vorrangig zum
  socializen in den Space.

* Vor und nach der Nutzung eines Raumes muss dieser, so weit möglich,
  gründlich gelüftet werden. Während der Nutzung bietet es sich an,
  mindestens ein Fenster gekippt zu lassen.

* Bitte wasche Dir regelmäßig die Hände und verhalte Dich hygienisch.

* Bringe ein eigenes Händehandtuch mit. Wenn Du kein eigenes Handtuch
  dabei hast, benutze Papierhandtücher.

* Nach der Nutzung müssen genutzte Oberflächen, Geräte und Maschinen
  entweder desinfiziert oder mit Seifenwasser abgewaschen werden.

* Und natürlich: Wenn Du dich krank fühlst, komm nicht in den Space!

Happy Hacking!
Euer Vorstand

PS:
Der Vorstand versucht diese Regelungen laufend zu optimieren.
Wenn Du Änderungsvorschläge hast lass uns diese gern wissen.
